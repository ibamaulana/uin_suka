<?php
use Symfony\Component\Console\Output\ConsoleOutput;

if (!function_exists('consoleprint')) {

    /**
     * Return variable in console terminal
     *
     * @param string $messages
     *
     * @return void
     */
    function consoleprint($messages) : void
    {
        $output = new ConsoleOutput();
        if (is_a($messages, 'Illuminate\Database\Eloquent\Collection')) {
            $output->writeln("<info>".implode(",", $messages->toArray())."</info>");
        } elseif (is_a($messages, 'Illuminate\Http\Request')) {
            $output->writeln("<info>".implode(",", $messages->all())."</info>");
        } elseif (is_array($messages)) {
            $output->writeln("<info>".implode(",", $messages)."</info>");
        } elseif (is_int($messages) or is_string($messages)) {
            $output->writeln("<info>".$messages."</info>");
        }
    }
}
