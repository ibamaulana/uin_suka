<?php

if (!function_exists('loadJson')) {
    /**
     * Methood loadJson, to load Json inside data/ directory
     *
     * @param string $dir
     * @param string $filename
     *
     * @return array
     */
    function loadJson(string $dir, string $filename)
    {
        $json = file_get_contents($dir ."$filename.json");
        return json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json), true);
    }
}
if (!function_exists('collection_simplify')) {
    /**
     * Get least data from collection
     *
     * @param mixed $array
     * @param mixed $key
     * @param mixed $value
     * @param mixed $child
     *
     * @return array
     */
    function collection_simplify($array, $key, $value, $child=null)
    {
        $arr = array();
        foreach ($array as $a) {
            $arr[$a->$key] = $a->$value;
            if (is_null($child)) {
                $arr[$a->$key] = $a->$value;
            } else {
                $arr[$a->$key] = $a->$value->$child;
            }
        }
        return $arr;
    }
}
if (!function_exists('value_of_key')) {
    /**
     * Match function in array key, if key doesn't exist return null
     *
     * @param array  $arr
     * @param string $key
     *
     * @return mixed
     */
    function value_of_key(array $arr, string $key)
    {
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        return null;
    }
}
if (!function_exists('array_pluck')) {
    /**
     * Filter array associative based on pluck parameter
     * Example: $array = {"name"=>name, "email"=>email@mail.com} and $pluck = {"name"}
     * Return: {"name"=>name}
     *
     * @param array $array
     * @param array $pluck
     *
     * @return array
     */
    function array_pluck(array $array, array $pluck) : array
    {
        $result = [];
        foreach ($pluck as $value) {
            $data = value_of_key($array, $value);
            if (!is_null($data)) {
                $result[$value] = $data;
            }
        }
        return $result;
    }
}
