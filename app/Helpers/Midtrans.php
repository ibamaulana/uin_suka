<?php 

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

class Midtrans
{
	public static function chargeToken($parameter = [])
	{
		$midtrans_status = env('MIDTRANS_STATUS');
		$base_url = env('MIDTRANS_'.$midtrans_status.'_URL');
		$token = base64_encode(env('MIDTRANS_'.$midtrans_status.'_SERVER_KEY').':');

		$headers = [
			'headers' => [
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'Authorization'=> 'Basic '.$token,
			],
			'json' => $parameter
		];
		
		try {
			$client = new \GuzzleHttp\Client(['base_uri' => $base_url]);
			$response = $client->request('POST', 'snap/v1/transactions', $headers);
		} catch (RequestException $exception) {
			$response = $exception->getResponse();
		}

		if(!is_null($response)){
			$result = [
				'status_code' => $response->getStatusCode(),
				'data' => json_decode($response->getBody()->getContents())
			];
			return $result;
		} else echo 'API DOWN';
	}

	public static function getDetail($order_id)
	{
		$midtrans_status = env('MIDTRANS_STATUS');
		$base_url = env('MIDTRANS_'.$midtrans_status.'_API_URL');
		$token = base64_encode(env('MIDTRANS_'.$midtrans_status.'_SERVER_KEY').':');

		$headers = [
			'headers' => [
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'Authorization'=> 'Basic '.$token,
			],
		];
		
		try {
			$client = new \GuzzleHttp\Client(['base_uri' => $base_url]);
			$response = $client->request('GET', 'v2/'.$order_id.'/status', $headers);
		} catch (RequestException $exception) {
			$response = $exception->getResponse();
		}

		if(!is_null($response)){
			$result = [
				'status_code' => $response->getStatusCode(),
				'data' => json_decode($response->getBody()->getContents())
			];
			return $result;
		} else echo 'API DOWN';
	}

	public static function getContents($response)
	{
		$data = $response->getBody()->getContents();
		return json_decode($data);
	}

	/**
	* Get parameter API Value for internal
	* param array('value', 'condition')
	* recieve condition =, !=, <=, >=, like
	* not acceptable  for beetwen
	* return string
	*/
	public static function getParamValue($parameter)
	{
		return implode(',', $parameter);
	}
}