<?php
use Illuminate\Support\Facades\Auth;
use App\Models\User;

if (!function_exists('user_role_order')) {
    /**
     * Method user_role_order
     * Higher role of authenticated user based on id, so record on db must be adjusted
     *
     * @param User $user
     *
     * @return string
     */
    function user_role_order(User $user) : string
    {
        $model = \Spatie\Permission\Models\Role::get();
        $arrRole = collection_simplify($model, 'name', 'id');
        $roles = $user->getRoleNames();
        $small = 9999;
        $higherRole = null;
        foreach ($roles as $role) {
            if ($small > $arrRole[$role]) {
                $small = $arrRole[$role];
                $higherRole = $role;
            }
        }
        return $higherRole ?? '';
    }
}
if (!function_exists('perm')) {
    /**
     * Method perm
     * Check whether the authenticated user has permission to
     *
     * @param User   $user
     * @param string $perm
     *
     * @return bool
     */
    function perm(User $user, string $perm) : bool
    {
        return $user->hasPermissionTo($perm);
    }
}
if (!function_exists('any_perm')) {
    /**
     * Method any_perm
     * Check whether the authenticated user has permission within array
     *
     * @param User  $user
     * @param array $perms
     *
     * @return bool
     */
    function any_perm(User $user, array $perms) : bool
    {
        if (is_array($perms)) {
            foreach ($perms as $perm) {
                if ($user->hasPermissionTo($perm) ) {
                    return true;
                }
            }
        }
        return $user->can($perms);
    }
}
