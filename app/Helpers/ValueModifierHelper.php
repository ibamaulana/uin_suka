<?php
if (!function_exists('phone_validate')) {
    /**
     * Method phone_validate
     * Verify phone number value, is it already corect or not
     *
     * @param string $phone
     * @param string $code
     *
     * @return string
     */
    function phone_validate(string $phone) : string
    {
        $regex = "/\+([ -]?\d+)+|\(\d+\)([ -]\d+)/";
        preg_match($regex, $phone, $match);
        return value_of_key($match, 0);
    }
}

if (!function_exists('array_content_to_lower')) {
    /**
     * Method array_content_to_lower
     *
     * @param array $array
     *
     * @return array
     */
    function array_content_to_lower(array $array) : array
    {
        $result = [];
        foreach ($array as $value) {
            array_push($result, strtolower($value));
        }
        return $result;
    }
}

if (!function_exists('simplify_paginate')) {
    /**
     * Method simplify paginate
     *
     * @param mixed $collection
     *
     * @return array
     */
    function simplify_paginate($collection) : array
    {
        $array = $collection->toArray();
        $array['result'] = $array['data'];
        $toRemove = ["first_page_url", "last_page_url", "links", "path", "next_page_url", "prev_page_url", "data"];
        foreach ($toRemove as $key) {
            unset($array[$key]);
        }
        return $array;
    }
}

if (!function_exists('remap_paginate')) {
    /**
     * Method remapping paginate
     *
     * @param mixed $collection
     * @param mixed $function
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    function remap_paginate($collection, $mapping) : \Illuminate\Pagination\LengthAwarePaginator
    {
        // Example $mapping:
        // function($item) {
        //     return [
        //         'id' => $item->id,
        //         'name' => $item->name,
        //     ];
        // }
        $itemsTransformed = $collection
            ->getCollection()
            ->map($mapping)->toArray();

        $paginate = new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsTransformed,
            $collection->total(),
            $collection->perPage(),
            $collection->currentPage(), [
                'query' => [
                    'page' => $collection->currentPage()
                ]
            ]
        );
        return $paginate;
    }
}

if (!function_exists('unset_collection')) {
    /**
     * Method simplify paginate
     *
     * @param mixed $collection
     *
     * @return array
     */
    function unset_collection($collection, array $unsetKey = []) : array
    {
        $array = $collection->toArray();
        foreach ($unsetKey as $key) {
            unset($array[$key]);
        }
        return $array;
    }
}

if (!function_exists('format_slug')) {
    /**
     * String in slug format
     *
     * @param string $string
     *
     * @return string
     */
    function format_slug($string) : string
    {
        $string = preg_replace('/[^a-z0-9\s\-]/i', '', $string);
        // Replace all spaces with hyphens
        $string = preg_replace('/\s/', '-', $string);
        // Replace multiple hyphens with a single hyphen
        $string = preg_replace('/\-\-+/', '-', $string);
        // Remove leading and trailing hyphens, and then lowercase the URL
        $string = strtolower(trim($string, '-'));

        return $string;
    }
}

if (!function_exists('array_to_select2')) {
    /**
     * Create select2 array
     *
     * @param mixed $array
     *
     * @return array
     */
    function array_to_select2($array) : array
    {
        $result = [];
        foreach ($array as $value => $text) {
            array_push(
                $result, [
                "text"=> $text,
                "value" => $value
                ]
            );
        }
        return $result;
    }
}

if (!function_exists('array_to_select2_identic')) {
    /**
     * Create select2 array but have same value on text and value
     *
     * @param mixed $array
     *
     * @return array
     */
    function array_to_select2_identic($array) : array
    {
        $result = [];
        foreach ($array as $text) {
            array_push(
                $result, [
                "text"=> $text,
                "value" => $text
                ]
            );
        }
        return $result;
    }
}
