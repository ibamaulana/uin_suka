<?php
/**
 * Anotations
 * php version 7.4.16
 *
 * @category AdminAnnotation
 * @package  Annotation
 * @author   smartjen <helo@smartjen.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0.html Apache License, Version 2.0
 * @link     http://smartjen.com
 */
// USER
 /**
  * Route api/users/
  *
  * @OA\Get(
  *      path="/admin/users/",
  *      operationId="getAllUser",
  *      tags={"Admin User"},
  *      summary="Get all user",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/users/include
  *
  * @OA\Post(
  *      path="/admin/users/include",
  *      tags={"Admin User"},
  *      summary="Get included user",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="role", in="query",
  *     description="get based on role"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  *
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"user_id"},
  * @OA\Property(
  *          property="user_id",
  *          example="[]",
  *        ),
  *     )
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/users/exclude
  *
  * @OA\Post(
  *      path="/admin/users/exclude",
  *      tags={"Admin User"},
  *      summary="Get excluded user",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="role", in="query",
  *     description="get based on role"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  *
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"user_id"},
  * @OA\Property(
  *          property="user_id",
  *          example="[]",
  *        ),
  *     )
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/users/select2
  *
  * @OA\Get(
  *      path="/admin/users/select2",
  *      tags={"Admin User"},
  *      summary="Get select2 from user",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/users
  *
  * @OA\Post(
  *      path="/admin/users",
  *      operationId="createUser",
  *      tags={"Admin User"},
  *      summary="Create user",
  *      description="[TIPS] You may insert tutor/student/parent value using this endpoint by add it's value attributes on request body",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\MediaType(
  *       mediaType="multipart/form-data",
  * @OA\Schema(ref="#/components/schemas/UserRequest")
  *    )
  *  ),
  * @OA\Response(response=201,                         description="Successful operation"),
  * @OA\Response(response=400,                         description="Bad request"),
  *     )
  */
 /**
  * Route api/users/{id}
  *
  * @OA\Get(
  *      path="/admin/users/{id}",
  *      operationId="getUserByID",
  *      tags={"Admin User"},
  *      summary="Get user by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="userID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/users/{id}?_method=PATCH
  *
  * @OA\post(
  *      path="/admin/users/{id}?_method=PATCH",
  *      operationId="updateUserByID",
  *      tags={"Admin User"},
  *      summary="Update user by ID",
  *      description="[TIPS] You may update tutor/student/parent value using this endpoint by add it's value attributes on request body",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\MediaType(
  *       mediaType="multipart/form-data",
  * @OA\Schema(ref="#/components/schemas/UserUpdateRequest")
  *    )
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="userID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/users/{id}
  *
  * @OA\delete(
  *      path="/admin/users/{id}",
  *      operationId="deleteUserByID",
  *      tags={"Admin User"},
  *      summary="Delete user by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="userID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/users/{id}/restore
  *
  * @OA\post(
  *      path="/admin/users/{id}/restore",
  *      tags={"Admin User"},
  *      summary="Restore archived user",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="userID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// USER TUTOR
 /**
  * Route api/tutors/
  *
  * @OA\Get(
  *      path="/admin/tutors/",
  *      operationId="getAllTutors",
  *      tags={"Admin Tutor"},
  *      summary="Get all tutor",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/tutors/select2
  *
  * @OA\Get(
  *      path="/admin/tutors/select2",
  *      tags={"Admin Tutor"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/tutors/{id}
  *
  * @OA\Get(
  *      path="/admin/tutors/{id}",
  *      operationId="getTutorByID",
  *      tags={"Admin Tutor"},
  *      summary="Get tutors by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="tutorID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/tutors/{id}
  *
  * @OA\patch(
  *      path="/admin/tutors/{id}",
  *      operationId="updateTutorByID",
  *      tags={"Admin Tutor"},
  *      summary="Update tutor by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/TutorRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="tutorID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// USER STUDENT
 /**
  * Route api/students/
  *
  * @OA\Get(
  *      path="/admin/students/",
  *      operationId="getAllStudents",
  *      tags={"Admin Student"},
  *      summary="Get all student",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/students/select2
  *
  * @OA\Get(
  *      path="/admin/students/select2",
  *      tags={"Admin Student"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/students/{id}
  *
  * @OA\Get(
  *      path="/admin/students/{id}",
  *      operationId="getStudentByID",
  *      tags={"Admin Student"},
  *      summary="Get students by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="studentID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/students/{id}
  *
  * @OA\patch(
  *      path="/admin/students/{id}",
  *      operationId="updateStudentByID",
  *      tags={"Admin Student"},
  *      summary="Update student by UserID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/StudentRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="studentID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// USER PARENT
 /**
  * Route api/parents/
  *
  * @OA\Get(
  *      path="/admin/parents/",
  *      operationId="getAllParents",
  *      tags={"Admin Parent"},
  *      summary="Get all parent",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/parents/select2
  *
  * @OA\Get(
  *      path="/admin/parents/select2",
  *      tags={"Admin Parent"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/parents/{id}
  *
  * @OA\Get(
  *      path="/admin/parents/{id}",
  *      operationId="getParentByID",
  *      tags={"Admin Parent"},
  *      summary="Get parents by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="parentID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/parents/{id}
  *
  * @OA\patch(
  *      path="/admin/parents/{id}",
  *      operationId="updateParentByID",
  *      tags={"Admin Parent"},
  *      summary="Update parent by UserID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/ParentRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="parentID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/parents/{id}/students
  *
  * @OA\Post(
  *      path="/admin/parents/{id}/students",
  *      tags={"Admin Parent"},
  *      summary="Create record student that child of parent",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"student_id"},
  * @OA\Property(
  *          property="student_id",
  *          type="integer",
  *          format="int32",
  *          example=1,
  *        ),
  *     )
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="parentID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route /admin/parents/{id}/students/{student_id}
  *
  * @OA\delete(
  *      path="/admin/parents/{id}/students/{student_id}",
  *      tags={"Admin Parent"},
  *      summary="Delete record student that child of parent",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="parentID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Parameter(
  *     name="student_id", required=true, in="path",
  *     description="studentID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// BRANCH
 /**
  * Route api/branches/
  *
  * @OA\Get(
  *      path="/admin/branches/",
  *      tags={"Admin Branch"},
  *      summary="Get all branch",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/branches/select2
  *
  * @OA\Get(
  *      path="/admin/branches/select2",
  *      tags={"Admin Branch"},
  *      summary="Get select2 from branch",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/branches
  *
  * @OA\Post(
  *      path="/admin/branches",
  *      tags={"Admin Branch"},
  *      summary="Create branch",
  *      description="``{{''{''SAMPLE_RULE'': {''type'': ''string'',''value'': ''This rule is for sample''}}``",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/BranchRequest")
  *  ),
  * @OA\Response(response=201,                                description="Successful operation"),
  * @OA\Response(response=400,                                description="Bad request"),
  *     )
  */
 /**
  * Route api/branches/{id}
  *
  * @OA\Get(
  *      path="/admin/branches/{id}",
  *      tags={"Admin Branch"},
  *      summary="Get branch by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/branches/{id}
  *
  * @OA\patch(
  *      path="/admin/branches/{id}",
  *      tags={"Admin Branch"},
  *      summary="Update branch by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/BranchRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/branches/{id}
  *
  * @OA\delete(
  *      path="/admin/branches/{id}",
  *      tags={"Admin Branch"},
  *      summary="Delete branch by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/branches/configs
  *
  * @OA\Post(
  *      path="/admin/branches/configs",
  *      tags={"Admin Branch"},
  *      summary="Create branch configuration for all",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/BranchConfigRequest")
  *  ),
  * @OA\Response(response=201,                                description="Successful operation"),
  * @OA\Response(response=400,                                description="Bad request"),
  *     )
  */
 /**
  * Route api/branches/{id}/configs
  *
  * @OA\Post(
  *      path="/admin/branches/{id}/configs",
  *      tags={"Admin Branch"},
  *      summary="Create branch specified cofiguration",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/BranchConfigRequest")
  *  ),
  * @OA\Response(response=201,                                description="Successful operation"),
  * @OA\Response(response=400,                                description="Bad request"),
  *     )
  */
 /**
  * Route api/branches/configs
  *
  * @OA\delete(
  *      path="/admin/branches/configs",
  *      tags={"Admin Branch"},
  *      summary="Delete all branch configuration by key that stored on branch_config",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"key"},
  * @OA\Property(
  *          property="key",
  *          type="string",
  *          example="SAMPLE_RULE",
  *        ),
  *     ),
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
  /**
  * Route api/branches/{id}/configs
  *
  * @OA\delete(
  *      path="/admin/branches/{id}/configs",
  *      tags={"Admin Branch"},
  *      summary="Delete specified branch configuration by key",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"key"},
  * @OA\Property(
  *          property="key",
  *          type="string",
  *          example="SAMPLE_RULE",
  *        ),
  *     ),
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// SCHOOL
 /**
  * Route api/schools/
  *
  * @OA\Get(
  *      path="/admin/schools/",
  *      tags={"Admin School"},
  *      summary="Get all school",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/schools/select2
  *
  * @OA\Get(
  *      path="/admin/schools/select2",
  *      tags={"Admin School"},
  *      summary="Get select2 from school",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/schools
  *
  * @OA\Post(
  *      path="/admin/schools",
  *      tags={"Admin School"},
  *      summary="Create school",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SchoolRequest")
  *  ),
  * @OA\Response(response=201,                                 description="Successful operation"),
  * @OA\Response(response=400,                                 description="Bad request"),
  *     )
  */
 /**
  * Route api/schools/{id}
  *
  * @OA\Get(
  *      path="/admin/schools/{id}",
  *      tags={"Admin School"},
  *      summary="Get school by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="schoolID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/schools/{id}
  *
  * @OA\patch(
  *      path="/admin/schools/{id}",
  *      tags={"Admin School"},
  *      summary="Update school by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SchoolRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="schoolID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/schools/{id}
  *
  * @OA\delete(
  *      path="/admin/schools/{id}",
  *      tags={"Admin School"},
  *      summary="Delete school by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="schoolID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// PROFESSION
 /**
  * Route api/professions/
  *
  * @OA\Get(
  *      path="/admin/professions/",
  *      tags={"Admin Profession"},
  *      summary="Get all professions",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/professions/select2
  *
  * @OA\Get(
  *      path="/admin/professions/select2",
  *      tags={"Admin Profession"},
  *      summary="Get select2 from professions",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/professions
  *
  * @OA\Post(
  *      path="/admin/professions",
  *      tags={"Admin Profession"},
  *      summary="Create professions",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/ProfessionRequest")
  *  ),
  * @OA\Response(response=201,                                 description="Successful operation"),
  * @OA\Response(response=400,                                 description="Bad request"),
  *     )
  */
 /**
  * Route api/professions/{id}
  *
  * @OA\Get(
  *      path="/admin/professions/{id}",
  *      tags={"Admin Profession"},
  *      summary="Get professions by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="professionID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/professions/{id}
  *
  * @OA\patch(
  *      path="/admin/professions/{id}",
  *      tags={"Admin Profession"},
  *      summary="Update professions by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/ProfessionRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="professionID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/professions/{id}
  *
  * @OA\delete(
  *      path="/admin/professions/{id}",
  *      tags={"Admin Profession"},
  *      summary="Delete professions by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="professionID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// SUBJECT
 /**
  * Route api/subjects/
  *
  * @OA\Get(
  *      path="/admin/subjects/",
  *      operationId="getAllSubject",
  *      tags={"Admin Subject"},
  *      summary="Get all subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/subjects/select2
  *
  * @OA\Get(
  *      path="/admin/subjects/select2",
  *      tags={"Admin Subject"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/subjects
  *
  * @OA\Post(
  *      path="/admin/subjects",
  *      operationId="createSubject",
  *      tags={"Admin Subject"},
  *      summary="Create subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SubjectRequest")
  *  ),
  * @OA\Response(response=201,                                 description="Successful operation"),
  * @OA\Response(response=400,                                 description="Bad request"),
  *     )
  */
 /**
  * Route api/subjects/{id}
  *
  * @OA\Get(
  *      path="/admin/subjects/{id}",
  *      operationId="getSubjectByID",
  *      tags={"Admin Subject"},
  *      summary="Get subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/subjects/{id}
  *
  * @OA\patch(
  *      path="/admin/subjects/{id}",
  *      operationId="updateSubjectByID",
  *      tags={"Admin Subject"},
  *      summary="Update subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SubjectRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/subjects/{id}
  *
  * @OA\delete(
  *      path="/admin/subjects/{id}",
  *      operationId="deleteSubjectByID",
  *      tags={"Admin Subject"},
  *      summary="Delete subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/subjects/{id}/stages
  *
  * @OA\Post(
  *      path="/admin/subjects/{id}/stages",
  *      tags={"Admin Subject"},
  *      summary="Create record stage related to subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"stage_id"},
  * @OA\Property(
  *          property="stage_id",
  *          type="integer",
  *          format="int32",
  *          example=1,
  *        ),
  *     )
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route /admin/subjects/{id}/stages/{stage_id}
  *
  * @OA\delete(
  *      path="/admin/subjects/{id}/stages/{stage_id}",
  *      tags={"Admin Subject"},
  *      summary="Delete record stage related to subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Parameter(
  *     name="stage_id", required=true, in="path",
  *     description="stageID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// STAGE
 /**
  * Route api/stages/
  *
  * @OA\Get(
  *      path="/admin/stages/",
  *      operationId="getAllStage",
  *      tags={"Admin Stage"},
  *      summary="Get all subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/stages/select2
  *
  * @OA\Get(
  *      path="/admin/stages/select2",
  *      tags={"Admin Stage"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/stages
  *
  * @OA\Post(
  *      path="/admin/stages",
  *      operationId="createStage",
  *      tags={"Admin Stage"},
  *      summary="Create subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/StageRequest")
  *  ),
  * @OA\Response(response=201,                               description="Successful operation"),
  * @OA\Response(response=400,                               description="Bad request"),
  *     )
  */
 /**
  * Route api/stages/{id}
  *
  * @OA\Get(
  *      path="/admin/stages/{id}",
  *      operationId="getStageByID",
  *      tags={"Admin Stage"},
  *      summary="Get subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/stages/{id}
  *
  * @OA\patch(
  *      path="/admin/stages/{id}",
  *      operationId="updateStageByID",
  *      tags={"Admin Stage"},
  *      summary="Update subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/StageRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/stages/{id}
  *
  * @OA\delete(
  *      path="/admin/stages/{id}",
  *      operationId="deleteStageByID",
  *      tags={"Admin Stage"},
  *      summary="Delete subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// LEVEL
 /**
  * Route api/levels/
  *
  * @OA\Get(
  *      path="/admin/levels/",
  *      operationId="getAllLevel",
  *      tags={"Admin Level"},
  *      summary="Get all subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="stage", in="query",
  *     description="filter by stageId"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/levels/select2
  *
  * @OA\Get(
  *      path="/admin/levels/select2",
  *      tags={"Admin Level"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="stage", in="query",
  *     description="filter by stageId"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/levels
  *
  * @OA\Post(
  *      path="/admin/levels",
  *      operationId="createLevel",
  *      tags={"Admin Level"},
  *      summary="Create subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/LevelRequest")
  *  ),
  * @OA\Response(response=201,                               description="Successful operation"),
  * @OA\Response(response=400,                               description="Bad request"),
  *     )
  */
 /**
  * Route api/levels/{id}
  *
  * @OA\Get(
  *      path="/admin/levels/{id}",
  *      operationId="getLevelByID",
  *      tags={"Admin Level"},
  *      summary="Get subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/levels/{id}
  *
  * @OA\patch(
  *      path="/admin/levels/{id}",
  *      operationId="updateLevelByID",
  *      tags={"Admin Level"},
  *      summary="Update subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/LevelRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/levels/{id}
  *
  * @OA\delete(
  *      path="/admin/levels/{id}",
  *      operationId="deleteLevelByID",
  *      tags={"Admin Level"},
  *      summary="Delete subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/levels/{id}/substrands
  *
  * @OA\Post(
  *      path="/admin/levels/{id}/substrands",
  *      tags={"Admin Level"},
  *      summary="Create record substrand related to level",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"substrand_id"},
  * @OA\Property(
  *          property="substrand_id",
  *          type="integer",
  *          format="int32",
  *          example=1,
  *        ),
  *     )
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="levelID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route /admin/levels/{id}/substrands/{substrand_id}
  *
  * @OA\delete(
  *      path="/admin/levels/{id}/substrands/{substrand_id}",
  *      tags={"Admin Level"},
  *      summary="Delete record substrand related to level",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="levelID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Parameter(
  *     name="substrand_id", required=true, in="path",
  *     description="substrandID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// STRAND
 /**
  * Route api/strands/
  *
  * @OA\Get(
  *      path="/admin/strands/",
  *      operationId="getAllStrand",
  *      tags={"Admin Strand"},
  *      summary="Get all subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/strands/select2
  *
  * @OA\Get(
  *      path="/admin/strands/select2",
  *      tags={"Admin Strand"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/strands
  *
  * @OA\Post(
  *      path="/admin/strands",
  *      operationId="createStrand",
  *      tags={"Admin Strand"},
  *      summary="Create subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/StrandRequest")
  *  ),
  * @OA\Response(response=201,                                description="Successful operation"),
  * @OA\Response(response=400,                                description="Bad request"),
  *     )
  */
 /**
  * Route api/strands/{id}
  *
  * @OA\Get(
  *      path="/admin/strands/{id}",
  *      operationId="getStrandByID",
  *      tags={"Admin Strand"},
  *      summary="Get subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/strands/{id}
  *
  * @OA\patch(
  *      path="/admin/strands/{id}",
  *      operationId="updateStrandByID",
  *      tags={"Admin Strand"},
  *      summary="Update subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/StrandRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/strands/{id}
  *
  * @OA\delete(
  *      path="/admin/strands/{id}",
  *      operationId="deleteStrandByID",
  *      tags={"Admin Strand"},
  *      summary="Delete subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// SUBSTRAND
 /**
  * Route api/substrands/
  *
  * @OA\Get(
  *      path="/admin/substrands/",
  *      operationId="getAllSubstrand",
  *      tags={"Admin Substrand"},
  *      summary="Get all subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="strand", in="query",
  *     description="strandId"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/substrands/select2
  *
  * @OA\Get(
  *      path="/admin/substrands/select2",
  *      tags={"Admin Substrand"},
  *      summary="Get select2 from subject",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="strand", in="query",
  *     description="strandId"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/substrands
  *
  * @OA\Post(
  *      path="/admin/substrands",
  *      operationId="createSubstrand",
  *      tags={"Admin Substrand"},
  *      summary="Create subject",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SubstrandRequest")
  *  ),
  * @OA\Response(response=201,                                   description="Successful operation"),
  * @OA\Response(response=400,                                   description="Bad request"),
  *     )
  */
 /**
  * Route api/substrands/{id}
  *
  * @OA\Get(
  *      path="/admin/substrands/{id}",
  *      operationId="getSubstrandByID",
  *      tags={"Admin Substrand"},
  *      summary="Get subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/substrands/{id}
  *
  * @OA\patch(
  *      path="/admin/substrands/{id}",
  *      operationId="updateSubstrandByID",
  *      tags={"Admin Substrand"},
  *      summary="Update subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/SubstrandRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/substrands/{id}
  *
  * @OA\delete(
  *      path="/admin/substrands/{id}",
  *      operationId="deleteSubstrandByID",
  *      tags={"Admin Substrand"},
  *      summary="Delete subject by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="subjectID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// CLASS
 /**
  * Route api/classes/
  *
  * @OA\Get(
  *      path="/admin/classes/",
  *      tags={"Admin Class"},
  *      summary="Get all class",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Parameter(
  *     name="branch", in="query",
  *     description="filter by branchId"),
  * @OA\Parameter(
  *     name="archive", in="query",
  *     description="archived record (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/classes
  *
  * @OA\Post(
  *      path="/admin/classes",
  *      tags={"Admin Class"},
  *      summary="Create class",
  *      description="Use this value in day as enum ('monday','tuesday','wednesday','thursday','friday','saturday','sunday')",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/ClassRequest")
  *  ),
  * @OA\Response(response=201,                               description="Successful operation"),
  * @OA\Response(response=400,                               description="Bad request"),
  *     )
  */
 /**
  * Route api/classes/{id}
  *
  * @OA\Get(
  *      path="/admin/classes/{id}",
  *      tags={"Admin Class"},
  *      summary="Get class by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/classes/{id}
  *
  * @OA\patch(
  *      path="/admin/classes/{id}",
  *      tags={"Admin Class"},
  *      summary="Update class by ID",
  *      description="Use this value in day as enum ('monday','tuesday','wednesday','thursday','friday','saturday','sunday')",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/ClassRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/classes/{id}
  *
  * @OA\delete(
  *      path="/admin/classes/{id}",
  *      tags={"Admin Class"},
  *      summary="Delete class by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/classes/{id}/people
  *
  * @OA\Get(
  *      path="/admin/classes/{id}/people",
  *      tags={"Admin Class"},
  *      summary="Get people of class by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/classes/{id}/people
  *
  * @OA\patch(
  *      path="/admin/classes/{id}/people",
  *      tags={"Admin Class"},
  *      summary="Update people of class by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(
  *        type="object",
  *        required={"tutor_id","student_id"},
  * @OA\Property(
  *          property="tutor_id",
  *          example="[]",
  *        ),
  * @OA\Property(
  *          property="student_id",
  *          example="[]",
  *        ),
  *     )
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/classes/{id}/restore
  *
  * @OA\post(
  *      path="/admin/classes/{id}/restore",
  *      tags={"Admin Class"},
  *      summary="Restore archived class",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="classID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// ROLE
 /**
  * Route api/roles/
  *
  * @OA\Get(
  *      path="/admin/roles/",
  *      operationId="getAllRole",
  *      tags={"Admin Role"},
  *      summary="Get all role",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/roles/select2
  *
  * @OA\Get(
  *      path="/admin/roles/select2",
  *      tags={"Admin Role"},
  *      summary="Get select2 from role",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/roles
  *
  * @OA\Post(
  *      path="/admin/roles",
  *      operationId="createRole",
  *      tags={"Admin Role"},
  *      summary="Create role",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/RoleRequest")
  *  ),
  * @OA\Response(response=201,                              description="Successful operation"),
  * @OA\Response(response=400,                              description="Bad request"),
  *     )
  */
 /**
  * Route api/roles/{id}
  *
  * @OA\Get(
  *      path="/admin/roles/{id}",
  *      operationId="getRoleByID",
  *      tags={"Admin Role"},
  *      summary="Get role by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="roleID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/roles/{id}
  *
  * @OA\patch(
  *      path="/admin/roles/{id}",
  *      operationId="updateRoleByID",
  *      tags={"Admin Role"},
  *      summary="Update role by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/RoleRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="roleID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/roles/{id}
  *
  * @OA\delete(
  *      path="/admin/roles/{id}",
  *      operationId="deleteRoleByID",
  *      tags={"Admin Role"},
  *      summary="Delete branch by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/roles/{id}/permissions
  *
  * @OA\Get(
  *      path="/admin/roles/{id}/permissions",
  *      operationId="getPermissionsByRoleID",
  *      tags={"Admin Role"},
  *      summary="Get permissions of role by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="roleID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/roles/{id}/permissions
  *
  * @OA\patch(
  *      path="/admin/roles/{id}/permissions",
  *      operationId="updatePermissionsByRoleID",
  *      tags={"Admin Role"},
  *      summary="Update permissions of role by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/RolePermissionRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="roleID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
// PERMISSION
 /**
  * Route api/permissions/
  *
  * @OA\Get(
  *      path="/admin/permissions/",
  *      operationId="getAllPermission",
  *      tags={"Admin Permission"},
  *      summary="Get all permission",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="page", in="query",
  *     description="go to specific page"),
  * @OA\Parameter(
  *     name="size", in="query",
  *     description="amount of record in a page"),
  * @OA\Parameter(
  *     name="search", in="query",
  *     description="search keyword"),
  * @OA\Parameter(
  *     name="sort", in="query",
  *     description="sort by column (look at its table)"),
  * @OA\Parameter(
  *     name="sort_asc", in="query",
  *     description="sort by ascending (1 or 0)"),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/permissions/select2
  *
  * @OA\Get(
  *      path="/admin/permissions/select2",
  *      tags={"Admin Permission"},
  *      summary="Get select2 from permission",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/permissions
  *
  * @OA\Post(
  *      path="/admin/permissions",
  *      operationId="createPermission",
  *      tags={"Admin Permission"},
  *      summary="Create permission",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/PermissionRequest")
  *  ),
  * @OA\Response(response=201,                                    description="Successful operation"),
  * @OA\Response(response=400,                                    description="Bad request"),
  *     )
  */
 /**
  * Route api/permissions/{id}
  *
  * @OA\Get(
  *      path="/admin/permissions/{id}",
  *      operationId="getPermissionByID",
  *      tags={"Admin Permission"},
  *      summary="Get permission by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="permissionID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/permissions/{id}
  *
  * @OA\patch(
  *      path="/admin/permissions/{id}",
  *      operationId="updatePermissionByID",
  *      tags={"Admin Permission"},
  *      summary="Update permission by ID",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\JsonContent(ref="#/components/schemas/PermissionRequest")
  *  ),
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="permissionID",
  * @OA\Schema(type="string")
  *  ),
  *
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
 /**
  * Route api/permissions/{id}
  *
  * @OA\delete(
  *      path="/admin/permissions/{id}",
  *      operationId="deletePermissionByID",
  *      tags={"Admin Permission"},
  *      summary="Delete branch by ID",
  *      security={{"bearer_token":{}}},
  * @OA\Parameter(
  *     name="id", required=true, in="path",
  *     description="branchID",
  * @OA\Schema(type="string")
  *  ),
  * @OA\Response(response=204, description="Successful operation without content"),
  * @OA\Response(response=400, description="Bad request"),
  * )
  */
