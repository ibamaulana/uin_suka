<?php
/**
 * Anotations
 * php version 7.4.16
 *
 * @category Annotation
 * @package  Annotation
 * @author   smartjen <helo@smartjen.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0.html Apache License, Version 2.0
 * @link     http://smartjen.com
 */

/**
 * Swagger Info
 *
 * @OA\Info(
 *     title="Smartjen 2",
 *     version="1.0.0",
 * @OA\Contact(
 *         email="helo@smartjen.com"
 *     ),
 * @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */

/**
 * Swagger server target
 *
 * @OA\Server(
 *      url="http://localhost:8000/api/",
 *     description="Smartjen 2 Backend using Artisan Serve"
 * )
 * @OA\Server(
 *      url="http://smartjen2.test/api/",
 *      description="Smartjen 2 Backend using Laragon"
 * )
 */

/**
 * Swagger security scheme
 *
 * @OA\SecurityScheme(
 *     type="http",
 *     in="header",
 *     name="Authorization",
 *     securityScheme="bearer_token",
 *     scheme="bearer",
 * )
 *
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     description="Use a global client_id / client_secret and your username / password combo to obtain a token",
 *     name="Password Based",
 *     in="header",
 *     scheme="https",
 *     securityScheme="Password Based",
 * @OA\Flow(
 *         flow="password",
 *         authorizationUrl="/oauth/authorize",
 *         tokenUrl="/oauth/token",
 *         refreshUrl="/oauth/token/refresh",
 *         scopes={}
 *     )
 * )
 *
 * @OA\OpenApi(
 *   security={
 *     {
 *       "oauth2": {"read:oauth2"},
 *     }
 *   }
 * )
 */

// AUTHENTICATION
 /**
  * Route api/login
  *
  * @OA\Post(
  *      path="/login",
  *      operationId="loginUser",
  *      tags={"Authentication"},
  *      summary="Login user",
  * @OA\RequestBody(
  *          required=true,
  * @OA\JsonContent(ref="#/components/schemas/LoginRequest")
  *      ),
  * @OA\Response(response=200,                               description="Successful operation"),
  * @OA\Response(response=400,                               description="Bad request"),
  *     )
  */
 /**
  * Route api/register
  *
  * @OA\Post(
  *      path="/register",
  *      operationId="registerUser",
  *      tags={"Authentication"},
  *      summary="Register user",
  * @OA\RequestBody(
  *          required=true,
  * @OA\JsonContent(ref="#/components/schemas/RegisterRequest")
  *      ),
  * @OA\Response(response=200,                                  description="Successful operation"),
  * @OA\Response(response=400,                                  description="Bad request"),
  *     )
  */

//USER
 /**
  * Route api/user
  *
  * @OA\Get(
  *      path="/user",
  *      operationId="userData",
  *      tags={"User"},
  *      summary="Get user information",
  *      security={{"bearer_token":{}}},
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/settings/password
  *
  * @OA\Patch(
  *      path="/settings/password",
  *      operationId="userUpdatePassword",
  *      tags={"User"},
  *      summary="Update user password",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *          required=true,
  * @OA\JsonContent(ref="#/components/schemas/PasswordUpdateRequest")
  *      ),
  * @OA\Response(response=204,                                        description="Successful operation without content"),
  * @OA\Response(response=400,                                        description="Bad request"),
  *     )
  */
 /**
  * Route api/settings/profile?_method=PATCH
  *
  * @OA\Post(
  *      path="/settings/profile?_method=PATCH",
  *      operationId="userUpdateProfile",
  *      tags={"User"},
  *      summary="Update user profile",
  *      security={{"bearer_token":{}}},
  * @OA\RequestBody(
  *      required=true,
  * @OA\MediaType(
  *        mediaType="multipart/form-data",
  * @OA\Schema(ref="#/components/schemas/ProfileUpdateRequest")
  *        )
  *     ),
  * @OA\Response(response=200,                                  description="Successful operation"),
  * @OA\Response(response=400,                                  description="Bad request"),
  *     )
  */
// PRIVILAGE
 /**
  * Route api/privileges/roles
  *
  * @OA\Get(
  *      path="/privileges/roles",
  *      operationId="getRoles",
  *      tags={"Privilage"},
  *      summary="Get all roles and permissions",
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
 /**
  * Route api/privileges/permissions
  *
  * @OA\Get(
  *      path="/privileges/permissions",
  *      operationId="getPerms",
  *      tags={"Privilage"},
  *      summary="Get all permissions",
  * @OA\Response(response=200, description="Successful operation"),
  * @OA\Response(response=400, description="Bad request"),
  *     )
  */
