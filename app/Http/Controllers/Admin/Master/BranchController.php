<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\Branch\BranchRequest;
use App\Http\Requests\Admin\Master\Branch\BranchConfigRequest;
use App\Repositories\BranchRepository;
use App\Repositories\BranchConfigRepository;


class BranchController extends BaseController
{
    protected $branchRepository;
    protected $configRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->branchRepository = new BranchRepository;
        $this->configRepository = new BranchConfigRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $branches  = $this->branchRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $branches,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\BranchRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        $branches = $this->branchRepository->create($request->all());
        if (!$branches) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $branches,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branches = $this->branchRepository->getById($id);
        if (!$branches) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $branches,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\BranchRequest $request
     * @param int                            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request, $id)
    {
        $branches = $this->branchRepository->update($request->all(), $id);
        if (!$branches) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $branches,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branches = $this->branchRepository->delete($id);
        if (!$branches) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->branchRepository->select2('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Endpoint to create new key to all branches and stored in branch_config.
     * value key always be uppercase
     * @method = POST /api/admin/branches/configs
     *
     * @param  BranchConfigRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function addGeneralConfig(BranchConfigRequest $request)
    {
        $data = $this->configRepository->createToAll($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Endpoint to create new key at specific branches
     * value key always be uppercase
     * @method = POST /api/admin/branches/{id}/configs
     *
     * @param  BranchConfigRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function addSpecificConfig(BranchConfigRequest $request, $id)
    {
        $data = $this->configRepository->createSpecific($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    public function destroyGeneralConfig(Request $request) {
        $request->validate(['key'=>'required']);
        $data = $this->configRepository->deleteToAll($request->key);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    public function destroySpecificConfig(Request $request, $id)
    {
        $request->validate(['key'=>'required']);
        $data = $this->configRepository->deleteByBranchIdAndKey($id, $request->key);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
