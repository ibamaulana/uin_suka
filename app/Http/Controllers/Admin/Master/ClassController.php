<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\ClassRequest;
use App\Repositories\ClassRepository;


class ClassController extends BaseController
{
    protected $classRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->classRepository = new ClassRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data  = $this->classRepository->getAllWithOption($size, $search, $sortField, $sortAsc, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\ClassRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ClassRequest $request)
    {
        $data = $this->classRepository->create($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->classRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\ClassRequest $request
     * @param int                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ClassRequest $request, $id)
    {
        $data = $this->classRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->classRepository->forceDelete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Restore the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function restore(int $id)
    {
        $data = $this->classRepository->restore($id);

        if (!$data) {
            return $this->sendError(__('response.FailedRestoreData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessRestoreData'), 200
        );
    }

    /**
     * Get all student and tutor that asociate on this class
     * Endpoint: /classes/{id}/people
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getClassPeople($id)
    {
        $data = $this->classRepository->getClassPeopleByClassId($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update all student and tutor that asociate on this class
     * Endpoint: /classes/{id}/people
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function patchClassPeople(Request $request, $id)
    {
        $data = $this->classRepository->updateClassPeopleByClassId($id, $request->only('student_id', 'tutor_id'));
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data
            ], __('response.SuccessUpdateData'), 200
        );
    }
}
