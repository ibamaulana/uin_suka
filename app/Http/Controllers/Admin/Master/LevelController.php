<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use App\Repositories\LevelRepository;
use App\Repositories\LevelSubstrandRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\LevelRequest;

class LevelController extends BaseController
{
    protected $levelRepository;
    protected $levelSubstrandRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->levelRepository = new LevelRepository;
        $this->levelSubstrandRepository = new LevelSubstrandRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $stageId   = $request->input('stage', null);

        $data  = $this->levelRepository->getAllWithStage($stageId, $size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\LevelRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(LevelRequest $request)
    {
        $data = $this->levelRepository->create($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->levelRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(LevelRequest $request, $id)
    {
        $data = $this->levelRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->levelRepository->delete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $stageId = $request->input('stage', null);

        $data = $this->levelRepository->select2WithStage($stageId);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Create stage related to subject
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function createLevelSubstrand(Request $request, $id)
    {
        $this->validate($request, ['substrand_id' => 'required']);
        $data = $this->levelSubstrandRepository->create(
            [
            "level_id" => $id,
            "substrand_id" => $request->substrand_id,
            ]
        );
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Delete stage related to subject
     *
     * @param int $id
     * @param int $stageId
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteLevelSubstrand($id, $stageId)
    {
        $data = $this->levelSubstrandRepository->deleteByLevelIdAndSubstrandId($id, $stageId);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
