<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use App\Repositories\SchoolRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\SchoolRequest;
use App\Services\UploadService;

class SchoolController extends BaseController
{
    protected $schoolRepository;
    protected $uploadService;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->schoolRepository = new SchoolRepository;
        $this->uploadService = new UploadService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $data  = $this->schoolRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SchoolRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $input = $request->all();
        if ($request->has('picture')) {
            $picturePath = $this->uploadService->uploadImage('public/school-picture', $request->picture);
            $input['picture'] = $picturePath;
        }
        $data = $this->schoolRepository->create($input);
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->schoolRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SchoolRequest $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolRequest $request, $id)
    {
        $input = $request->all();
        if ($request->has('picture')) {
            $user = $this->schoolRepository->find($id);
            $schoolPicture = $user->picture;
            if ($schoolPicture) {
                $this->uploadService->deleteImage($schoolPicture);
            }
            $picturePath = $this->uploadService->uploadImage('public/school-picture', $request->picture);
            $input['picture'] = $picturePath;
        }
        $data = $this->schoolRepository->update($input, $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->schoolRepository->delete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->schoolRepository->select2('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
