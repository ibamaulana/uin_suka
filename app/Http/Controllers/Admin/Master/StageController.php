<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use App\Repositories\StageRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\StageRequest;

class StageController extends BaseController
{
    protected $stageRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->stageRepository = new StageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $data  = $this->stageRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\Master\StageRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StageRequest $request)
    {
        $data = $this->stageRepository->create($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->stageRepository->getById($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StageRequest $request, $id)
    {
        $this->validate(
            $request, [
            'sortorder' => 'unique:stages,sortorder,'.$id,
            ]
        );
        $data = $this->stageRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->stageRepository->delete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->stageRepository->select2('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
