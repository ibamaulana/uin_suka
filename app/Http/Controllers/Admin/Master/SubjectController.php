<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use App\Repositories\SubjectRepository;
use App\Repositories\SubjectStageRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\SubjectRequest;

class SubjectController extends BaseController
{
    protected $subjectRepository;
    protected $subjectStageRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->subjectRepository = new SubjectRepository;
        $this->subjectStageRepository = new SubjectStageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $data  = $this->subjectRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubjectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        $data = $this->subjectRepository->create($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->subjectRepository->getWithStage($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectRequest $request, $id)
    {
        $data = $this->subjectRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->subjectRepository->delete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->subjectRepository->select2('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Create stage related to subject
     *
     * @param \Illuminate\Http\ParentRequest $request
     * @param int                            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function createSubjectStage(Request $request, $id)
    {
        $this->validate($request, ['stage_id' => 'required']);
        $data = $this->subjectStageRepository->create(
            [
            "subject_id" => $id,
            "stage_id" => $request->stage_id,
            ]
        );
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Delete stage related to subject
     *
     * @param int $id
     * @param int $stageId
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSubjectStage($id, $stageId)
    {
        $data = $this->subjectStageRepository->deleteBySubjectIdAndStageId($id, $stageId);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
