<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\BaseController;
use App\Repositories\SubstrandRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Master\SubstrandRequest;

class SubstrandController extends BaseController
{
    protected $substrandRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->substrandRepository = new SubstrandRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $strandId = $request->input('strand', null);

        $data  = $this->substrandRepository->getAllWithStrand($strandId, $size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubstrandRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SubstrandRequest $request)
    {
        $data = $this->substrandRepository->create($request->all());
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->substrandRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\SubstrandRequest $request
     * @param int                               $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SubstrandRequest $request, $id)
    {
        $data = $this->substrandRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->substrandRepository->delete($id);
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $strandId = $request->input('strand', null);

        $data = $this->substrandRepository->select2WithStrand($strandId);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
