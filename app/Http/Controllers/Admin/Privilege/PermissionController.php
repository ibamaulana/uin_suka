<?php

namespace App\Http\Controllers\Admin\Privilege;

use App\Http\Controllers\BaseController;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Privilege\PermissionRequest;

class PermissionController extends BaseController
{
    protected $roleRepository;
    protected $permissionRepository;

    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->roleRepository = new RoleRepository;
        $this->permissionRepository = new PermissionRepository;
    }

    /**
     * Get all permissions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function permissions()
    {
        $permissions = $this->permissionRepository->all();
        return $this->sendResponse(
            $permissions, __('response.SuccessGetData'), 200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $permissions  = $this->permissionRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $permissions,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\PermissionRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $permissions = $this->permissionRepository->create($request->all());
        if (!$permissions) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $permissions,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permissions = $this->permissionRepository->find($id);
        if (!$permissions) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $permissions,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\PermissionRequest $request
     * @param int                                $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        $permissions = $this->permissionRepository->update($request->all(), $id);
        if (!$permissions) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $permissions,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permissions = $this->permissionRepository->delete($id);
        if (!$permissions) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->permissionRepository->select2Identic('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
