<?php

namespace App\Http\Controllers\Admin\Privilege;

use App\Http\Controllers\BaseController;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Privilege\RoleRequest;
use App\Http\Requests\Admin\Privilege\RolePermissionRequest;

class RoleController extends BaseController
{
    protected $roleRepository;
    protected $permissionRepository;

    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->roleRepository = new RoleRepository;
        $this->permissionRepository = new PermissionRepository;
    }

    /**
     * Get all roles and permissions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function roles()
    {
        $rolePermissions = $this->roleRepository->getAllRolePermisisons();
        return $this->sendResponse(
            $rolePermissions, __('response.SuccessGetData'), 200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);
        $roles  = $this->roleRepository->getAll($size, $search, $sortField, $sortAsc);
        return $this->sendResponse(
            [
                'data' => $roles,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\RoleRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $roles = $this->roleRepository->create($request->all());
        if (!$roles) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $roles,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = $this->roleRepository->find($id);
        if (!$roles) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $roles,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\RoleRequest $request
     * @param int                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $roles = $this->roleRepository->update($request->all(), $id);
        if (!$roles) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $roles,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = $this->roleRepository->delete($id);
        if (!$roles) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Get all permissions that asociate on this role
     * Endpoint: /roles/{id}/permissions
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getRolePermission($id)
    {
        $perms = $this->roleRepository->getRolePermisison($id);
        if (!$perms) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $perms
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update all permissions that asociate on this role
     * Endpoint: /roles/{id}/permissions
     *
     * @param mixed $request
     * @param int   $id
     *
     * @return \Illuminate\Http\Response
     */
    public function patchUpdatePermission(RolePermissionRequest $request, $id)
    {
        $perms = $this->roleRepository->updateRolePermisison($id, $request->only('permissions'));
        if (!$perms) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $perms
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        $data = $this->roleRepository->select2Identic('name');
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
