<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\BaseController;
use App\Repositories\ParentRepository;
use App\Repositories\ParentStudentRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\User\ParentRequest;

class ParentController extends BaseController
{
    protected $parentRepository;
    protected $parentStudentRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->parentRepository = new ParentRepository;
        $this->parentStudentRepository = new ParentStudentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data  = $this->parentRepository->getAllWithOption($size, $search, $sortField, $sortAsc, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->parentRepository->getWithStudent($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\ParentRequest $request
     * @param int                            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ParentRequest $request, $id)
    {
        $data = $this->parentRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Create student related to parent
     *
     * @param \Illuminate\Http\ParentRequest $request
     * @param int                            $id
     *
     * @return \Illuminate\Http\Response
     */
    public function createStudentParent(Request $request, $id)
    {
        $this->validate($request, ['student_id' => 'required']);
        $parentStudent = $this->parentStudentRepository->create(
            [
            "parent_id" => $id,
            "student_id" => $request->student_id,
            ]
        );
        if (!$parentStudent) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $parentStudent,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Delete student related to parent
     *
     * @param int $id
     * @param int $studentId
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteStudentParent($id, $studentId)
    {
        $parentStudent = $this->parentStudentRepository->deleteByParentIdAndStudentId($id, $studentId);
        if (!$parentStudent) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data = $this->parentRepository->select2WithOption([
                    'branch' => $brachId,
                    'archive'=> $archive,
                ]);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
