<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\BaseController;
use App\Repositories\TutorRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\User\TutorRequest;

class TutorController extends BaseController
{
    protected $tutorRepository;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->tutorRepository = new TutorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data  = $this->tutorRepository->getAllWithOption($size, $search, $sortField, $sortAsc, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->tutorRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\TutorRequest $request
     * @param int                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(TutorRequest $request, $id)
    {
        $data = $this->tutorRepository->update($request->all(), $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data = $this->tutorRepository->select2WithOption([
                    'branch' => $brachId,
                    'archive'=> $archive,
                ]);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }
}
