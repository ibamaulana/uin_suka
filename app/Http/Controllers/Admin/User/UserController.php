<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\BaseController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\User\UserRequest;
use App\Http\Requests\Admin\User\UserUpdateRequest;
use App\Services\UploadService;

class UserController extends BaseController
{
    protected $userRepository;
    protected $uploadService;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository;
        $this->uploadService = new UploadService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data  = $this->userRepository->getAllWithOption($size, $search, $sortField, $sortAsc, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\UserRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $input = $request->all();
        if ($request->has('picture')) {
            $picturePath = $this->uploadService->uploadImage('public/profile-picture', $request->picture);
            $input['picture'] = $picturePath;
        }
        $data = $this->userRepository->createAccount($input);
        if (!$data) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->userRepository->find($id);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserUpdateRequest $request
     * @param int                                $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $this->validate(
            $request, [
            'username' => 'unique:users,username,'.$id,
            'email' => 'unique:users,email,'.$id,
            ]
        );
        $input = $request->all();
        if ($request->has('picture')) {
            $user = $this->userRepository->find($id);
            $userProfile = $user->picture;
            if ($userProfile) {
                $this->uploadService->deleteImage($userProfile);
            }
            $picturePath = $this->uploadService->uploadImage('public/profile-picture', $request->picture);
            $input['picture'] = $picturePath;
        }
        $data = $this->userRepository->update($input, $id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->userRepository->delete($id);
        $userProfile = $data->picture;
        if (!$data) {
            return $this->sendError(__('response.FailedDeleteData'));
        }
        if ($userProfile) {
            $this->uploadService->deleteImage($userProfile);
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Restore the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function restore(int $id)
    {
        $data = $this->userRepository->restore($id);

        if (!$data) {
            return $this->sendError(__('response.FailedRestoreData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessRestoreData'), 200
        );
    }

    /**
     * Select2 option of the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $archive   = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data = $this->userRepository->select2WithOption('name', [
                    'branch' => $brachId,
                    'archive'=> $archive,
                ]);
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Get included user user
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function include(Request $request)
    {
        $this->validate(
            $request, [
                'user_id' => 'required|array',
            ]);
        $size   = $request->input('size', 10);
        $search = $request->input('search', null);
        $role   = $request->input('role', null);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data    = $this->userRepository->getAllInclude($size, $search, $role, $request->user_id, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Get excluded user user
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function exclude(Request $request)
    {
        $this->validate(
            $request, [
                'user_id' => 'required|array',
            ]);
        $size   = $request->input('size', 10);
        $search = $request->input('search', null);
        $role   = $request->input('role', null);

        $archive = $request->input('archive', null);
        $brachId = $request->input('branch', null);
        $data    = $this->userRepository->getAllExclude($size, $search, $role, $request->user_id, [
                        'branch' => $brachId,
                        'archive'=> $archive,
                    ]);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }
}
