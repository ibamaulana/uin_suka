<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Mail\EmailVerif;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        //cek username
        $user = User::where('username',$request->username)->first();
        if(!$user){
            return $this->sendError('Email/NIK tidak terdaftar');
        }
        if(!$user->is_verified){
            return $this->sendError('Akun belum terverifikasi');
        }

        $token = Auth::guard()->attempt($request->only('username', 'password'));

        if (!$token) {
            return $this->sendError('Passsword salah!');
        }

        $user = Auth::guard()->user();

        Auth::guard()->setToken($token);

        $token = (string) Auth::guard()->getToken();
        $expiration = Auth::guard()->getPayload()->get('exp');

        return $this->sendResponse(
            [
                'user' => $user,
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => $expiration - time(),
            ],'success'
        );
    }

    public function register(Request $request)
    {
        if($request->status === 'mahasiswa' || $request->status === 'dosen'){
            $username = $request->nik;
            $email = $request->nik.($request->status === 'mahasiswa' ? '@student.uin-suka.ac.id' : '@uin-suka.ac.id');
        }else{
            $username = $request->email;
            $email = $request->email;
        }
        
        //cek nik
        $user = User::where('username',$username)->first();
        if($user){
            return $this->sendError('Email/NIK sudah terdaftar');
        }

        //register
        $user = new User();
        $user->name = $request->name;
        $user->username = $username;
        $user->email = $email;
        $user->nik = $request->nik;
        $user->level = 2;
        $user->status = $request->status;
        $user->password = bcrypt($request->password);
        $user->is_verified = 0;
        $user->save();

        //email
        Mail::to($user)->send(new EmailVerif(['name' => $user->name,'id' => $user->id]));

        return $this->sendResponse(
            [
            'user' => $user,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function verify(Request $request)
    {
        $customer_id = decrypt($request->token);
        //update customer
        $user = User::where('id', $customer_id)->update(['is_verified' => 1]);

        return $this->sendResponse(
            [
            'status' => 'Akun berhasil diverifiikasi',
            ], __('response.SuccessGetData'), 200
        );
    }

    public function current(Request $request)
    {
        $user = $request->user();
        // dd($request->user());
        return $this->sendResponse(
            [
            'user' => $user,
            ], __('response.SuccessGetData'), 200
        );
    }
}
