<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Validator;
use App\Repositories\UserRepository;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $userRepository;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->userRepository = new UserRepository;
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Illuminate\Http\Response
     */
    protected function registered(Request $request, User $user)
    {
        if ($user instanceof MustVerifyEmail) {
            return response()->json(['status' => trans('verification.sent')]);
        }

        return response()->json($user);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data, [
            'name' => 'required|max:255',
            'username' => 'required|max:30|unique:users',
            'email' => 'required|email:filter|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' =>  'nullable|max:16',
            'role' => 'nullable',
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create(array $data): User
    {
        return $this->userRepository->createAccount($data);
    }
}
