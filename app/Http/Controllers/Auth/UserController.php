<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends BaseController
{
    /**
     * Get authenticated user.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function current(Request $request)
    {
        $user = $request->user();
        // dd($request->user());
        return $this->sendResponse(
            [
            'user' => $user,
            ], __('response.SuccessGetData'), 200
        );
    }
}
