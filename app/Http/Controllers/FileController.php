<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FileController extends BaseController
{
    public function upload(Request $request)
    {
        $file = $request->image->store('produk');
        return json_encode(['image' => url('/berkas/'.$file)]);
    }

    public function uploadbulk(Request $request)
    {
        $file = $request->file->store('produk');
        return json_encode(['image' => url('/berkas/'.$file)]);
    }

    public function remove(Request $request)
    {
        $image_path = $request->image;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        return json_encode(['status' => 'oke']);
    }
}
