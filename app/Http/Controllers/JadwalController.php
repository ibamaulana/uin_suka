<?php

namespace App\Http\Controllers;

use App\Helpers\Midtrans;
use App\Http\Controllers\BaseController;
use App\Mail\EmailTransaction;
use App\Mail\OrderMail;
use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Futsal;
use App\Models\Gedung;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Tenis;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Mail;

class JadwalController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', false);

        $data = new Order;

        if($request->user()->level == 2){
            $data = $data->where('user_id',$request->user()->id);
        }

        if($search){
            $data = $data->where('order_number', 'like', '%'.$search.'%');
        }

        if($request->status){
            $status = explode(',',$request->status);
            $data = $data->whereIn('status', $status);
        }

        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function storefutsal(Request $request)
    {
        //order number
        if($request->order_type === 'tenis'){
            $prefix = 'TN';
        }else if($request->order_type === 'futsal'){
            $prefix = 'FUT';
        }else if($request->order_type === 'gedung'){
            $prefix = 'GD';
        }else if($request->order_type === 'produk'){
            $prefix = 'PD';
        }else if($request->order_type === 'clubhouse'){
            $prefix = 'CH';
        }else if($request->order_type === 'makanan'){
            $prefix = 'FD';
        }
        $orderdate = date('yymd',strtotime('now'));
        $ordercount = Order::where('order_type',$request->order_type)->whereMonth('created_at',date('m',strtotime('now')))->count();
        $ordercount = sprintf('%04d', ($ordercount+1));
        $ordernumber = $prefix.''.$orderdate.''.$ordercount;

        $date_start = date('Y-m-d',strtotime($request->date_start));
        $date_end = date('Y-m-d',strtotime($request->date_end));
        $jam_mulai = (int)$request->jam_mulai;
        $jam_selesai = (int)$request->jam_selesai;

        $period = CarbonPeriod::create($date_start, $date_end);
        $orderdetail = [];
        foreach ($period as $date) {
            for ($x=$jam_mulai; $x <= ($jam_selesai-1); $x++) {
                $title = sprintf('%02d', ($x)).':00 - '.sprintf('%02d', ($x+1)).':00';
                $hour_start = sprintf('%02d', ($x)).':00';
                $hour_end = sprintf('%02d', ($x+1)).':00';
                //cek dulu
                $check = OrderDetail::selectRaw('date_start,product_name')
                    ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
                    ->where('orders.order_type',$request->order_type)
                    ->whereIn('orders.status',['confirmation','waiting','success'])
                    ->whereBetween('date_start',[$date->format('Y-m-d'),$date->format('Y-m-d')])
                    ->whereBetween('hour_start',[$hour_start,$hour_end]);
                if($request->order_type === 'tenis'){
                    $check = $check->where('orders.lapangan',$request->lapangan);
                }
                $check = $check->count();
                if($check){
                    return $this->sendError('Jam sudah terbooking! Tanggal: '.$date->format('Y-m-d').' Jam: '.$title, 500
                    );
                }
                $orderdetail[] = [
                    'product_name' => $title,
                    'date_start' => $date->format('Y-m-d'),
                    'date_end' => $date->format('Y-m-d'),
                    'hour_start' => $hour_start,
                    'hour_end' => $hour_end,
                    'qty' => 1,
                    'price' => 0,
                ];

            }
        }

        $data = new Order();
        $data->user_id = 0;
        $data->lapangan = $request->lapangan;
        $data->order_number = $ordernumber;
        $data->order_type = $request->order_type;
        $data->user_type = $request->user_type;
        $data->service_type = $request->service_type;
        $data->payment_type = 'lunas';
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->member_number = @$request->member_number;
        $data->nama_corporate = @$request->nama_corporate;
        $data->berkas_corporate = @$request->berkas_corporate;
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 0;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'success';
        $data->expired_at = Carbon::now()->addMinute(30);
        $data->save();

        foreach($orderdetail as $val){
            $detail = new OrderDetail();
            $detail->order_id = $data->id;
            $detail->product_name = $val['product_name'];
            $detail->date_start = $val['date_start'];
            $detail->date_end = $val['date_end'];
            $detail->hour_start = $val['hour_start'];
            $detail->hour_end = $val['hour_end'];
            $detail->qty = $val['qty'];
            $detail->price = $val['price'];
            $detail->save();
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function storeclub(Request $request)
    {
        //order number
        $prefix = 'CH';

        $orderdate = date('yymd',strtotime('now'));
        $ordercount = Order::where('order_type',$request->order_type)->whereMonth('created_at',date('m',strtotime('now')))->count();
        $ordercount = sprintf('%04d', ($ordercount+1));
        $ordernumber = $prefix.''.$orderdate.''.$ordercount;

        $date_start = date('Y-m-d',strtotime($request->date_start));
        $date_end = date('Y-m-d',strtotime($request->date_end));
        $jam_mulai = (int)$request->jam_mulai;
        $jam_selesai = (int)$request->jam_selesai;

        $period = CarbonPeriod::create($date_start, $date_end);
        $orderdetail = [];
        foreach ($period as $date) {
            $data = OrderDetail::selectRaw('qty')
            ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
            ->where('orders.order_type','clubhouse')
            ->whereIn('orders.status',['waiting','success'])
            ->whereBetween('date_start',[$date->format('Y-m-d'),$date->format('Y-m-d')])
            ->first();
            $qtyorder = $data ? $data->qty : 0;
            $totalqty = $qtyorder + $request->qty;

            if($totalqty > 19){
                return $this->sendError('Waktu yang dipilih sudah terbooking!', 500);
            }
        }

        $orderdetail[] = [
            'product_name' => $request->qty.' Kamar,'.($request->qty*2).' Tamu',
            'date_start' => $date_start,
            'date_end' => $date_end,
            'hour_start' => '09:00',
            'hour_end' => '15:00',
            'qty' => $request->qty,
            'price' => 0,
        ];

        $data = new Order();
        $data->user_id = 0;
        $data->lapangan = $request->lapangan;
        $data->order_number = $ordernumber;
        $data->order_type = 'clubhouse';
        $data->user_type = $request->user_type;
        $data->service_type = $request->service_type;
        $data->payment_type = 'lunas';
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->member_number = @$request->member_number;
        $data->nama_corporate = @$request->nama_corporate;
        $data->berkas_corporate = @$request->berkas_corporate;
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 0;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'success';
        $data->expired_at = Carbon::now()->addMinute(30);
        $data->save();

        foreach($orderdetail as $val){
            $detail = new OrderDetail();
            $detail->order_id = $data->id;
            $detail->product_name = $val['product_name'];
            $detail->date_start = $val['date_start'];
            $detail->date_end = $val['date_end'];
            $detail->hour_start = $val['hour_start'];
            $detail->hour_end = $val['hour_end'];
            $detail->qty = $val['qty'];
            $detail->price = $val['price'];
            $detail->save();
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function storegedung(Request $request)
    {
        //order number
        $prefix = 'GD';

        $orderdate = date('yymd',strtotime('now'));
        $ordercount = Order::where('order_type',$request->order_type)->whereMonth('created_at',date('m',strtotime('now')))->count();
        $ordercount = sprintf('%04d', ($ordercount+1));
        $ordernumber = $prefix.''.$orderdate.''.$ordercount;

        $date_start = date('Y-m-d',strtotime($request->date_start));
        $date_end = date('Y-m-d',strtotime($request->date_start));
        $jam_mulai = (int)$request->jam_mulai;
        $jam_selesai = (int)$request->jam_selesai;

        $period = CarbonPeriod::create($date_start, $date_end);
        $orderdetail = [];
        foreach ($period as $date) {
            $data = OrderDetail::selectRaw('date_start,product_name')
            ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
            ->where('orders.order_type','gedung')
            ->whereIn('orders.status',['confirmation','waiting','success'])
            ->whereBetween('date_start',[$date_start,$date_end])
            ->whereBetween('hour_start',[$request->jam_mulai,$request->jam_selesai])
            ->count();

            if($data != 0){
                return $this->sendError('Waktu yang dipilih sudah terbooking!', 500);
            }
        }

        $orderdetail[] = [
            'product_name' => $request->service_type,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'hour_start' => $request->jam_mulai,
            'hour_end' => $request->jam_selesai,
            'qty' => 1,
            'price' => 0,
        ];

        $data = new Order();
        $data->user_id = 0;
        $data->lapangan = $request->lapangan;
        $data->order_number = $ordernumber;
        $data->order_type = 'gedung';
        $data->user_type = $request->user_type;
        $data->service_type = $request->service_type;
        $data->payment_type = 'lunas';
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->member_number = @$request->member_number;
        $data->nama_corporate = @$request->nama_corporate;
        $data->berkas_corporate = @$request->berkas_corporate;
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 0;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'success';
        $data->expired_at = Carbon::now()->addMinute(30);
        $data->save();

        foreach($orderdetail as $val){
            $detail = new OrderDetail();
            $detail->order_id = $data->id;
            $detail->product_name = $val['product_name'];
            $detail->date_start = $val['date_start'];
            $detail->date_end = $val['date_end'];
            $detail->hour_start = $val['hour_start'];
            $detail->hour_end = $val['hour_end'];
            $detail->qty = $val['qty'];
            $detail->price = $val['price'];
            $detail->save();
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function storehall(Request $request)
    {
        //order number
        $prefix = 'H';

        $orderdate = date('yymd',strtotime('now'));
        $ordercount = Order::where('order_type',$request->order_type)->whereMonth('created_at',date('m',strtotime('now')))->count();
        $ordercount = sprintf('%04d', ($ordercount+1));
        $ordernumber = $prefix.''.$orderdate.''.$ordercount;

        $date_start = date('Y-m-d',strtotime($request->date_start));
        $date_end = date('Y-m-d',strtotime($request->date_start));
        $jam_mulai = (int)$request->jam_mulai;
        $jam_selesai = (int)$request->jam_selesai;

        $period = CarbonPeriod::create($date_start, $date_end);
        $orderdetail = [];
        foreach ($period as $date) {
            $data = OrderDetail::selectRaw('date_start,product_name')
            ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
            ->where('orders.order_type','hall')
            ->whereIn('orders.status',['confirmation','waiting','success'])
            ->whereBetween('date_start',[$date_start,$date_end])
            ->whereBetween('hour_start',[$request->jam_mulai,$request->jam_selesai])
            ->count();

            if($data != 0){
                return $this->sendError('Waktu yang dipilih sudah terbooking!', 500);
            }
        }

        $orderdetail[] = [
            'product_name' => $request->service_type,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'hour_start' => $request->jam_mulai,
            'hour_end' => $request->jam_selesai,
            'qty' => 1,
            'price' => 0,
        ];

        $data = new Order();
        $data->user_id = 0;
        $data->lapangan = $request->lapangan;
        $data->order_number = $ordernumber;
        $data->order_type = 'hall';
        $data->user_type = $request->user_type;
        $data->service_type = $request->service_type;
        $data->payment_type = 'lunas';
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->member_number = @$request->member_number;
        $data->nama_corporate = @$request->nama_corporate;
        $data->berkas_corporate = @$request->berkas_corporate;
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 0;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'success';
        $data->expired_at = Carbon::now()->addMinute(30);
        $data->save();

        foreach($orderdetail as $val){
            $detail = new OrderDetail();
            $detail->order_id = $data->id;
            $detail->product_name = $val['product_name'];
            $detail->date_start = $val['date_start'];
            $detail->date_end = $val['date_end'];
            $detail->hour_start = $val['hour_start'];
            $detail->hour_end = $val['hour_end'];
            $detail->qty = $val['qty'];
            $detail->price = $val['price'];
            $detail->save();
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function detail($id,Request $request)
    {
        $data = Order::with('orderdetail')->where('id',$id)->first();
        $midtrans = null;
        if($data->payment_method === 'midtrans'){
            $midtrans = Midtrans::getDetail($data->order_number);
            if($midtrans['data']->transaction_status === 'expire'){
                $data->status = 'expired';
                $data->update();
            }

            if($midtrans['data']->transaction_status === 'settlement'){
                $data->status = 'success';
                $data->update();
            }

            if($midtrans['data']->transaction_status === 'cancel'){
                $data->status = 'cancel';
                $data->update();
            }
        }

        return $this->sendResponse(
            [
                'data' => $data,
                'midtrans' => $midtrans
            ], __('response.SuccessGetData'), 200
        );
    }

    public function handling(Request $request)
    {
        $data = Order::where('id',$request['order_id'])->first();

        if($data){
            if($request['fraud_status'] === 'settlement'){
                $status = 'success';
            }
            if($request['fraud_status'] === 'cancel'){
                $status = 'cancel';
            }
            if($request['fraud_status'] === 'expire'){
                $status = 'expired';
            }
            $data->status = $status;
            $data->update();
        }

        Mail::to($data->user)->send(new EmailTransaction(['name' => $data->user->name,'id' => $data->user->id]));

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function updatebukti($id,Request $request)
    {
        $data = Order::where('id',$id)->first();
        $data->bukti_bayar = $request->bukti_bayar;
        $data->update();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalfutsal(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->date));
        $date_end = date('Y-m-d',strtotime($request->date));

        $data = OrderDetail::selectRaw('orders.status, orders.id as id,date_start,product_name,orders.name as name,orders.phone as phone,orders.service_type as service_type')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','futsal')
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();

        // dd($data);

        $booked = [];
        $detail = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
            $detail[$key->date_start] = (object)[
                'id' => $key->id,
                'name' => $key->name,
                'phone' => $key->phone,
                'email' => $key->email,
                'service_type' => $key->service_type,
            ];
        }


        $jadwal = [];
        $turnamen = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            $x = 7;
            $y = 20;
            for ($x; $x <= $y; $x=$x+1) {
                $keterangan = '-';
                $title = sprintf('%02d', ($x)).':00 - '.sprintf('%02d', ($x+1)).':00';
                if(isset($booked[$date->format('Y-m-d')])){
                    if(in_array($title,$booked[$date->format('Y-m-d')])){
                        $available = false;
                        $dt = $detail[$date->format('Y-m-d')];
                        $idne = $dt->id;
                        $description = 'Sudah Terbooking';
                        $keterangan = 'Tipe: '.$dt->service_type.', Atas Nama: '.$dt->name.' ('.$dt->phone.')';
                    }else{
                        $available = true;
                        $idne = null;
                        $description = 'Lapangan Tersedia';
                    }
                }else{
                    $available = true;
                    $idne = null;
                    $description = 'Lapangan Tersedia';
                }

                $data = (object)[
                    'id' => $idne,
                    'title' => $title,
                    'description' => $description,
                    'class' => $available? 'text-success' : 'text-danger',
                    'keterangan' => $keterangan,
                    'available' => $available,
                    'checked' => false
                ];
                $jadwal[] = $data;
            }
        }

        return $this->sendResponse(
            [
                'data' => $jadwal,
                'pagination' => false
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwaltenis(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->date));
        $date_end = date('Y-m-d',strtotime($request->date));

        $data = OrderDetail::selectRaw('orders.id,date_start,product_name,orders.name as name,orders.phone as phone,orders.service_type as service_type')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','tenis')
        ->where('orders.lapangan',$request->lapangan)
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();
        $booked = [];
        $detail = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
            $detail[$key->date_start.' '.$key->product_name] = (object)[
                'id' => $key->id,
                'name' => $key->name,
                'phone' => $key->phone,
                'email' => $key->email,
                'service_type' => $key->service_type
            ];
        }


        $jadwal = [];
        $turnamen = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            $x = 7;
            $y = 20;
            for ($x; $x <= $y; $x=$x+1) {
                $keterangan = '-';
                $title = sprintf('%02d', ($x)).':00 - '.sprintf('%02d', ($x+1)).':00';
                if(isset($booked[$date->format('Y-m-d')])){
                    if(in_array($title,$booked[$date->format('Y-m-d')])){
                        $available = false;
                        $dt = $detail[$date->format('Y-m-d').' '.$title];
                        $idne = $dt->id;
                        $description = 'Sudah Terbooking';
                        $keterangan = 'Tipe: '.$dt->service_type.', Atas Nama: '.$dt->name.' ('.$dt->phone.')';
                    }else{
                        $available = true;
                        $idne = null;
                        $description = 'Lapangan Tersedia';
                    }
                }else{
                    $available = true;
                    $idne = null;
                    $description = 'Lapangan Tersedia';
                }

                $data = (object)[
                    'id' => $idne,
                    'title' => $title,
                    'description' => $description,
                    'class' => $available? 'text-success' : 'text-danger',
                    'keterangan' => $keterangan,
                    'available' => $available,
                    'checked' => false
                ];
                $jadwal[] = $data;
            }
        }

        return $this->sendResponse(
            [
                'data' => $jadwal,
                'pagination' => false
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalclub(Request $request)
    {
        $data = OrderDetail::selectRaw('orders.id,qty,orders.name,orders.phone,orders.status,orders.order_number,date_start,date_end')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','clubhouse')
        ->whereIn('orders.status',['waiting','success'])
        ->get();
        $result = [];
        foreach($data as $val){
            $period = CarbonPeriod::create($val->date_start, $val->date_end);
            foreach($period as $date){
                if(!isset($result[$date->format('Y-m-d')])){
                    $result[$date->format('Y-m-d')] = [
                        'total' => 0,
                        'detail' => []
                    ];
                }
                $result[$date->format('Y-m-d')]['total'] = $result[$date->format('Y-m-d')]['total'] + $val->qty;
                $result[$date->format('Y-m-d')]['detail'][] = (object)[
                    'id' => $val->id,
                    'order_number' => $val->order_number,
                    'name' => $val->name,
                    'phone' => $val->phone,
                    'status' => $val->status,
                    'qty' => $val->qty
                ];
            }
        }

        return $this->sendResponse(
            [
                'data' => (array)$result,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalgedung(Request $request)
    {
        $data = OrderDetail::selectRaw('orders.id,qty,orders.name,orders.phone,orders.status,orders.order_number,date_start,date_end,hour_start,hour_end,orders.service_type')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','gedung')
        ->whereIn('orders.status',['confirmation','waiting','success'])
        ->get();
        $result = [];
        foreach($data as $val){
            $period = CarbonPeriod::create($val->date_start, $val->date_end);
            foreach($period as $date){
                $service = ucwords(str_replace('_', ' ', $val->service_type));
                $title = $date->format('Y-m-d').' '.$val->hour_start.'-'.$val->hour_end.' ('.$service.')';
                if(!isset($result[$title])){
                    $result[$title] = [
                        'date' => $date->format('Y-m-d'),
                        'status' => $val->status,
                        'detail' => []
                    ];
                }
                $result[$title]['detail'] = (object)[
                    'id' => $val->id,
                    'order_number' => $val->order_number,
                    'name' => $val->name,
                    'phone' => $val->phone,
                    'status' => $val->status,
                    'qty' => $val->qty
                ];
            }
        }

        return $this->sendResponse(
            [
                'data' => (array)$result,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalhall(Request $request)
    {
        $data = OrderDetail::selectRaw('orders.id,qty,orders.name,orders.phone,orders.status,orders.order_number,date_start,date_end,hour_start,hour_end,orders.service_type')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','hall')
        ->whereIn('orders.status',['confirmation','waiting','success'])
        ->get();
        $result = [];
        foreach($data as $val){
            $period = CarbonPeriod::create($val->date_start, $val->date_end);
            foreach($period as $date){
                $service = ucwords(str_replace('_', ' ', $val->service_type));
                $title = $date->format('Y-m-d').' '.$val->hour_start.'-'.$val->hour_end.' ('.$service.')';
                if(!isset($result[$title])){
                    $result[$title] = [
                        'date' => $date->format('Y-m-d'),
                        'status' => $val->status,
                        'detail' => []
                    ];
                }
                $result[$title]['detail'] = (object)[
                    'id' => $val->id,
                    'order_number' => $val->order_number,
                    'name' => $val->name,
                    'phone' => $val->phone,
                    'status' => $val->status,
                    'qty' => $val->qty
                ];
            }
        }

        return $this->sendResponse(
            [
                'data' => (array)$result,
            ], __('response.SuccessGetData'), 200
        );
    }
}
