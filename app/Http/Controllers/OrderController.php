<?php

namespace App\Http\Controllers;

use App\Helpers\Midtrans;
use App\Http\Controllers\BaseController;
use App\Mail\EmailTransaction;
use App\Mail\OrderMail;
use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Futsal;
use App\Models\Gedung;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Tenis;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Mail;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', false);

        $data = Order::with('orderdetail');

        if($request->user()->level == 2){
            $data = $data->where('user_id',$request->user()->id);
        }

        if($search){
            $data = $data->where('order_number', 'like', '%'.$search.'%');
        }

        if($request->status){
            $status = explode(',',$request->status);
            $data = $data->whereIn('status', $status);
        }
        if($request->status_filter != 'all'){
            $data = $data->where('status', $request->status_filter);
        }

        if($request->type != 'all'){
            $data = $data->where('order_type', $request->type);
        }

        if($request->date){
            $data = $data->whereDate('created_at',$request->date);
        }

        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function store(Request $request)
    {
        //order number
        if($request->order_type === 'tenis'){
            $prefix = 'TN';
        }else if($request->order_type === 'futsal'){
            $prefix = 'FUT';
        }else if($request->order_type === 'gedung'){
            $prefix = 'GD';
        }else if($request->order_type === 'produk'){
            $prefix = 'PD';
        }else if($request->order_type === 'clubhouse'){
            $prefix = 'CH';
        }else if($request->order_type === 'makanan'){
            $prefix = 'FD';
        }else if($request->order_type === 'hall'){
            $prefix = 'CH';
        }
        $orderdate = date('yymd',strtotime('now'));
        $ordercount = Order::where('order_type',$request->order_type)->whereMonth('created_at',date('m',strtotime('now')))->count();
        $ordercount = sprintf('%04d', ($ordercount+1));
        $ordernumber = $prefix.''.$orderdate.''.$ordercount;

        $data = new Order();
        $data->user_id = $request->user()->id;
        $data->lapangan = $request->lapangan;
        $data->order_number = $ordernumber;
        $data->order_type = $request->order_type;
        $data->user_type = $request->user_type;
        $data->service_type = $request->service_type;
        $data->payment_type = $request->payment_type;
        $data->name = $request->name;
        $data->email = $request->user()->email;
        $data->phone = $request->phone;
        $data->member_number = $request->member_number;
        $data->nama_corporate = $request->nama_corporate;
        $data->berkas_corporate = $request->berkas_corporate;
        $data->payment_method = $request->payment_method;
        $data->down_payment = $request->down_payment;
        $data->subtotal = $request->subtotal;
        $data->discount = 0;
        $data->total = $request->total;
        $data->status = 'confirmation';
        $data->expired_at = Carbon::now()->addMinute(30);
        $data->save();

        //for midtrans
        $item_detail = [];
        foreach($request->order_detail as $val){
            $detail = new OrderDetail();
            $detail->order_id = $data->id;
            $detail->product_name = $val['product_name'];
            $detail->date_start = $val['date_start'];
            $detail->date_end = $val['date_end'];
            $detail->hour_start = $val['hour_start'];
            $detail->hour_end = $val['hour_end'];
            $detail->qty = $val['qty'];
            $detail->price = $val['price'];
            $detail->save();
            $item_detail[] = (object)[
                'id' => $detail->id,
                "price"=>$detail->price/$detail->qty,
                "quantity"=>$detail->qty,
                "name"=>$detail->product_name,
            ];
        }

        //sendmidtrans
        $midtrans = null;
        if($request->is_midtrans){
            $param['transaction_details'] = (object)[
                'order_id' => $data->order_number,
                'gross_amount' => $data->total
            ];
            $param['item_details'] = $item_detail;
            $param['customer_details'] = (object)[
                "first_name"=>$request->user()->name,
                "last_name"=>"",
                "email"=>$request->user()->email,
            ];
            $param['callbacks'] = (object)[
                "finish"=> $request->midtrans_callback,
            ];
            $param['expiry'] = (object)[
                "start_time"=> Carbon::now()->addSecond()->format('Y-m-d H:i:s').' +0700',
                "unit"=>"minutes",
                "duration"=>15
            ];
            $param['enabled_payments'] = ['bri_va','gopay','bni_va'];
            $param['gopay'] = (object)[
                "enable_callback" => true,
                "callback_url" => $request->midtrans_callback
            ];
            $midtrans = Midtrans::chargeToken($param);
        }
        $user = User::where('id',$request->user()->id)->first();
        Mail::to($user)->send(new OrderMail($user,$data));

        return $this->sendResponse(
            [
                'data' => $data,
                'midtrans' => $midtrans,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function detail($id,Request $request)
    {
        $data = Order::with('orderdetail')->where('id',$id)->first();
        $midtrans = null;
        if($data->payment_method === 'midtrans'){
            $midtrans = Midtrans::getDetail($data->order_number);
            if($midtrans['data']->transaction_status === 'expire'){
                $data->status = 'expired';
                $data->update();
            }

            if($midtrans['data']->transaction_status === 'settlement'){
                $data->status = 'success';
                $data->update();
            }

            if($midtrans['data']->transaction_status === 'cancel'){
                $data->status = 'cancel';
                $data->update();
            }
        }

        return $this->sendResponse(
            [
                'data' => $data,
                'midtrans' => $midtrans
            ], __('response.SuccessGetData'), 200
        );
    }

    public function handling(Request $request)
    {
        $data = Order::where('order_number',$request['order_id'])->first();

        if($data){
            if($request['fraud_status'] === 'settlement'){
                if($data->status === 'waiting_dp'){
                    $status = 'waiting';
                    $ordernumber = explode('-',$data->order_number);
                    $data->order_number = $ordernumber[1];
                }else{
                    $status = 'success';
                }
            }
            if($request['fraud_status'] === 'accept'){
                if($data->status === 'waiting_dp'){
                    $status = 'waiting';
                    $ordernumber = explode('-',$data->order_number);
                    $data->order_number = $ordernumber[1];
                }else{
                    $status = 'success';
                }
            }
            if($request['fraud_status'] === 'cancel'){
                $status = 'cancel';
            }
            if($request['fraud_status'] === 'expire'){
                $status = 'expired';
            }
            $data->status = $status;
            $data->update();
        }

        Mail::to($data->user)->send(new EmailTransaction(['name' => $data->user->name,'id' => $data->user->id]));

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function updatebukti($id,Request $request)
    {
        $data = Order::where('id',$id)->first();
        $data->bukti_bayar = $request->bukti_bayar;
        $data->update();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function updategedung($id,Request $request)
    {
        $data = Order::where('id',$id)->first();
        if($request->status === 'waiting'){
            $data->order_number = 'DP-'.$data->order_number;
            $data->status = 'waiting_dp';
        }else{
            $data->status = $request->status;
        }
        $data->total = $request->total;
        $data->down_payment = $request->dp;
        $data->paid_left = $request->total-$request->dp;
        $data->update();

        $order_detail = OrderDetail::where('order_id',$id)->first();
        $order_detail->price = $request->total;
        $order_detail->update();

        $user = User::where('id',$request->user()->id)->first();
        Mail::to($user)->send(new OrderMail($user,$data));

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function bayargedung($id,Request $request)
    {
        $data = Order::where('id',$id)->first();
        $order_detail = OrderDetail::where('order_id',$id)->get();
        $item_detail = [];
        if($data->status === 'waiting_dp'){
            $item_detail[] = (object)[
                'id' => 1,
                'price' => $data->down_payment,
                'quantity' => 1,
                'name' => 'Down Payment'
            ];
            $midtrans = null;
            if($request->is_midtrans){
                $param['transaction_details'] = (object)[
                    'order_id' => $data->order_number,
                    'gross_amount' => $data->down_payment
                ];
                $param['item_details'] = $item_detail;
                $param['customer_details'] = (object)[
                    "first_name"=>$request->user()->name,
                    "last_name"=>"",
                    "email"=>$request->user()->email,
                ];
                $param['callbacks'] = (object)[
                    "finish"=> $request->midtrans_callback,
                ];
                $param['expiry'] = (object)[
                    "start_time"=> Carbon::now()->addSecond()->format('Y-m-d H:i:s').' +0700',
                    "unit"=>"minutes",
                    "duration"=>15
                ];
                $param['enabled_payments'] = ['bri_va','gopay','bni_va'];
                $param['gopay'] = (object)[
                    "enable_callback" => true,
                    "callback_url" => $request->midtrans_callback
                ];
                $midtrans = Midtrans::chargeToken($param);
            }
        }else{
            foreach($order_detail as $val){
                $item_detail[] = (object)[
                    'id' => $val->id,
                    "price"=>$data->paid_left,
                    "quantity"=>$val->qty,
                    "name"=>$val->product_name,
                ];
            }
            $midtrans = null;
            if($request->is_midtrans){
                $param['transaction_details'] = (object)[
                    'order_id' => $data->order_number,
                    'gross_amount' => $data->paid_left
                ];
                $param['item_details'] = $item_detail;
                $param['customer_details'] = (object)[
                    "first_name"=>$request->user()->name,
                    "last_name"=>"",
                    "email"=>$request->user()->email,
                ];
                $param['callbacks'] = (object)[
                    "finish"=> $request->midtrans_callback,
                ];
                $param['expiry'] = (object)[
                    "start_time"=> Carbon::now()->addSecond()->format('Y-m-d H:i:s').' +0700',
                    "unit"=>"minutes",
                    "duration"=>15
                ];
                $param['enabled_payments'] = ['bri_va','gopay','bni_va'];
                $param['gopay'] = (object)[
                    "enable_callback" => true,
                    "callback_url" => $request->midtrans_callback
                ];
                $midtrans = Midtrans::chargeToken($param);
            }
        }

        $user = User::where('id',$request->user()->id)->first();
        Mail::to($user)->send(new OrderMail($user,$data));

        return $this->sendResponse(
            [
                'data' => $data,
                'midtrans' => $midtrans,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalfutsal(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','futsal')
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();
        $booked = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
        }


        $jadwal = [];
        $turnamen = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            $x = 7;
            $y = 20;
            for ($x; $x <= $y; $x=$x+1) {
                $title = sprintf('%02d', ($x)).':00 - '.sprintf('%02d', ($x+1)).':00';
                if(isset($booked[$date->format('Y-m-d')])){
                    if(in_array($title,$booked[$date->format('Y-m-d')])){
                        $available = false;
                        $description = 'Sudah Terbooking';
                    }else{
                        $available = true;
                        $description = 'Lapangan Tersedia';
                    }
                }else{
                    $available = true;
                    $description = 'Lapangan Tersedia';
                }

                $data = (object)[
                    'title' => $title,
                    'description' => $description,
                    'available' => $available,
                    'checked' => false
                ];
                $jadwal[$date->format('Y-m-d')][] = $data;
            }
        }

        if($request)

        return $this->sendResponse(
            [
                'data' => $jadwal,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalfutsalturnamen(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','futsal')
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();
        $booked = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
        }


        $jadwal = [];
        $turnamen = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            if(isset($booked[$date->format('Y-m-d')])){
                $jadwal[$date->format('Y-m-d')] = false;
            }else{
                $jadwal[$date->format('Y-m-d')] = true;
            }
        }

        if($request)

        return $this->sendResponse(
            [
                'data' => $jadwal,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwaltenis(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','tenis');
        if($request->lapangan){
            $data = $data->where('orders.lapangan',$request->lapangan);
        }
        $data= $data
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();
        $booked = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
        }


        $jadwal = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            $x = 7;
            $y = 20;
            for ($x; $x <= $y; $x=$x+1) {
                $title = sprintf('%02d', ($x)).':00 - '.sprintf('%02d', ($x+1)).':00';
                if(isset($booked[$date->format('Y-m-d')])){
                    if(in_array($title,$booked[$date->format('Y-m-d')])){
                        $available = false;
                        $description = 'Sudah Terbooking';
                    }else{
                        $available = true;
                        $description = 'Lapangan Tersedia';
                    }
                }else{
                    $available = true;
                    $description = 'Lapangan Tersedia';
                }

                $data = (object)[
                    'title' => $title,
                    'description' => $description,
                    'available' => $available,
                    'checked' => false
                ];
                $jadwal[$date->format('Y-m-d')][] = $data;
            }
        }

        return $this->sendResponse(
            [
                'data' => $jadwal,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwaltenisturnamen(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','tenis')
        ->whereIn('orders.status',['waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->get();
        $booked = [];
        foreach($data as $key){
            $booked[$key->date_start][] = $key->product_name;
        }


        $jadwal = [];
        $turnamen = [];
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            if(isset($booked[$date->format('Y-m-d')])){
                $jadwal[$date->format('Y-m-d')] = false;
            }else{
                $jadwal[$date->format('Y-m-d')] = true;
            }
        }

        if($request)

        return $this->sendResponse(
            [
                'data' => $jadwal,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalgedung(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','gedung')
        ->whereIn('orders.status',['confirmation','waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->whereBetween('hour_start',[$request->hour_start,$request->hour_end])
        ->count();
        $booked = false;
        if($data == 0){
            $booked = true;
        }

        return $this->sendResponse(
            [
                'data' => $booked,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalhall(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));

        $data = OrderDetail::selectRaw('date_start,product_name')
        ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
        ->where('orders.order_type','hall')
        ->whereIn('orders.status',['confirmation','waiting','success'])
        ->whereBetween('date_start',[$date_start,$date_end])
        ->whereBetween('hour_start',[$request->hour_start,$request->hour_end])
        ->count();
        $booked = false;
        if($data == 0){
            $booked = true;
        }

        return $this->sendResponse(
            [
                'data' => $booked,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function jadwalclub(Request $request)
    {
        $date_start = date('Y-m-d',strtotime($request->start_date));
        $date_end = date('Y-m-d',strtotime($request->end_date));
        $period = CarbonPeriod::create($date_start, $date_end);
        // Iterate over the period
        foreach ($period as $date) {
            $data = OrderDetail::selectRaw('qty')
            ->leftjoin('orders','orders.id', '=', 'order_details.order_id')
            ->where('orders.order_type','clubhouse')
            ->whereIn('orders.status',['waiting','success'])
            ->whereBetween('date_start',[$date->format('Y-m-d'),$date->format('Y-m-d')])
            ->first();
            $qtyorder = $data ? $data->qty : 0;
            $totalqty = $qtyorder + $request->qty;

            if($totalqty > 19){
                return $this->sendResponse(
                    [
                        'data' => false,
                    ], __('response.SuccessGetData'), 200
                );
            }
        }

        return $this->sendResponse(
            [
                'data' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function delete($id)
    {
        $data = Order::where('id',$id);
        if (!$data->update(['status' => 'cancel'])) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    public function cancelgedung($id)
    {
        $data = Order::where('id', $id);
        if(!$data->update(['status' => 'cancel'])) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
