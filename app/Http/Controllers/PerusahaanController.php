<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\StatusPerusahaan;
use App\Models\TindakLanjut;
use Carbon\Carbon;

class PerusahaanController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = new Perusahaan;
        if($request->user()->level == 3){
            $data = $data->where('nip_user',$request->user()->nip);
        }
        if($search){
            $data = $data->where('nama_perusahaan', 'like', '%'.$search.'%');
        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function datastatus(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = StatusPerusahaan::with('perusahaan','user')->where('perusahaan_id',$request->id);
        if($search){
            // $data = $data->where('nama_perusahaan', 'like', '%'.$search.'%');
        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function datatindaklanjut(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = TindakLanjut::with('perusahaan','user')->where('perusahaan_id',$request->id);
        if($search){
            // $data = $data->where('nama_perusahaan', 'like', '%'.$search.'%');
        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function datatindaklanjut_store(Request $request)
    {
        $data = new TindakLanjut;
        $data->perusahaan_id = $request->perusahaan_id;
        $data->keterangan = $request->keterangan;
        $data->tanggal = $request->tanggal;
        $data->berkas = $request->berkas->store('/berkas');
        $data->user_id = $request->user()->id;

        if (!$data->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }
    }

    public function datatindaklanjut_detail($id)
    {
        $data = TindakLanjut::where('id',$id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function datatindaklanjut_update(Request $request, $id)
    {
        $data = TindakLanjut::where('id',$id)->first();
        $data->perusahaan_id = $request->perusahaan_id;
        $data->keterangan = $request->keterangan;
        $data->tanggal = $request->tanggal;
        $data->berkas = $request->berkas->store('/berkas');
        $data->user_id = $request->user()->id;

        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    public function datatindaklanjut_delete($id)
    {
        $data = TindakLanjut::where('id',$id);
        if (!$data->delete()) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubjectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Perusahaan;
        $data->nama_perusahaan = $request->nama_perusahaan;
        $data->no_pendaftaran = $request->no_pendaftaran;
        $data->alamat = $request->alamat;
        $data->nama_pic = $request->nama_pic;
        $data->nohp_pic = $request->nohp_pic;
        $data->status = $request->status;
        $data->jenis_ketidakpatuhan = $request->jenis_ketidakpatuhan;
        $data->nilai_ketidakpatuhan = $request->nilai_ketidakpatuhan??0;
        $data->nip_user = $request->petugas_pemeriksa;

        if (!$data->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        //perusahaan status
        $status = new StatusPerusahaan;
        $status->perusahaan_id = $data->id;
        $status->status = $request->status;
        $status->user_id = $request->user()->id;

        if (!$status->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = Perusahaan::where('id',$id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Perusahaan::where('id',$id)->first();
        if($data->status != $request->status){
            $status = new StatusPerusahaan;
            $status->perusahaan_id = $data->id;
            $status->status = $request->status;
            $status->user_id = $request->user()->id;

            if (!$status->save()) {
                return $this->sendError(__('response.FailedStoreData'));
            }
        }
        $data->nama_perusahaan = $request->nama_perusahaan;
        $data->no_pendaftaran = $request->no_pendaftaran;
        $data->alamat = $request->alamat;
        $data->nama_pic = $request->nama_pic;
        $data->nohp_pic = $request->nohp_pic;
        $data->status = $request->status;
        $data->jenis_ketidakpatuhan = $request->jenis_ketidakpatuhan;
        $data->nilai_ketidakpatuhan = $request->nilai_ketidakpatuhan??0;
        $data->nip_user = $request->petugas_pemeriksa;

        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Perusahaan::where('id',$id);
        if (!$data->delete()) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    public function status(Request $request)
    {
        $data = Perusahaan::where('status',$request->status);
        if($request->user()->level == 3){
            $data = $data->where('nip_user',$request->user()->nip);
        }
        $data = $data->count();

        return $data;
    }

    public function export(Request $request)
    {
        //triwulan 1
        $year = date('Y',strtotime('now'));
        $date1 = Carbon::parse($year.'-01')->startofMonth()->toDateString().' 00:01';
        $date2 = Carbon::parse($year.'-03')->endOfMonth()->toDateString().' 23:59';
        $date1 = date('Y',strtotime('now'));
        $data = StatusPerusahaan::whereYear('created_at');

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }
}
