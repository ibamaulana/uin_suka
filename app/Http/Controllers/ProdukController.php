<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Produk;
use Illuminate\Support\Facades\Storage;

class ProdukController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->all ? 100000 : $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = new Produk();

        if($search){
            $data = $data->where('name', 'like', '%'.$search.'%');
        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');

        $data = $data->paginate($size);

        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubjectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Produk;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->image = $request->image;
        $data->price = $request->price;

        if (!$data->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = Produk::where('id',$id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Produk::where('id',$id)->first();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->image = $request->image;
        $data->price = $request->price;

        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Produk::where('id',$id);
        if (!$data->delete()) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
