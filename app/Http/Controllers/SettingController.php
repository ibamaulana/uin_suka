<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\StatusPerusahaan;
use App\Models\TindakLanjut;
use App\Models\Futsal;
use App\Models\Gedung;
use App\Models\Tenis;
use App\Models\Club;
use Carbon\Carbon;

class SettingController extends BaseController
{
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = new Perusahaan;
        if($request->user()->level == 3){
            $data = $data->where('nip_user',$request->user()->nip);
        }
        if($search){
            $data = $data->where('nama_perusahaan', 'like', '%'.$search.'%');
        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);
       
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function store(Request $request)
    {
        $data = new Perusahaan;
        $data->nama_perusahaan = $request->nama_perusahaan;
        $data->no_pendaftaran = $request->no_pendaftaran;
        $data->alamat = $request->alamat;
        $data->nama_pic = $request->nama_pic;
        $data->nohp_pic = $request->nohp_pic;
        $data->status = $request->status;
        $data->jenis_ketidakpatuhan = $request->jenis_ketidakpatuhan;
        $data->nilai_ketidakpatuhan = $request->nilai_ketidakpatuhan??0;
        $data->nip_user = $request->petugas_pemeriksa;

        if (!$data->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        //perusahaan status
        $status = new StatusPerusahaan;
        $status->perusahaan_id = $data->id;
        $status->status = $request->status;
        $status->user_id = $request->user()->id;

        if (!$status->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    public function detail($id)
    {
        $data = Perusahaan::where('id',$id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function futsal()
    {
        $data = Futsal::first();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function gedung()
    {
        $data = Gedung::first();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function tenis()
    {
        $data = Tenis::first();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function club()
    {
        $data = Club::first();

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function updategedung(Request $request, $id)
    {
        $data = Gedung::first();
        $data->whatsapp = $request->whatsapp;
        // $data->harga_convention_hall = $request->harga_convention_hall;
        // $data->harga_pernikahan = $request->harga_pernikahan;
        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }    
        
        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );  
    }

    public function updateclub(Request $request, $id)
    {
        $data = Club::first();
        $data->max_room = $request->max_room;
        $data->price_per_room = $request->price_per_room;
        $data->price_per_breakfast = $request->price_per_breakfast;
        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }    
        
        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );  
    }

    public function updatefutsal(Request $request, $id)
    {
        $data = Futsal::first();
        $data->harga_internal_siang = $request->harga_internal_siang;
        $data->harga_internal_sore = $request->harga_internal_sore;
        $data->harga_eksternal_siang = $request->harga_eksternal_siang;
        $data->harga_eksternal_sore = $request->harga_eksternal_sore;
        $data->harga_internal_turnamen = $request->harga_internal_turnamen;
        $data->harga_eksternal_turnamen = $request->harga_eksternal_turnamen;
        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }    
        
        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );  
    }

    public function updatetenis(Request $request, $id)
    {
        $data = Tenis::first();
        $data->harga_internal = $request->harga_internal;
        $data->harga_member_siang = $request->harga_member_siang;
        $data->harga_member_sore = $request->harga_member_sore;
        $data->harga_internal_siang = $request->harga_internal_siang;
        $data->harga_internal_sore = $request->harga_internal_sore;
        $data->harga_turnamen = $request->harga_turnamen;

        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }    
        
        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );  
    }

    public function update(Request $request, $id)
    {
        $data = Perusahaan::where('id',$id)->first();
        if($data->status != $request->status){
            $status = new StatusPerusahaan;
            $status->perusahaan_id = $data->id;
            $status->status = $request->status;
            $status->user_id = $request->user()->id;

            if (!$status->save()) {
                return $this->sendError(__('response.FailedStoreData'));
            }
        }
        $data->nama_perusahaan = $request->nama_perusahaan;
        $data->no_pendaftaran = $request->no_pendaftaran;
        $data->alamat = $request->alamat;
        $data->nama_pic = $request->nama_pic;
        $data->nohp_pic = $request->nohp_pic;
        $data->status = $request->status;
        $data->jenis_ketidakpatuhan = $request->jenis_ketidakpatuhan;
        $data->nilai_ketidakpatuhan = $request->nilai_ketidakpatuhan??0;
        $data->nip_user = $request->petugas_pemeriksa;

        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    public function delete($id)
    {
        $data = Perusahaan::where('id',$id);
        if (!$data->delete()) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }
}
