<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\PasswordUpdateRequest as Request;

class PasswordController extends BaseController
{
    /**
     * Update the user's password.
     *
     * @param App\Http\Requests\Settings\PasswordUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $request->user()->update(
            [
            'password' => bcrypt($request->password),
            ]
        );

        return $this->sendResponse(
            [], __('response.SuccessUpdateData'), 204
        );
    }
}
