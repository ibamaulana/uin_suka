<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\BaseController;
use App\Repositories\UserRepository;
use App\Http\Requests\Settings\ProfileUpdateRequest as Request;
use App\Services\UploadService;

class ProfileController extends BaseController
{
    protected $userRepository;
    protected $uploadService;
    /**
     * Dependency Injection
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository;
        $this->uploadService = new UploadService;
    }
    /**
     * Update the user's profile information.
     *
     * @param App\Http\Requests\Settings\ProfileUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate(
            $request, [
            'username' => 'unique:users,username,'.$user->id,
            'email' => 'unique:users,email,'.$user->id,
            ]
        );
        $input = $request->only('name', 'username', 'email', 'phone_number');

        if ($request->has('picture')) {
            $userProfile = $user->picture;
            if ($userProfile) {
                $this->uploadService->deleteImage($userProfile);
            }
            $picturePath = $this->uploadService->uploadImage('public/profile-picture', $request->picture);
            $input['picture'] = $picturePath;
        }

        $data = $this->userRepository->update($input, $user->id);
        if (!$data) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }
}
