<?php

namespace App\Http\Controllers;

class SpaController extends Controller
{
    /**
     * Get the SPA view.
     *
     * @return void
     */
    public function __invoke()
    {
        return view('spa');
    }
}
