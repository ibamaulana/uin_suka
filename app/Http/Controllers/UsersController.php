<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\StatusPerusahaan;
use App\Models\User;
use Carbon\Carbon;

class UsersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size      = $request->input('size', 10);
        $search    = $request->input('search', null);
        $sortField = $request->input('sort', 'id');
        $sortAsc   = $request->input('sort_asc', true);

        $data = User::where('level','!=',1);
        if($search){
            $data = $data->where('name', 'like', '%'.$search.'%');
        }
        if($request->status){
            $data = $data->where('status', $request->status);

        }
        $data = $data->orderBy($sortField, $sortAsc ? 'asc' : 'desc');
        $data = $data->paginate($size);
       
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }

    public function select()
    {
        $data = User::where('level',3)->get();

        $datas = [];
        foreach ($data as $key) {
            $datas[] = [
                'id' => $key->nip,
                'text' => $key->name,
            ];
        }
        return json_encode($datas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\SubjectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new User;
        //cek NIP
        if($request->level == 3){
            $checknip = User::where('nip',$request->nip)->first();
            if($checknip){
                return $this->sendError('NIP Sudah terdaftar!');
            }else{
                $data->nip = $request->nip;
            }
        }
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->level = $request->level;
        
        if (!$data->save()) {
            return $this->sendError(__('response.FailedStoreData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessStoreData'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = User::where('id',$id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    public function current(Request $request)
    {
        $data = User::where('id',$request->user()->id)->first();
        if (!$data) {
            return $this->sendError(__('response.FailedGetData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessGetData'), 201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::where('id',$id)->first();
        $data->name = $request->name;
        $data->email = $request->email;
        if($request->password){
            $data->password = bcrypt($request->password);
        }
        $data->level = $request->level;
        if (!$data->update()) {
            return $this->sendError(__('response.FailedUpdateData'));
        }

        return $this->sendResponse(
            [
                'data' => $data,
            ], __('response.SuccessUpdateData'), 200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = User::where('id',$id);
        if (!$data->delete()) {
            return $this->sendError(__('response.FailedDeleteData'));
        }

        return $this->sendResponse(
            [], __('response.SuccessDeleteData'), 204
        );
    }

    public function status(Request $request)
    {
        $data = Perusahaan::where('status',$request->status)->count();
       
        return $data;
    }

    public function export(Request $request)
    {
        //triwulan 1
        $year = date('Y',strtotime('now'));
        $date1 = Carbon::parse($year.'-01')->startofMonth()->toDateString().' 00:01';
        $date2 = Carbon::parse($year.'-03')->endOfMonth()->toDateString().' 23:59';
        $date1 = date('Y',strtotime('now'));
        $data = StatusPerusahaan::whereYear('created_at');
       
        return $this->sendResponse(
            [
                'data' => $data,
                'pagination' => true,
            ], __('response.SuccessGetData'), 200
        );
    }
}
