<?php

namespace App\Http\Requests\Admin\Master\Branch;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="BranchConfigRequest",
 *      description="Branch Config Request",
 *      type="object",
 *      required={"type", "key", "value"}
 * )
 */
class BranchConfigRequest extends FormRequest
{
    use RequestTrait;
     /**
      * Type prop
      *
      * @OA\Property(
      *      title="type", example="string"
      * ),
      */
      protected $type;
       /**
      * Key prop
      *
      * @OA\Property(
      *      title="key", example="SAMPLE_RULE"
      * ),
      */
    protected $key;
     /**
      * Value prop
      *
      * @OA\Property(
      *      title="value", example="This rule is for sample"
      * ),
      */
      protected $value;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'      => 'required|string|max:191',
            'key'       => 'required|string|max:191',
            'value'     => 'nullable|string',
        ];
    }
}
