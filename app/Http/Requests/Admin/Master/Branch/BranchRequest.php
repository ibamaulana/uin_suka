<?php

namespace App\Http\Requests\Admin\Master\Branch;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="BranchRequest",
 *      description="General Branch Request",
 *      type="object",
 *      required={"name", "address", "regno", "website", "contact", "image", "config"}
 * )
 */
class BranchRequest extends FormRequest
{
    use RequestTrait;
     /**
      * Name prop
      *
      * @OA\Property(
      *      title="name", example="SmartJen"
      * ),
      */
    protected $name;
     /**
      * Address prop
      *
      * @OA\Property(
      *      title="address", example="Woodsland, Singapore"
      * ),
      */
    protected $address;
     /**
      * Regno prop
      *
      * @OA\Property(
      *      title="regno", example="sj-012345"
      * ),
      */
    protected $regno;
     /**
      * Website prop
      *
      * @OA\Property(
      *      title="website", example="https://www.smartjen.com/"
      * ),
      */
    protected $website;
     /**
      * Contact prop
      *
      * @OA\Property(
      *      title="contact", example="+6598219996"
      * ),
      */
    protected $contact;
     /**
      * Image prop
      *
      * @OA\Property(
      *      title="image", example="https://www.smartjen.com/img"
      * ),
      */
    protected $image;
     /**
      * Config prop
      *
      * @OA\Property(
      *      title="config", example="{}"
      * ),
      */
    protected $config;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string|min:7|max:191',
            'address'      => 'nullable|string|max:191',
            'regno'        => 'required|string|max:10',
            'website'      => 'required|string|max:64',
            'contact'      => 'nullable|string|max:20',
            'image'        => 'nullable|string',
            'config'       => 'required',
        ];
    }
}
