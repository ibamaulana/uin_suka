<?php

namespace App\Http\Requests\Admin\Master;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="ClassRequest",
 *      description="General Class Request",
 *      type="object",
 *      required={"name","suject_stage_id", "branch_id", "day", "end_hour", "start_hour"}
 * )
 */
class ClassRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Name prop
     *
     * @OA\Property(
     *      title="name", example="Sample Class"
     * ),
     */
    protected $name;
    /**
     * Subject Stage prop
     *
     * @OA\Property(
     *      title="suject_stage_id", example="1"
     * ),
     */
      protected $subject_stage_id;
    /**
     * Day prop
     *
     * @OA\Property(
     *      type="string",
     *      enum= {"monday","tuesday","wednesday","thursday","friday","saturday","sunday"},
     *      example="monday"
     * ),
     */
      protected $day;
    /**
     * Start Date prop
     *
     * @OA\Property(
     *      title="start_date", example="2021-06-1"
     * ),
     */
    protected $start_date;
    /**
     * End Date prop
     *
     * @OA\Property(
     *      title="end_date", example="2022-06-30"
     * ),
     */
      protected $end_date;
    /**
     * Start Hour prop
     *
     * @OA\Property(
     *      title="start_hour", example="08:00"
     * ),
     */
      protected $start_hour;
    /**
     * End Hour prop
     *
     * @OA\Property(
     *      title="end_hour", example="16:00"
     * ),
     */
      protected $end_hour;
    /**
     * Branch prop
     *
     * @OA\Property(
     *      title="branch_id", example="1"
     * ),
     */
      protected $branch_id;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|string|min:3|max:191',
            'subject_stage_id' => 'required|numeric',
            'day'              => [ 'required', Rule::in(['monday','tuesday','wednesday','thursday','friday','saturday','sunday'])],
            'start_hour'       => 'date_format:H:i',
            'end_hour'         => 'date_format:H:i|after:start_hour',
            'branch_id'        => 'required|numeric',
        ];
    }
}
