<?php

namespace App\Http\Requests\Admin\Master;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="LevelRequest",
 *      description="General Branch Request",
 *      type="object",
 *      required={"stage_id","number"}
 * )
 */
class LevelRequest extends FormRequest
{
    use RequestTrait;
     /**
      * StageID prop
      *
      * @OA\Property(
      *      title="stage_id", example="1"
      * ),
      */
    protected $stage_id;
     /**
      * Number prop
      *
      * @OA\Property(
      *      title="number", example="4"
      * ),
      */
      protected $number;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stage_id' => 'required|numeric',
            'number'   => 'required|numeric',
        ];
    }
}
