<?php

namespace App\Http\Requests\Admin\Master;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="ProfessionRequest",
 *      description="General Branch Request",
 *      type="object",
 *      required={"name"}
 * )
 */
class ProfessionRequest extends FormRequest
{
    use RequestTrait;
     /**
      * Name prop
      *
      * @OA\Property(
      *      title="name", example="Sample Profession"
      * ),
      */
    protected $name;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"        => 'required|string|min:3|max:191',
        ];
    }
}
