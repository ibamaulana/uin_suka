<?php

namespace App\Http\Requests\Admin\Master;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="StrandRequest",
 *      description="General Branch Request",
 *      type="object",
 *      required={"name", "subject_stage_id "}
 * )
 */
class StrandRequest extends FormRequest
{
    use RequestTrait;
     /**
      * Name prop
      *
      * @OA\Property(
      *      title="name", example="Critical Thinking"
      * ),
      */
    protected $name;
     /**
      * Subject stage ID prop
      *
      * @OA\Property(
      *      title="subject_stage_id", example="7"
      * ),
      */
      protected $subject_stage_id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|string|min:3|max:191',
            'subject_stage_id' => 'required|numeric',
        ];
    }
}
