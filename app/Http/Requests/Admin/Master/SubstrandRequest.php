<?php

namespace App\Http\Requests\Admin\Master;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="SubstrandRequest",
 *      description="General Branch Request",
 *      type="object",
 *      required={"name", "strand_id "}
 * )
 */
class SubstrandRequest extends FormRequest
{
    use RequestTrait;
     /**
      * Name prop
      *
      * @OA\Property(
      *      title="name", example="Critical Thinking"
      * ),
      */
    protected $name;
     /**
      * Strand ID prop
      *
      * @OA\Property(
      *      title="strand_id", example="7"
      * ),
      */
      protected $strand_id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|string|min:3|max:191',
            'subject_stage_id' => 'required|numeric',
        ];
    }
}
