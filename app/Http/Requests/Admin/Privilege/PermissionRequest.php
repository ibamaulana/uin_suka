<?php

namespace App\Http\Requests\Admin\Privilege;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="PermissionRequest",
 *      description="General permission request",
 *      type="object",
 *      required={"name","guard_name"}
 * )
 */
class PermissionRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Name prop
     *
     * @OA\Property(
     *      title="name", example="admin.feature.*"
     * ),
     */
    protected $name;
    /**
     * Guard name prop
     *
     * @OA\Property(
     *      title="guard_name", example="api"
     * ),
     */
    protected $guard_name;

    /**
     * Determine if the permission is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string|min:3|max:191',
            'guard_name' => 'required|string|max:191',
        ];
    }
}
