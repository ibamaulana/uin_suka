<?php

namespace App\Http\Requests\Admin\Privilege;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="RolePermissionRequest",
 *      description="General permission request",
 *      type="object",
 *      required={"permissions"}
 * )
 */
class RolePermissionRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Permissions prop
     *
     * @OA\Property(
     *      title="permissions", example="[]"
     * ),
     */
      protected $permissions;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permissions' => 'required'
        ];
    }
}
