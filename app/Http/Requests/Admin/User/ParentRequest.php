<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="ParentRequest",
 *      description="General parent request",
 *      type="object",
 *      required={"user_id"}
 * )
 */
class ParentRequest extends FormRequest
{
    use RequestTrait;
    /**
     * UserId prop
     *
     * @OA\Property(
     *      title="user_id", example="1"
     * ),
     */
    protected $user_id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
