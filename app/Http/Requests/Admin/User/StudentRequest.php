<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="StudentRequest",
 *      description="General student request",
 *      type="object",
 *      required={}
 * )
 */
class StudentRequest extends FormRequest
{
    use RequestTrait;
    /**
     * SchoolId prop
     *
     * @OA\Property(
     *      title="school_id", example="1"
     * ),
     */
    protected $school_id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
