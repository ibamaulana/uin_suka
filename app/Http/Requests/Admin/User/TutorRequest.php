<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="TutorRequest",
 *      description="General tutor request",
 *      type="object",
 *      required={}
 * )
 */
class TutorRequest extends FormRequest
{
    use RequestTrait;
    /**
     * ProfessionId prop
     *
     * @OA\Property(
     *      title="profession", example="1"
     * ),
     */
    protected $profession_id;
    /**
     * SchoolId prop
     *
     * @OA\Property(
     *      title="school_id", example="1"
     * ),
     */
    protected $school_id;
    /**
     * Biography prop
     *
     * @OA\Property(
     *      title="biography", example="1"
     * ),
     */
    protected $biography;
    /**
     * Work prop
     *
     * @OA\Property(
     *      title="work", example="Teacher at Private School"
     * ),
     */
    protected $work;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
