<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="LoginRequest",
 *      description="Authenticate user by email and password",
 *      type="object",
 *      required={"name", "email", "password"}
 * )
 */

class LoginRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Email prop
     *
     * @OA\Property(
     *      title="email", example="admin@mail.com"
     * )
     * @var          string
     */
    protected $email;
     /**
      * Password prop
      *
      * @OA\Property(
      *      title="password", example="password"
      * )
      * @var          string
      */
    protected $password;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string',
            'password' => 'required|string'
        ];
    }
}
