<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="RegisterRequest",
 *      description="Create account",
 *      type="object",
 *      required={"name", "email", "password","password_confirmation","role"}
 * )
 */

class RegisterRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Name prop
     *
     * @OA\Property(
     *      title="name", example="John Doe"
     * )
     * @var          string
     */
    protected $name;
    /**
     * Username prop
     *
     * @OA\Property(
     *      title="username", example="jhondoe"
     * )
     * @var          string
     */
    protected $username;
    /**
     * Email prop
     *
     * @OA\Property(
     *      title="email", example="johndoe@mail.com"
     * )
     * @var          string
     */
    protected $email;
     /**
      * Password Confirmation prop
      *
      * @OA\Property(
      *      title="password_confirmation", example="password"
      * )
      * @var          string
      */
    protected $password_confirmation;
    /**
     * Password prop
     *
     * @OA\Property(
     *      title="password", example="password"
     * )
     * @var          string
     */
    protected $password;
    /**
     * Role prop
     *
     * @OA\Property(
     *      title="role", example="student"
     * )
     * @var          string
     */
    protected $role;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'username' => 'required|max:30|alpha_dash|unique:users',
            'email' => 'required|email:filter|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' =>  'nullable|max:16',
            'role' => 'required',
        ];
    }
}
