<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="PasswordUpdateRequest",
 *      description="Request user update password",
 *      type="object",
 *      required={"password", "password_confirmation"}
 * )
 */
class PasswordUpdateRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Password prop
     *
     * @OA\Property(
     *      title="password", example="password"
     * )
     * @var          string
     */
    protected $password;
    /**
     * Password Confirmation prop
     *
     * @OA\Property(
     *      title="password_confirmation", example="password"
     * )
     * @var          string
     */
    protected $password_confirmation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:6'
        ];
    }
}
