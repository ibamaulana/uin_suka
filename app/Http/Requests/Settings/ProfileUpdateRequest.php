<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Swagger schema
 *
 * @OA\Schema(
 *      title="ProfileUpdateRequest",
 *      description="Request user update profile",
 *      type="object",
 *      required={"name", "username", "email", "phone_number", "picture"}
 * )
 */
class ProfileUpdateRequest extends FormRequest
{
    use RequestTrait;

    /**
     * Name prop
     *
     * @OA\Property(
     *      title="name", example="John Doe"
     * )
     * @var          string
     */
    protected $name;
    /**
     * Username prop
     *
     * @OA\Property(
     *      title="username", example="jhondoe"
     * ),
     */
    protected $username;
    /**
     * Email prop
     *
     * @OA\Property(
     *      title="email", example="johndoe@mail.com"
     * )
     * @var          string
     */
    protected $email;
    /**
     * Phone Number prop
     *
     * @OA\Property(
     *      title="phone_number", example="+6585000000000"
     * ),
     */
    protected $phone_number;
    /**
     * Picture Prop
     *
     * @OA\Property(
     *      title="picture", example="image.jpg",
     *      type="file", format="binary"
     * ),
     */
    protected $picture;
      /**
       * Determine if the user is authorized to make this request.
       *
       * @return bool
       */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'username' => 'required|string|alpha_dash',
            'email' => 'required|email',
            'phone_number' => 'required|max:20',
        ];
    }
}
