<?php

    namespace App\Libs\Hash;

    use Illuminate\Contracts\Hashing\Hasher as HasherContract;
    use Illuminate\Hashing\AbstractHasher;

class Sha256Hash extends AbstractHasher implements HasherContract
{

    /**
     * Hash the given value.
     *
     * @param string $value
     * @param array  $options
     *
     * @return string
     */
    public function make($value, array $options = array()) : string
    {
        //Options has no use.
        $options = [];
        return hash('sha256', $value);
    }

    /**
     * Check the given plain value against a hash.
     *
     * @param string $value
     * @param string $hashedValue
     * @param array  $options
     *
     * @return bool
     */
    public function check($value, $hashedValue, array $options = array()) : bool
    {
        //Options has no use.
        $options = [];
        return $this->make($value) === $hashedValue;
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param string $hashedValue
     * @param array  $options
     *
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = array()) : bool
    {
        //Options and hash value has no use.
        $options = [];
        $hashedValue = '';
        return false;
    }

}
