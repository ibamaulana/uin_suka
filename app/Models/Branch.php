<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;

    protected $table = 'branches';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable inputs
     *
     * @var array
     */
    protected $fillable = [
        'name', 'regno', 'website',
        'address', 'contact', 'image', 'config'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get config list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function branchConfigs()
    {
        return $this->hasMany(BranchConfig::class);
    }

    /**
     * Get user list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
