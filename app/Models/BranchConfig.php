<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchConfig extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable Inputs
     *
     * @var array
     */
    protected $fillable = ['branch_id', 'type', 'key', 'value', 'created_at', 'updated_at'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get config branch of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
