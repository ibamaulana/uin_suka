<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassModel extends Model
{
    use SoftDeletes;

    protected $table = 'classes';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = [
        'name', 'subject_stage_id', 'day', 'start_hour', 'end_hour', 'start_date', 'end_date', 'branch_id',
    ];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get subject stage from class. Example: EnglishPrimary
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subjectStage()
    {
        return $this->belongsTo(SubjectStage::class);
    }

    /**
     * Get student of class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classStudents()
    {
        return $this->hasMany(ClassStudent::class, 'class_id');
    }

    /**
     * Get tutor of class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classTutors()
    {
        return $this->hasMany(ClassTutor::class, 'class_id');
    }

    /**
     * Get branch of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
