<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPesanan extends Model
{
    use SoftDeletes;

    protected $table = 'order_details';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['*'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get subject stage list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pesanan()
    {
        return $this->belongsTo(Pesanan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
