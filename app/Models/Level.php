<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['stage_id', 'number'];

    protected $appends = [
        'stage_name',
    ];

    /**
     * Get the primary role attribute.
     *
     * @return string
     */
    public function getStageNameAttribute()
    {
        if ($this->stage) {
            return $this->stage->name.' '.$this->number;
        }
        return null;

    }

    // =============
    // ORM RELATION
    // =============

    /**
     * Get stage of the level
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     * Get list of level substrands
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function levelSubstrands()
    {
        return $this->hasMany(LevelSubstrand::class);
    }
}
