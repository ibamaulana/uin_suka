<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelSubstrand extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['level_id', 'substrand_id'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get substrand of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function substrand()
    {
        return $this->belongsTo(Substrand::class);
    }

    /**
     * Get Level of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
