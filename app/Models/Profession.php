<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['name'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get list tutor of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tutors()
    {
        return $this->hasMany(Tutor::class);
    }
}
