<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class School extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = [
        'stage_id', 'name', 'description',
        'picture', 'address',
    ];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get stage of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     * Get student list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function students()
    {
        return $this->hasMany(Student::class);
    }

    /**
     * Get tutor list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tutors()
    {
        return $this->hasMany(Tutor::class);
    }
}
