<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Strand extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = [
        'subject_stage_id', 'name',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'subject_stage_name',
    ];

    /**
     * Get the subject stage name attribute.
     *
     * @return string
     */
    public function getSubjectStageNameAttribute()
    {
        if ($this->subjectStage) {
            $stageName = $this->subjectStage->stage_name;
            $subjectName = $this->subjectStage->subject_name;
            return $stageName." ".$subjectName;
        }
        return null;
    }

    // =============
    // ORM RELATION
    // =============

    /**
     * Get subject stage of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subjectStage()
    {
        return $this->belongsTo(SubjectStage::class);
    }

    /**
     * Get list substrand of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function substrands()
    {
        return $this->hasMany(Substrand::class);
    }
}
