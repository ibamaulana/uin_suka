<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'school_id',
    ];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get user of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get school of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Get class students of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classStudents()
    {
        return $this->hasMany(ClassStudent::class);
    }

    /**
     * Get parent list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentStudents()
    {
        return $this->hasMany(ParentStudent::class);
    }
}
