<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubjectStage extends Model
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['stage_id', 'subject_id'];

    protected $appends = [
        'stage_name',
        'subject_name',
    ];

    /**
     * Get the stage name attribute.
     *
     * @return string
     */
    public function getStageNameAttribute()
    {
        if ($this->stage) {
            return $this->stage->name;
        }
        return null;
    }

    /**
     * Get the subject name attribute.
     *
     * @return string
     */
    public function getSubjectNameAttribute()
    {
        if ($this->subject) {
            return $this->subject->name;
        }
        return null;
    }


    // =============
    // ORM RELATION
    // =============

    /**
     * Get subject of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Get stage of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     * Get class list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes()
    {
        return $this->hasMany(Classes::class);
    }

    /**
     * Get strand list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function strands()
    {
        return $this->hasMany(Strand::class);
    }
}
