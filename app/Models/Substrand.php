<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Substrand extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['strand_id', 'name'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'strand_name',
    ];

    /**
     * Get the primary role attribute.
     *
     * @return string
     */
    public function getStrandNameAttribute()
    {
        if ($this->strand) {
            return $this->strand->name;
        }
        return null;
    }

    // =============
    // ORM RELATION
    // =============

    /**
     * Get strand of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function strand()
    {
        return $this->belongsTo(Strand::class);
    }

    /**
     * Get level substrand list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function levelSubstrands()
    {
        return $this->hasMany(LevelSubstrand::class);
    }
}
