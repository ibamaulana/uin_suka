<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenis extends Model
{
    // use SoftDeletes;

    protected $table = 'tenis_setting';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Fillable input
     *
     * @var array
     */
    protected $fillable = ['*'];

    // =============
    // ORM RELATION
    // =============

    /**
     * Get subject stage list of this record belong
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function perusahaan()
    // {
    //     return $this->belongsTo(Perusahaan::class);
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
