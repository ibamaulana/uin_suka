<?php

namespace App\Repositories;

use App\Models\Branch;
use App\Models\BranchConfig;

class BranchConfigRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new BranchConfig;
    }

    /**
     * Store data to db.
     *
     * @param array $data
     *
     * @return User
     */
    public function createSpecific(array $data, $id)
    {
        $data['key'] = str_replace(['\'', '"',',' , ';', '<', '>', '-', ' '], '_', $data['key']);

        $cfg = $this->model->create([
            'branch_id' => $id,
            'type'      => strtolower($data['type']),
            'key'       => strtoupper($data['key']),
            'value'     => $data['value'],
        ]);
        return $cfg;
    }

    /**
     * Store mass data to db.
     *
     * @param array $data
     *
     * @return User
     */
    public function createToAll(array $data)
    {
        $branches = Branch::get();
        foreach ($branches as $branch) {
            $cfg = $this->createSpecific($data, $branch->id);
            if (!$cfg) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get record by BranchId and Key
     *
     * @param  mixed $branchId
     * @param  mixed $key
     * @return Model
     */
    public function getByBranchIdAndKey($branchId, $key)
    {
        $data = $this->model->where('branch_id', $branchId)
                            ->where('key', strtoupper($key))->first();
        if ($data) {
            return $data;
        }
        return false;
    }

    /**
     * Update record by BranchId and Key
     *
     * @param  mixed $branchId
     * @param  mixed $key
     * @return Model
     */
    public function updateByBranchIdAndKey($data, $branchId, $key)
    {
        $data = $this->model->where('branch_id', $branchId)
                            ->where('key', strtoupper($key))->first();
        if ($data) {
            return $data->update([
                    'type' => $data['type'],
                    'value' => $data['value']
                ]);
        }
        return false;
    }

    /**
     * Delete record by BranchId and Key
     *
     * @param  mixed $branchId
     * @param  mixed $key
     *
     * @return void
     */
    public function deleteByBranchIdAndKey($branchId, $key)
    {
        $data = $this->getByBranchIdAndKey($branchId, $key);
        if ($data) {
            return $data->delete();
        }
        return false;
    }

    public function deleteToAll($key)
    {
        return $this->model->where('key',strtoupper($key))->delete();
    }
}
