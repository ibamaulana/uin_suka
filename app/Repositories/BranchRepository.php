<?php

namespace App\Repositories;

use App\Models\Branch;

class BranchRepository extends Repository
{
    private $configRepository;
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Branch;
        $this->configRepository = new BranchConfigRepository;
    }

    /**
     * Store data to db
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data)
    {
        $data['contact'] = phone_validate($data['contact']);
        $data['config']  = json_encode($data['config']);
        return $this->model->create($data);
    }

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    public function update(array $data, $id)
    {
        $model = $this->find($id);
        $branchId = $model->id;
        if ($model) {
            if (isset($data['contact'])) {
                $data['contact'] = phone_validate($data['contact']);
            }

            if (isset($data['config'])) {
                $defaultKey = [ "BRANCH_TAG", "BRANCH_NAME", "BRANCH_TITLE",
                                "BRANCH_EMAIL", "BRANCH_LOGO", "BRANCH_LOGO_PDF",
                                "BRANCH_HEADER_PDF", "BRANCH_HOME_CONTENT", "BRANCH_FOOTER",
                                "BRANCH_CSS", "BRANCH_REG_FORM", "BRANCH_ADD_USER",
                                "BRANCH_USER_PRIVILEGE_ACTIVE", "BRANCH_ATS", "BRANCH_TTS",
                                "BRANCH_AWG", "BRANCH_TID", "BRANCH_FUN_QUIZ",
                                "BRANCH_SUBJECT_MATH", "BRANCH_SUBJECT_ENGLISH", "BRANCH_SUBJECT_SCIENCE",
                                "ZOOM_REDIRECT_URI", "ZOOM_CLIENT_ID", "ZOOM_CLIENT_SECRET"];
                // Logic Flow
                // If array key in $defaultConfig, store on different variable, and do unset to quarantine the value.
                // the remaining will stored in branch_config model
                // the quarantine branch will be encoded and store to config column
                $baseConfig = json_decode($model->config);
                foreach ($data['config'] as $key => $value){
                    if (in_array($key,$defaultKey)) {
                        // Update config that have same key, then remove it
                        $baseConfig->$key = $value;
                        unset($data['config'][$key]);
                    }
                }
                $extraConfig = $data['config'];
                $data['config']  = json_encode($baseConfig);
                // Create or update via branch config
                foreach ($extraConfig as $key => $value) {
                    $formattedCfg = [
                        'type'  => $value['type'],
                        'key'   => $key,
                        'value' => $value['value'],
                    ];
                    $newCfg = $this->configRepository->updateByBranchIdAndKey($formattedCfg, $branchId, $key);
                    if (!$newCfg) {
                        $this->configRepository->createSpecific($formattedCfg, $branchId);
                    }
                }
            }
            $model->update($data);
            return $this->getById($branchId);
        }
        return 0;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     *
     * @return mixed
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->query() :
            $this->model->where('name', 'like', '%'.$query.'%')
            ->orWhere('address', 'like', '%'.$query.'%')
            ->orWhere('regno', 'like', '%'.$query.'%')
            ->orWhere('contact', 'like', '%'.$query.'%');
    }

    /**
     * Find data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function getById(int $id)
    {
        $result = $this->model->find($id);
        if ($result) {
            $config = json_decode($result->config, true);
        }

        $branchConfig = $result->branchConfigs;
        foreach ($branchConfig as $cfg) {
            $config[$cfg->key] = [
                'type'  => $cfg->type,
                'value' => $cfg->value,
            ];
        }
        $result->config = $config;
        return unset_collection($result, ['branch_configs']);
    }
}
