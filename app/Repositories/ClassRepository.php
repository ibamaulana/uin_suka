<?php

namespace App\Repositories;

use App\Models\ClassModel;

class ClassRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new ClassModel;
        $this->stageRepository = new StageRepository;
        $this->classStudentRepository = new ClassStudentRepository;
        $this->classTutorRepository = new ClassTutorRepository;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     * @param mixed $branchId
     *
     * @return mixed
     */
    public function searchWithOption($query, $opt = [])
    {
        $option = [
            'branch' => value_of_key($opt, 'branch'),
            'archive' => value_of_key($opt, 'archive'),
        ];
        $archive = $option['archive'];
        $branchId = $option['branch'];
        $orm = empty($branchId) ?
                $this->model->with('classStudents', 'classTutors'):
                $this->model->with('classStudents', 'classTutors')->where('branch_id', $branchId);
        $result = empty($query) ?
                    $orm:
                    $orm->whereHas(
                        "subjectStage", function ($q) use ($query) {
                                $q->where('stage_name', 'like', '%'.$query.'%')
                                    ->orWhere('subject_name', 'like', '%'.$query.'%');
                        }
                    )->orWhere('number', $query);
        if (!empty($archive)) {
            return $result->onlyTrashed();
        }
        return $result;
    }

    /**
      * Get all record
      *
      * @param int    $branchId
      * @param int    $size
      * @param string $search
      * @param string $sortField
      * @param bool   $sortAsc
      *
      * @return void
      */
      public function getAllWithOption(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true, $option = [])
      {
          return simplify_paginate(
              $this->searchWithOption($search ?? null, $option)
                  ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                  ->paginate($size ?? 10)
          );
      }

    /**
     * Get record by Id
     *
     * @param mixed $id
     *
     * @return void
     */
    public function find(int $id)
    {
        $result = $this->model->with('classStudents', 'classTutors')->find($id);
        return $result;
    }

    /**
     * Get all student and tutor that asociate on this class
     *
     * @param int $id
     *
     * @return void
     */
    public function getClassPeopleByClassId($id)
    {
        $class = $this->model->find($id);
        if ($class) {
            $student = $class->classStudents->pluck('student_id');
            $tutor = $class->classTutors->pluck('tutor_id');
            // $student = collection_simplify($class->classStudents, 'id', 'id');
            // $tutor = collection_simplify($class->classTutors, 'id', 'id');
            return [
                'student_id' => $student,
                'tutor_id' => $tutor
            ];
        }
        return 0;
    }

    /**
     * Update all student and tutor that asociate on this class
     *
     * @param int   $id
     * @param array $input
     *
     * @return void
     */
    public function updateClassPeopleByClassId(int $id, array $input)
    {
        $class = $this->model->find($id);
        if ($class) {
            //remove all
            $this->classTutorRepository->deleteByClassId($id);
            $this->classStudentRepository->deleteByClassId($id);
            //set all
            foreach ($input['tutor_id'] as $tutorId) {
                $this->classTutorRepository->create(
                    [
                    'class_id' => $id,
                    'tutor_id' => $tutorId
                    ]
                );
            }
            foreach ($input['student_id'] as $studentId) {
                $this->classStudentRepository->create(
                    [
                    'class_id' => $id,
                    'student_id' => $studentId
                    ]
                );
            }
            $result = $this->getClassPeopleByClassId($id);
            return $result;
        }
        return 0;
    }
}
