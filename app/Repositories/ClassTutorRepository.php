<?php

namespace App\Repositories;

use App\Models\ClassTutor;

class ClassTutorRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new ClassTutor;
    }

    /**
     * Delete By ClassId
     *
     * @param mixed $id
     *
     * @return void
     */
    public function deleteByClassId($id)
    {
        $data = $this->model->where('class_id', $id);
        return $data->delete();
    }
}
