<?php

namespace App\Repositories\Contract;

abstract class RepositoryAbstract
{

    protected $model;

    /**
     * Get all data
     *
     * @return Collection
     */
    abstract public function all();

    /**
     * Get all data order by created at desc
     *
     * @return Collection
     */
    abstract function getLatest();

    /**
     * Get all data order by created at desc
     *
     * @param string $column
     * @param string $method
     *
     * @return Collection
     */
    abstract public function getOrderBy(string $column, string $method = 'asc');

    /**
     * Store data to db
     *
     * @param array $data
     *
     * @return Model
     */
    abstract public function create(array $data);

    /**
     * Store data to db
     *
     * @param array $data
     *
     * @return Model
     */
    abstract public function store(array $data);

    /**
     * Find data by id
     *
     * @param int $id
     *
     * @return Model
     */
    abstract public function find(int $id);

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    abstract public function update(array $data, int $id);

    /**
     * Delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    abstract public function delete(int $id);

    /**
     * Delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    abstract public function destroy(int $id);
}
