<?php

namespace App\Repositories\Contract;

trait RepositoryTrait
{

    /**
     * Do opeartion as matching value to existing records
     * If record already exist, do create, else update the value
     *
     * @param mixed $attribute
     * @param mixed $value
     *
     * @return mixed
     */
    public function updateOrCreate($attribute, $value)
    {
        return $this->model::updateOrCreate($attribute, $value);
    }
}
