<?php

namespace App\Repositories\Contract;

trait RepositoryUserTrait
{
    /**
     * Store data to db based on fillabe value
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data)
    {
        $fillable = $this->model->getFillable();
        $data = array_pluck($data, $fillable);
        return $this->model->create($data);
    }

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    public function update(array $data, int $id)
    {
        $model = $this->find($id);
        consoleprint($id);
        if ($model) {
            $fillable = $this->model->getFillable();
            $data = array_pluck($data, $fillable);
            $model->update($data);
            return $model;
        }
        return 0;
    }

    /**
     * Get data by user id
     *
     * @param mixed $id
     *
     * @return Model
     */
    public function findByUserID($id)
    {
        $data = $this->model->where('user_id', $id)->first();
        return $data;
    }
    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    public function updateByUserID(array $data, int $id)
    {
        $model = $this->findByUserID($id);
        if ($model) {
            $model->update($data);
            return $model;
        }
        return 0;
    }
    /**
     * Delete data by user id
     *
     * @param int $id
     *
     * @return void
     */
    public function deleteByUserID($id)
    {
        $data = $this->model->where('user_id', $id)->first();
        return $data->delete();
    }

    /**
     * Force delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function forceDeleteByUserID(int $id)
    {
        $model = method_exists($this->model, 'withTrashed') ?
                $this->model->withTrashed()->where('user_id', $id)->first() :
                $this->model->where('user_id', $id)->first();
        if ($model) {
            return $model->forceDelete();
        }
        return 0;
    }

    /**
     * Get all record as select2
     * Return ["text" = user full name, "value" = parentId/ tutorId/ studentId]
     *
     * @param mixed $branch
     *
     * @return void
     */
    public function select2WithOption($option = [])
    {
        $records = $this->searchWithOption(null, $option)->get()->pluck('user.name', 'id');
        $records = $records->toArray();
        return array_to_select2($records);
    }

     /**
      * Get all record
      *
      * @param int    $branchId
      * @param int    $size
      * @param string $search
      * @param string $sortField
      * @param bool   $sortAsc
      *
      * @return void
      */
      public function getAllWithOption(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true, $option = [])
      {
          return simplify_paginate(
              $this->searchWithOption($search ?? null, $option)
                  ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                  ->paginate($size ?? 10)
          );
      }
}
