<?php

namespace App\Repositories;

use App\Models\Level;

class LevelRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Level;
        $this->stageRepository = new StageRepository;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     * @param mixed $stageId
     *
     * @return mixed
     */
    public function searchWithStage($query, $stageId = null)
    {
        $ormStage = empty($stageId) ?
                $this->model->with('stage', 'levelSubstrands'):
                $this->model->with('stage', 'levelSubstrands')->where('stage_id', $stageId);
        $result = empty($query) ?
                    $ormStage:
                    $ormStage->whereHas(
                        "stage", function ($q) use ($query) {
                                $q->where('name', 'like', '%'.$query.'%');
                        }
                    )->orWhere('number', $query);
        return $result;
    }

    /**
     * Get all record
     *
     * @param mixed  $stageId
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAllWithStage($stageId, int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->searchWithStage($search ?? null, $stageId ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get record by Id
     *
     * @param mixed $id
     *
     * @return void
     */
    public function find(int $id)
    {
        $result = $this->model->with('levelSubstrands')->find($id);
        return $result;
    }

    /**
     * Get Level by fill param stage id and level number
     *
     * @param string $stageId
     * @param string $levelNumber
     *
     * @return Model
     */
    public function findByStageIdAndNumber(string $stageId, string $levelNumber)
    {
        $level = $this->model->where("stage_id", $stageId)->where("number", $levelNumber)->first();
        if ($level ) {
            return $level;
        }
        return null;
    }

    /**
     * Get Level by fill param stage name and level number
     *
     * @param mixed $stageName
     * @param mixed $levelNumber
     *
     * @return void
     */
    public function findByStageNameAndNumber(string $stageName, string $levelNumber)
    {
        $stage = $this->stageRepository->findByName($stageName)->id;
        $stageId = $stage->id;
        $level = $this->findByStageIdAndNumber($stageId, $levelNumber);
        if ($level ) {
            return $level;
        }
        return null;
    }

    /**
     * Get all record as select2
     *
     * @param mixed $stageId
     *
     * @return void
     */
    public function select2WithStage($stageId = null)
    {
        $records = $this->searchWithStage(null, $stageId)->get()->pluck('stage_name', 'id');
        $records = $records->toArray();
        return array_to_select2($records);
    }
}
