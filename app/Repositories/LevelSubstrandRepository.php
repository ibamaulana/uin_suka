<?php

namespace App\Repositories;

use App\Models\LevelSubstrand;

class LevelSubstrandRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new LevelSubstrand;
    }

    /**
     * Get by levelId and substrandId
     *
     * @param mixed $levelId
     * @param mixed $substrandId
     *
     * @return Model
     */
    public function findByLevelIdAndSubstrandId($levelId, $substrandId)
    {
        $data = $this->model->where('level_id', $levelId)->where('substrand_id', $substrandId)->first();
        if ($data) {
            return $data;
        }
        return 0;
    }

    /**
     * Delete by levelId and substrandId
     *
     * @param mixed $levelId
     * @param mixed $substrandId
     *
     * @return void
     */
    public function deleteByLevelIdAndSubstrandId($levelId, $substrandId)
    {
        $data = $this->findByLevelIdAndSubstrandId($levelId, $substrandId);
        if ($data) {
            return $data->delete();
        }
        return 0;
    }
}
