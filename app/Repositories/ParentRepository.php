<?php

namespace App\Repositories;

use App\Models\ParentModel;
use App\Models\Student;
use App\Repositories\Contract\RepositoryUserTrait;

class ParentRepository extends Repository
{
    use RepositoryUserTrait;
    protected $parentStudentRepository;
    protected $studentRepository;
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new ParentModel;
        $this->parentStudentRepository = new ParentStudentRepository;
        $this->studentRepository = new StudentRepository;
    }

    /**
     * Store data to db based on fillabe value
     *
     * @param array $data
     *
     * @return ParentModel
     */
    public function create(array $data) : ParentModel
    {
        $fillable   = $this->model->getFillable();
        $studentIds = value_of_key($data, 'parent_of');
        $data       = array_pluck($data, $fillable);
        $parent     = $this->model->create($data);
        if ($parent && !is_null($studentIds)) {
            if (is_array($studentIds)) {
                // Catch if "parent_of" is array, so will do mass create record
                foreach ($studentIds as $id) {
                    $this->parentStudentRepository->create(
                        [
                        "parent_id" => $parent->id,
                        "student_id" => $id,
                        ]
                    );
                }
            } elseif (is_numeric($studentIds)) {
                // only one value of "parent_of"
                $this->parentStudentRepository->create(
                    [
                    "parent_id" => $parent->id,
                    "student_id" => $studentIds,
                    ]
                );
            }
        }
        return $parent;
    }

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    public function update(array $data, int $id)
    {
        $parent = $this->find($id);
        if ($parent) {
            $studentIds = value_of_key($data, 'parent_of');
            $fillable = $this->model->getFillable();
            $data = array_pluck($data, $fillable);
            $parent->update($data);

            if ($parent && !is_null($studentIds)) {
                // Remove record parentStudent
                foreach ($parent->parentStudents as $deleteRecord) {
                    $deleteRecord->delete();
                }

                if (is_array($studentIds)) {
                    // Catch if "parent_of" is array, so will do mass create record
                    foreach ($studentIds as $id) {
                        $this->parentStudentRepository->create(
                            [
                            "parent_id" => $parent->id,
                            "student_id" => $id,
                            ]
                        );
                    }
                } elseif (is_numeric($studentIds)) {
                    // only one value of "parent_of"
                    $this->parentStudentRepository->create(
                        [
                        "parent_id" => $parent->id,
                        "student_id" => $studentIds,
                        ]
                    );
                }
            }
            return $parent;
        }
        return 0;
    }

    /**
     * Get data by user id
     *
     * @param mixed $id
     *
     * @return Model
     */
    public function getWithStudent($id)
    {
        $data = $this->model->with('user')
            ->find($id);
        $student=[];
        if ($data->parentStudents) {
            foreach ($data->parentStudents as $parentStudent) {
                array_push($student, Student::with('user', 'school')->find($parentStudent['student_id']));
            }
        }
        $data['students'] = $student;
        return $data;
    }

    /**
     * Overload Search using querry
     *
     * @param mixed $query
     * @param int   $branchId
     *
     * @return mixed
     */
    public function searchWithOption($query, $opt=[])
    {
        $option = [
            'branch' => value_of_key($opt, 'branch'),
            'archive' => value_of_key($opt, 'archive'),
        ];
        $branchId = $option['branch'];
        $archive = $option['archive'];
        $orm = empty($branchId) ?
                $this->model
            ->with('user', 'parentStudents'):
                $this->model
            ->with('user', 'parentStudents')->whereHas(
                "user", function ($q) use ($branchId) {
                        $q->where('branch_id', $branchId);
                }
            );
        $result = empty($query) ?
                    $orm:
                    $orm->whereHas(
                        "user", function ($q) use ($query) {
                            $q->where('name', 'like', '%'.$query.'%');
                        }
                    );
        if (!empty($archive)) {
            return $result->onlyTrashed();
        }
        return $result;
    }

}
