<?php

namespace App\Repositories;

use App\Models\ParentStudent;

class ParentStudentRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new ParentStudent;
    }

    /**
     * Get record that associated with student Id and parent Id
     *
     * @param mixed $parentId
     * @param mixed $studentId
     *
     * @return Model
     */
    public function findByParentIdAndStudentId($parentId, $studentId)
    {
        $data = $this->model->where('parent_id', $parentId)->where('student_id', $studentId)->first();
        if ($data) {
            return $data;
        }
        return 0;
    }

    /**
     * Delete record that associated with student Id and parent Id
     *
     * @param mixed $parentId
     * @param mixed $studentId
     *
     * @return void
     */
    public function deleteByParentIdAndStudentId($parentId, $studentId)
    {
        $data = $this->findByParentIdAndStudentId($parentId, $studentId);
        if ($data) {
            return $data->delete();
        }
        return 0;
    }
}
