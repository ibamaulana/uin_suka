<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission as PermissionSpatie;

class PermissionRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new PermissionSpatie;
    }

    /**
     * Search query in Permission
     *
     * @param mixed $query
     *
     * @return Model
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->query() :
            $this->model->where('name', 'like', '%'.$query.'%')
            ->orWhere('guard_name', 'like', '%'.$query.'%');
    }
}
