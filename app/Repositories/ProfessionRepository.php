<?php

namespace App\Repositories;

use App\Models\Profession;

class ProfessionRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Profession;
    }
}
