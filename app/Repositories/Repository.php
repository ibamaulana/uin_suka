<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Repositories\Contract\RepositoryAbstract;
use App\Repositories\Contract\RepositoryTrait;

class Repository extends RepositoryAbstract
{
    use RepositoryTrait;
    protected $model;

    /**
     * Search query in Role
     *
     * @param mixed $query
     *
     * @return Model
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->query() :
            $this->model->where('name', 'like', '%'.$query.'%');
    }

    /**
     * Get all data
     *
     * @return Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Get all data order by created at desc
     *
     * @return Collection
     */
    public function getLatest()
    {
        return $this->model->latest()->get();
    }

    /**
     * Get all data order by created at desc
     *
     * @param string $column
     * @param string $method
     *
     * @return Collection
     */
    public function getOrderBy(string $column, string $method = 'asc')
    {
        return $this->model->orderBy($column, $method)->get();
    }

    /**
     * Store data to db
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Store data to db
     *
     * @param array $data
     *
     * @return Model
     */
    public function store(array $data)
    {
        return $this->create($data);
    }

    /**
     * Find data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return Model
     */
    public function update(array $data, int $id)
    {
        $model = $this->find($id);
        if ($model) {
            $model->update($data);
            return $model;
        }
        return 0;
    }

    /**
     * Delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function delete(int $id)
    {
        $model = $this->find($id);
        if ($model) {
            return $model->delete();
        }
        return 0;
    }

    /**
     * Force delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function forceDelete(int $id)
    {
        $model = method_exists($this->model, 'withTrashed') ?
                $this->model->withTrashed()->find($id) :
                $this->model->find($id);
        if ($model) {
            if( is_null($model->deleted_at) ){
                return $model->delete();
            }else{
                return $model->forceDelete();
            }
        }
        return 0;
    }

    /**
     * Restore data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function restore(int $id)
    {
        $model = $this->model->withTrashed()->find($id);
        if ($model) {
            return $model->restore();
        }
        return 0;
    }

    /**
     * Delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function destroy(int $id)
    {
        return $this->delete($id);
    }

    /**
     * Get all record
     *
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAll(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->search($search ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get all record as select2
     *
     * @param mixed $column
     * @param mixed $column2
     * @param mixed $query
     *
     * @return void
     */
    public function select2($column, $column2 = 'id', $query = null)
    {
        $records = $this->search($query??null)->pluck($column, $column2?? 'id');
        return array_to_select2($records);
    }

    /**
     * Get all record as select2
     *
     * @param mixed $column
     * @param mixed $query
     *
     * @return void
     */
    public function select2Identic($column, $query = null)
    {
        $records = $this->search($query??null)->pluck($column);
        return array_to_select2_identic($records);
    }
}
