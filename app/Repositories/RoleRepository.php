<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role as RoleSpatie;
use Spatie\Permission\Models\Permission as PermissionSpatie;

class RoleRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new RoleSpatie;
    }

    /**
     * Search query in Role
     *
     * @param mixed $query
     *
     * @return Model
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->query() :
            $this->model->where('name', 'like', '%'.$query.'%')
            ->orWhere('guard_name', 'like', '%'.$query.'%');
    }

    /**
     * Get all permission that associate in every roles
     *
     * @return array
     */
    public function getAllRolePermisisons() : array
    {
        $result = [];
        $roles = $this->model->all();
        foreach ($roles as $role) {
            $perms = $this->model->findByName($role->name)->permissions;
            $result[$role->name] = collection_simplify($perms, 'name', 'id');
        }
        return $result;
    }

    /**
     * Get all permission that associate in every roles
     *
     * @param int $id
     *
     * @return array
     */
    public function getRolePermisison(int $id)
    {
        $role = $this->model->find($id);
        if ($role) {
            $result = collection_simplify($role->permissions, 'name', 'id');
            return $result;
        }
        return 0;
    }

    /**
     * Get all permission that associate in every roles
     *
     * @param int   $id
     * @param array $perms
     *
     * @return array
     */
    public function updateRolePermisison(int $id, array $perms)
    {
        $role = $this->model->find($id);
        if ($role) {
            $role->revokePermissionTo(PermissionSpatie::get());
            $role->givePermissionTo($perms);
            $result = collection_simplify($role->permissions, 'name', 'id');
            return $result;
        }
        return 0;
    }
}
