<?php

namespace App\Repositories;

use App\Models\School;

class SchoolRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new School;
    }
}
