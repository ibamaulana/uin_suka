<?php

namespace App\Repositories;

use App\Models\Stage;

class StageRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Stage;
    }

    /**
     * Get stage by stage name
     *
     * @param mixed $name
     *
     * @return Model
     */
    public function findByName(string $name)
    {
        $stage = $this->model->where('name', $name)->with('levels')->first();
        if ($stage ) {
            return $stage;
        }
        return null;
    }

    /**
     * Create record
     *
     * @param array $data
     *
     * @return void
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update record by given Id
     *
     * @param array $data
     * @param int   $id
     *
     * @return void
     */
    public function update(array $data, $id)
    {
        $model = $this->find($id);
        if ($model) {
            $model->update($data);
            return $model;
        }
        return 0;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     *
     * @return mixed
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->with('levels') :
            $this->model->with('levels')->where('name', 'like', '%'.$query.'%');
    }

    /**
     * Get all record
     *
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAll(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->search($search ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get record by Id
     *
     * @param mixed $id
     *
     * @return void
     */
    public function getById(int $id)
    {
        $result = $this->model->find($id);
        return $result;
    }
}
