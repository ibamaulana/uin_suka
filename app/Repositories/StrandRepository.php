<?php

namespace App\Repositories;

use App\Models\Strand;

class StrandRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Strand;
    }
}
