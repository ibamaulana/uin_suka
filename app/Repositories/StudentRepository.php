<?php

namespace App\Repositories;

use App\Models\Student;
use App\Models\ParentModel;
use App\Repositories\Contract\RepositoryUserTrait;

class StudentRepository extends Repository
{
    use RepositoryUserTrait;
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Student;
    }

    /**
     * Get data by user id
     *
     * @param mixed $id
     *
     * @return Model
     */
    public function findWithParent($id)
    {
        $data = $this->model->with('user', 'school')
            ->find($id);
        $parent=[];
        if ($data->parentStudents) {
            foreach ($data->parentStudents as $parentStudent) {
                array_push($parent, ParentModel::with('user')->find($parentStudent->parent_id));
            }
        }
        $data['parents'] = $parent;
        return $data;
    }

    /**
     * Overload Search using querry
     *
     * @param mixed $query
     * @param int   $branchId
     *
     * @return mixed
     */
    public function searchWithOption($query, $opt = [])
    {
        $option = [
            'branch' => value_of_key($opt, 'branch'),
            'archive' => value_of_key($opt, 'archive'),
        ];
        $branchId = $option['branch'];
        $archive = $option['archive'];
        $orm = empty($branchId) ?
                $this->model
            ->with('user', 'school', 'parentStudents'):
                $this->model
            ->with('user', 'school', 'parentStudents')->whereHas(
                "user", function ($q) use ($branchId) {
                        $q->where('branch_id', $branchId);
                }
            );
        $result = empty($query) ?
                    $orm:
                    $orm->whereHas(
                        "user", function ($q) use ($query) {
                            $q->where('name', 'like', '%'.$query.'%');
                        }
                    )->orWhereHas(
                        "school", function ($q) use ($query) {
                                $q->where('name', 'like', '%'.$query.'%');
                        }
                    );
        if (!empty($archive)) {
            return $result->onlyTrashed();
        }
        return $result;
    }

}
