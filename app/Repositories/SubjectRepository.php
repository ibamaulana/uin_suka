<?php

namespace App\Repositories;

use App\Models\Subject;
use App\Models\Stage;

class SubjectRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Subject;
    }

    /**
     * Create record
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update record by given Id
     *
     * @param array $data
     * @param int   $id
     *
     * @return void
     */
    public function update(array $data, $id)
    {
        $model = $this->find($id);
        if ($model) {
            $model->update($data);
            return $model;
        }
        return 0;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     *
     * @return mixed
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->with('subjectStages') :
            $this->model->with('subjectStages')->where('name', 'like', '%'.$query.'%');
    }

    /**
     * Get all record
     *
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAll(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->search($search ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get data by user id
     *
     * @param mixed $id
     *
     * @return Model
     */
    public function getWithStage($id)
    {
        $data = $this->model->with('subjectStages')->find($id);
        $stages = [];
        if ($data->subjectStages) {
            foreach ($data->subjectStages as $subjectStages) {
                array_push($stages, Stage::find($subjectStages['stage_id']));
            }
        }
        $data['stages'] = $stages;
        return $data;
    }
}
