<?php

namespace App\Repositories;

use App\Models\SubjectStage;

class SubjectStageRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new SubjectStage;
    }

    /**
     * Create record
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update record by given Id
     *
     * @param array $data
     * @param int   $id
     *
     * @return void
     */
    public function update(array $data, $id)
    {
        $model = $this->find($id);
        if ($model) {
            $model->update($data);
            return $model;
        }
        return 0;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     *
     * @return mixed
     */
    public function search($query)
    {
        return empty($query) ?
            $this->model->query() :
            $this->model->where('name', 'like', '%'.$query.'%');
    }

    /**
     * Get all record
     *
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAll(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->search($search ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get record by Id
     *
     * @param mixed $id
     *
     * @return void
     */
    public function getById(int $id)
    {
        $result = $this->model->find($id);
        return $result;
    }

    /**
     * Get by subjectId and stageId
     *
     * @param mixed $subjectId
     * @param mixed $stageId
     *
     * @return Model
     */
    public function findBySubjectIdAndStageId($subjectId, $stageId)
    {
        $data = $this->model->where('subject_id', $subjectId)->where('stage_id', $stageId)->first();
        if ($data) {
            return $data;
        }
        return 0;
    }

    /**
     * Delete by subjectId and stageId
     *
     * @param mixed $subjectId
     * @param mixed $stageId
     *
     * @return void
     */
    public function deleteBySubjectIdAndStageId($subjectId, $stageId)
    {
        $data = $this->findBySubjectIdAndStageId($subjectId, $stageId);
        if ($data) {
            return $data->delete();
        }
        return 0;
    }
}
