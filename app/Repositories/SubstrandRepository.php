<?php

namespace App\Repositories;

use App\Models\Substrand;

class SubstrandRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Substrand;
    }

    /**
     * Search using querry
     *
     * @param mixed $query
     * @param int   $strandId
     *
     * @return mixed
     */
    public function searchWithStrand($query, $strandId = null)
    {
        $ormStrand = empty($strandId) ?
                $this->model->with('strand'):
                $this->model->with('strand')->where('strand_id', $strandId);
        $result = empty($query) ?
                    $ormStrand:
                    $ormStrand->whereHas(
                        "strand", function ($q) use ($query) {
                                $q->where('name', 'like', '%'.$query.'%');
                        }
                    )->orWhere('name', $query);
        return $result;
    }

    /**
     * Get all record
     *
     * @param int    $strandId
     * @param int    $size
     * @param string $search
     * @param string $sortField
     * @param bool   $sortAsc
     *
     * @return void
     */
    public function getAllWithStrand($strandId, int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true)
    {
        return simplify_paginate(
            $this->searchWithStrand($search ?? null, $strandId ?? null)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get all record as select2
     *
     * @param mixed $strandId
     *
     * @return void
     */
    public function select2WithStrand($strandId = null)
    {
        $records = $this->searchWithStrand(null, $strandId)->get()->pluck('name', 'id');
        $records = $records->toArray();
        return array_to_select2($records);
    }
}
