<?php

namespace App\Repositories;

use App\Models\Tutor;
use App\Repositories\Contract\RepositoryUserTrait;

class TutorRepository extends Repository
{
    use RepositoryUserTrait;
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new Tutor;
    }

    /**
     * Get data by user id
     *
     * @param mixed $id
     *
     * @return Model
     */
    public function find($id)
    {
        $data = $this->model->with('user', 'profession', 'school')
            ->find($id);
        return $data;
    }

    /**
     * Overload Search using querry
     *
     * @param mixed $query
     * @param int   $branchId
     *
     * @return mixed
     */
    public function searchWithOption($query, $opt = [])
    {
        $option = [
            'branch' => value_of_key($opt, 'branch'),
            'archive' => value_of_key($opt, 'archive'),
        ];
        $branchId = $option['branch'];
        $archive = $option['archive'];
        $orm = empty($branchId) ?
                $this->model
            ->with('user', 'profession', 'school'):
                $this->model
            ->with('user', 'profession', 'school')->whereHas(
                "user", function ($q) use ($branchId) {
                        $q->where('branch_id', $branchId);
                }
            );
        $result = empty($query) ?
                    $orm:
                    $orm->whereHas(
                        "user", function ($q) use ($query) {
                            $q->where('name', 'like', '%'.$query.'%');
                        }
                    )->orWhereHas(
                        "school", function ($q) use ($query) {
                                $q->where('name', 'like', '%'.$query.'%');
                        }
                    )->orWhereHas(
                        "profession", function ($q) use ($query) {
                                $q->where('name', 'like', '%'.$query.'%');
                        }
                    );
        if (!empty($archive)) {
            return $result->onlyTrashed();
        }
        return $result;
    }

}
