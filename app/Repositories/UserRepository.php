<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserRepository extends Repository
{
    /**
     * Dependency Injection
     *
     * @return void
     */
    public function __construct()
    {
        $this->model = new User;
    }

    /**
     * Store data to db based on fillabe value
     *
     * @param array $data
     *
     * @return User
     */
    public function create(array $data) : User
    {
        $fillable = $this->model->getFillable();
        array_push($fillable, "password");
        $data = array_pluck($data, $fillable);
        $data['password'] = Hash::make($data['password']);
        return $this->model->create($data);
    }
    /**
     * Create account and it's nested account likes Tutor, Parent or Student
     *
     * @param array $data
     *
     * @return User
     */
    public function createAccount(array $data) : User
    {
        if (isset($data['phone_number'])) {
            $data['phone_number'] = phone_validate($data['phone_number']);
        }
        $data['email'] = strtolower($data['email']);
        $user        = $this->create($data);
        $defaultRole = 'Student';
        $role = ucwords(strtolower(value_of_key($data, 'role')??$defaultRole));
        if ($user) {
            $data['user_id'] = $user->id;
            $repository = "App\\Repositories\\".$role."Repository";
            if (class_exists($repository)) {
                $repository = new $repository;
                $repository->create($data);
            }

            $user->assignRole($role);
            return $user;
        }
    }

    /**
     * Update data by id
     *
     * @param array $data
     * @param int   $id
     *
     * @return void
     */
    public function update(array $data, $id)
    {
        $user = $this->model->find($id);
        $data['user_id'] = $user->id;
        if ($user) {
            if (isset($data['phone_number']) ) {
                $data['phone_number'] = phone_validate($data['phone_number']);
            }

            if (isset($data['password']) ) {
                $data['password'] = Hash::make($data['password']);
            }

            if (isset($data['role']) ) {
                $curentRoles = array_content_to_lower($user->getRoleNames()->toArray());
                $user->fresh()->roles;
                // Change string to array
                $data['role'] = is_array($data['role']) ? $data['role'] : [$data['role']];

                if (is_array($data['role']) ) {
                    $newRoles = array_content_to_lower($data['role']);
                    if ($curentRoles !== $newRoles) {
                        // Remove record on table associate to role if role changed
                        $roleToRemove = array_diff($curentRoles, $newRoles);
                        foreach ($roleToRemove as $roleRemoved) {
                            $roleRemoved = ucwords(strtolower($roleRemoved));
                            $repository = "App\\Repositories\\".$roleRemoved."Repository";
                            if (class_exists($repository)) {
                                $repository = new $repository;
                                $repository->forceDeleteByUserID($data["user_id"]);
                            }
                            $user->removeRole($roleRemoved);
                        }

                        // Create record on table associate to new role
                        $roleToAdd = array_diff($newRoles, $curentRoles);
                        foreach ($roleToAdd as $role) {
                            $role = ucwords(strtolower($role));
                            $repository = "App\\Repositories\\".$role."Repository";
                            if (class_exists($repository)) {
                                $repository = new $repository;
                                $repository->create($data);
                            }
                            $user->assignRole($role);
                        }
                    }
                }
            } else {
                $role = $user->primary_role;
                $roleRepo = ucwords(strtolower($role));
                $repository = "App\\Repositories\\".$roleRepo."Repository";
                if (class_exists($repository)) {
                    $repository = new $repository;
                    $repository->update($data, $user->$role->id);
                }
            }
            $user->update($data);
            return $user;
        }
        return 0;
    }

    /**
     * Find data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function find(int $id)
    {
        $user = $this->model->find($id);
        return $user;
    }

    /**
     * Get User by they credential type, either Email or Password
     *
     * @param string $column (email/username)
     * @param string $value
     *
     * @return mixed
     */
    public function getUserByLoginType( $column, $value )
    {
        return $this->model->where($column, $value)->first();
    }

    /**
     * Overload Search using querry
     *
     * @param mixed $query
     * @param int   $branchId
     *
     * @return mixed
     */
    public function searchWithOption($query, $opt=[])
    {
        $option = [
            'branch' => value_of_key($opt, 'branch'),
            'archive' => value_of_key($opt, 'archive'),
        ];
        $ormBranch = empty($option['branch']) ?
                $this->model->with('branch'):
                $this->model->with('branch')->where('branch_id', $option['branch']);
        $result = empty($query) ?
                    $ormBranch:
                    $ormBranch->where('name', 'like', '%'.$query.'%')
            ->orWhere('username', 'like', '%'.$query.'%')
            ->orWhere('email', 'like', '%'.$query.'%');
        if (!empty($option['archive'])) {
            return $result->onlyTrashed();
        }
        return $result;
    }

     /**
      * Get all record
      *
      * @param int    $branchId
      * @param int    $size
      * @param string $search
      * @param string $sortField
      * @param bool   $sortAsc
      *
      * @return void
      */
    public function getAllWithOption(int $size = 10, string $search = null, string $sortField = 'id', bool $sortAsc = true, $option = [])
    {
        return simplify_paginate(
            $this->searchWithOption($search ?? null, $option)
                ->orderBy($sortField ?? 'id', $sortAsc ? 'asc' : 'desc')
                ->paginate($size ?? 10)
        );
    }

    /**
     * Get all record as select2
     *
     * @param mixed $column
     * @param mixed $branch
     *
     * @return void
     */
    public function select2WithOption($column, $option = [])
    {
        $records = $this->searchWithOption(null, $option)->pluck($column, 'id');
        $records = $records->toArray();
        return array_to_select2($records);
    }

    /**
     * Force delete data by id
     *
     * @param int $id
     *
     * @return Model
     */
    public function delete(int $id)
    {
        $model = $this->model->withTrashed()->find($id);
        if ($model) {
            $role = $model->primary_role;
            if( is_null($model->deleted_at) ){
                // TODO: on delete do soft delete too on student, parent or tutor
                return $model->delete();
            }else{
                return $model->forceDelete();
            }
        }
        return 0;
    }
    /**
     * Get all included user
     *
     * @param mixed $column
     * @param mixed $branch
     *
     * @return void
     */
    public function getAllInclude(int $size, string $search = null, string $role = null, array $ids, $option = [])
    {
        $orm = $this->searchWithOption($search ?? null, $option);
        if (!empty($role)) {
            $orm = $orm->role($role);
        }
        $collection = $orm->whereIn('id', $ids)->paginate($size ?? 10);
        $result = remap_paginate($collection, function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
            ];
        });
        return simplify_paginate($result);
    }

    /**
     * Get excluded user
     *
     * @param mixed $column
     * @param mixed $branch
     *
     * @return void
     */
    public function getAllExclude(int $size, string $search = null, string $role = null, array $ids, $option = [])
    {
        $orm = $this->searchWithOption($search ?? null, $option);
        if (!empty($role)) {
            $orm = $orm->role($role);
        }
        $collection = $orm->whereNotIn('id', $ids)->paginate($size ?? 10);
        $result = remap_paginate($collection, function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
            ];
        });
        return simplify_paginate($result);
    }
}
