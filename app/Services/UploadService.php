<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class UploadService
{

    /**
     * Upload image to storage
     * Example for param $path : public/article
     *
     * @param mixed $path
     * @param mixed $object
     *
     * @return string
     */
    public function uploadImage($path, $object)
    {
        $objPart = explode(";base64,", $object);
        $imageType = explode("image/", $objPart[0]);
        $extension = $imageType[1];
        $imageBase64 = base64_decode($objPart[1]);
        $fileName = date('YmHis') . '-'. uniqid() . "." . $extension;
        Storage::put($path.'/'.$fileName, $imageBase64);
        return $path."/".$fileName;
        // $imageName = format_slug(pathinfo($object->getClientOriginalName(), PATHINFO_FILENAME));
        // $filename = date('YmHis') . '-'. $imageName . "." . $object->getClientOriginalExtension();
        // $object->storeAs($path, $filename);
        // return $path."/".$filename;
    }

    /**
     * Delete image from storage
     *
     * @param mixed $object
     *
     * @return string
     */
    public function deleteImage($object)
    {
        $filePath = $object;
        Storage::delete($filePath);
    }
}
