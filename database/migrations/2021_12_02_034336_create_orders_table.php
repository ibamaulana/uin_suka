<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('order_number');
            $table->string('order_type');
            $table->string('user_type');
            $table->string('service_type');
            $table->string('payment_type');
            $table->string('lapangan')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('member_number')->nullable();
            $table->string('nik')->nullable();
            $table->string('proposal')->nullable();
            $table->string('nama_corporate')->nullable();
            $table->string('berkas_corporate')->nullable();
            $table->string('payment_method');
            $table->integer('down_payment');
            $table->integer('subtotal');
            $table->integer('discount')->default(0);
            $table->integer('total');
            $table->string('status');
            $table->string('expired_at');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
