<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInternalExternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('futsal_setting', function (Blueprint $table) {
            $table->integer('harga_eksternal_sore')->default(0);
            $table->integer('harga_eksternal_siang')->default(0);
            $table->integer('harga_internal_sore')->default(0);
            $table->integer('harga_internal_siang')->default(0);
            $table->integer('harga_internal_turnamen')->default(0);
            $table->integer('harga_eksternal_turnamen')->default(0);
        });

        Schema::table('tenis_setting', function (Blueprint $table) {
            $table->integer('harga_internal')->default(0);
            $table->integer('harga_member_siang')->default(0);
            $table->integer('harga_member_sore')->default(0);
            $table->integer('harga_internal_siang')->default(0);
            $table->integer('harga_internal_sore')->default(0);
            $table->integer('harga_turnamen')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('futsal_setting', function (Blueprint $table) {
            //
        });
    }
}
