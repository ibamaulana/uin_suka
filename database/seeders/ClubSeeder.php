<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        DB::table('club_setting')->insert([
            [
                'max_room' => 12,
                'price_per_room' => 150000,
                'price_per_breakfast' => 50000,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
