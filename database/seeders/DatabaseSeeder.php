<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(FutsalSeeder::class);
        $this->call(GedungSeeder::class);
        $this->call(ClubSeeder::class);
        $this->call(TenisSeeder::class);
        $this->call(RekeningSeeder::class);

        $this->call(OrderSeeder::class);
        $this->call(OrderDetailSeeder::class);
    }
}
