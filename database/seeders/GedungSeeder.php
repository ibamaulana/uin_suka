<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GedungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        DB::table('gedung_setting')->insert([
            [
                'whatsapp' => '+6285641728881',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
