<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        DB::table('order_details')->insert([
            //FUTSAL REG UMUM
            [
                'order_id' => '1',
                'product_name' => '08:00 - 09:00',
                'date_start' => '2021-12-10',
                'date_end' => '2021-12-10',
                'hour_start' => '08:00',
                'hour_end' => '09:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => '1',
                'product_name' => '09:00 - 10:00',
                'date_start' => '2021-12-10',
                'date_end' => '2021-12-10',
                'hour_start' => '09:00',
                'hour_end' => '10:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //FUTSAL REG INTERNAL
            [
                'order_id' => '2',
                'product_name' => '08:00 - 09:00',
                'date_start' => '2021-12-11',
                'date_end' => '2021-12-11',
                'hour_start' => '08:00',
                'hour_end' => '09:00',
                'qty' => 1,
                'price' => 50000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //FUTSAL TUR UMUM
            [
                'order_id' => '3',
                'product_name' => '10:00 - 11:00',
                'date_start' => '2021-12-11',
                'date_end' => '2021-12-11',
                'hour_start' => '10:00',
                'hour_end' => '11:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => '3',
                'product_name' => '10:00 - 11:00',
                'date_start' => '2021-12-12',
                'date_end' => '2021-12-12',
                'hour_start' => '10:00',
                'hour_end' => '11:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //FUTSAL TUR INTERNAL
            [
                'order_id' => '4',
                'product_name' => '10:00 - 11:00',
                'date_start' => '2021-12-13',
                'date_end' => '2021-12-13',
                'hour_start' => '10:00',
                'hour_end' => '11:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS REG UMUM
            [
                'order_id' => '5',
                'product_name' => '10:00 - 11:00',
                'date_start' => '2021-12-14',
                'date_end' => '2021-12-14',
                'hour_start' => '10:00',
                'hour_end' => '11:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS REG CORPORATE
            [
                'order_id' => '6',
                'product_name' => '10:00 - 11:00',
                'date_start' => '2021-12-15',
                'date_end' => '2021-12-15',
                'hour_start' => '10:00',
                'hour_end' => '11:00',
                'qty' => 1,
                'price' => 50000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS MEM UMUM
            [
                'order_id' => '7',
                'product_name' => '11:00 - 12:00',
                'date_start' => '2021-12-13',
                'date_end' => '2021-12-13',
                'hour_start' => '11:00',
                'hour_end' => '12:00',
                'qty' => 1,
                'price' => 100000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS MEM CORPORATE
            [
                'order_id' => '8',
                'product_name' => '11:00 - 12:00',
                'date_start' => '2021-12-16',
                'date_end' => '2021-12-16',
                'hour_start' => '11:00',
                'hour_end' => '12:00',
                'qty' => 1,
                'price' => 50000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS TUR UMUM
            [
                'order_id' => '9',
                'product_name' => '2021-12-15 - 2021-12-18',
                'date_start' => '2021-12-15',
                'date_end' => '2021-12-18',
                'hour_start' => null,
                'hour_end' => null,
                'qty' => 1,
                'price' => 300000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            //TENIS TUR CORPORATE
            [
                'order_id' => '10',
                'product_name' => '2021-12-20 - 2021-12-25',
                'date_start' => '2021-12-20',
                'date_end' => '2021-12-25',
                'hour_start' => null,
                'hour_end' => null,
                'qty' => 1,
                'price' => 300000,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
