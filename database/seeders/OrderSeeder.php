<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        //status
        // 1. waiting
        // 2. success
        // 3. expired
        // 4. confirmation (cuman gedung)
        // 5. cancel

        // ORDER FUTSAL REGULER UMUM
        $data = new Order();
        $data->user_id = '2';
        $data->order_number = '#FTREGUM21120001';
        $data->order_type = 'futsal';
        $data->user_type = 'umum';
        $data->service_type = 'reguler';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 200000;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER FUTSAL REGULER INTERNAL
        $data = new Order();
        $data->user_id = '2';
        $data->order_number = '#FTREGIN21120001';
        $data->order_type = 'futsal';
        $data->user_type = 'internal';
        $data->service_type = 'reguler';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->nik = '24010313120028';
        $data->down_payment = 0;
        $data->subtotal = 50000;
        $data->discount = 0;
        $data->total = 50000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER FUTSAL TURNEY UMUM
        $data = new Order();
        $data->user_id = '2';
        $data->order_number = '#FTTURUM21120001';
        $data->order_type = 'futsal';
        $data->user_type = 'umum';
        $data->service_type = 'turnamen';
        $data->payment_type = 'dp';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->nik = null;
        $data->down_payment = 20000;
        $data->subtotal = 200000;
        $data->discount = 0;
        $data->total = 180000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER FUTSAL TURNEY INTERNAL 
        $data = new Order();
        $data->user_id = '2';
        $data->order_number = '#FTTURIN21120001';
        $data->order_type = 'futsal';
        $data->user_type = 'internal';
        $data->service_type = 'turnamen';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->nik = '24010313120028';
        $data->down_payment = 0;
        $data->subtotal = 100000;
        $data->discount = 0;
        $data->total = 100000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER TENIS REGULER UMUM
        $data = new Order();
        $data->user_id = '2';
        $data->lapangan = 'lapangan_1';
        $data->order_number = '#TNREGUM21120001';
        $data->order_type = 'tenis';
        $data->user_type = 'umum';
        $data->service_type = 'reguler';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->nik = null;
        $data->down_payment = 0;
        $data->subtotal = 100000;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

         // ORDER TENIS REGULER CORPORATE
         $data = new Order();
        $data->user_id = '2';
        $data->lapangan = 'lapangan_2';
        $data->order_number = '#TNREGCOR21120001';
        $data->order_type = 'tenis';
        $data->user_type = 'corporate';
        $data->service_type = 'reguler';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->nama_corporate = 'RiotGame';
        $data->berkas_corporate = 'https://uin.vantura.id/berkas/corporate.pdf';
        $data->payment_method = 'manual';
        $data->nik = '24010313120028';
        $data->down_payment = 0;
        $data->subtotal = 50000;
        $data->discount = 0;
        $data->total = 50000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER TENIS MEMBER UMUM
        $data = new Order();
        $data->user_id = '2';
        $data->lapangan = 'lapangan_2';
        $data->order_number = '#TNMEMUM21120001';
        $data->order_type = 'tenis';
        $data->user_type = 'umum';
        $data->service_type = 'member';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->member_number = '08564171281';
        $data->payment_method = 'manual';
        $data->down_payment = 0;
        $data->subtotal = 100000;
        $data->discount = 0;
        $data->total = 0;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER TENIS MEMBER CORPORATE
        $data = new Order();
        $data->user_id =  '2';
        $data->lapangan =  'lapangan_1';
        $data->order_number =  '#TNMEMCOR21120001';
        $data->order_type =  'tenis';
        $data->user_type =  'corporate';
        $data->service_type =  'member';
        $data->payment_type =  'lunas';
        $data->name =  'PangPT';
        $data->email =  'pangpt@gmail.com';
        $data->phone =  '08564171281';
        $data->member_number =  '08564171281';
        $data->nama_corporate =  'RiotGame';
        $data->berkas_corporate =  'https://uin.vantura.id/berkas/corporate.pdf';
        $data->payment_method =  'manual';
        $data->nik =  '24010313120028';
        $data->down_payment =  0;
        $data->subtotal =  50000;
        $data->discount =  0;
        $data->total =  50000;
        $data->status =  'waiting';
        $data->expired_at =  $now;
        $data->save();

        // ORDER TENIS TURNEY UMUM
        $data = new Order();
        $data->user_id = '2';
        $data->lapangan = 'lapangan_2';
        $data->order_number = '#TNTURUM21120001';
        $data->order_type = 'tenis';
        $data->user_type = 'umum';
        $data->service_type = 'turnamen';
        $data->payment_type = 'dp';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->payment_method = 'manual';
        $data->down_payment = 30000;
        $data->subtotal = 300000;
        $data->discount = 0;
        $data->total = 370000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();

        // ORDER TENIS TURNEY CORPORATE
        $data = new Order();
        $data->user_id = '2';
        $data->lapangan = 'lapangan_1';
        $data->order_number = '#TNTURCOR21120001';
        $data->order_type = 'tenis';
        $data->user_type = 'corporate';
        $data->service_type = 'turnamen';
        $data->payment_type = 'lunas';
        $data->name = 'PangPT';
        $data->email = 'pangpt@gmail.com';
        $data->phone = '08564171281';
        $data->nama_corporate = 'RiotGame';
        $data->berkas_corporate = 'https://uin.vantura.id/berkas/corporate.pdf';
        $data->payment_method = 'manual';
        $data->nik = '24010313120028';
        $data->down_payment = 0;
        $data->subtotal = 300000;
        $data->discount = 0;
        $data->total = 300000;
        $data->status = 'waiting';
        $data->expired_at = $now;
        $data->save();
    }
}
