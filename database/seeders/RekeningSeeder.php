<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RekeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        DB::table('rekening')->insert([
            [
                'bank_name' => 'BCA',
                'account_name' => 'UIN Sunan Kalijaga',
                'account_number' => '24010313120028',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
