<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TenisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        DB::table('tenis_setting')->insert([
            [
                'price_per_hour' => 100000,
                'price_per_tourney' => 300000,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
