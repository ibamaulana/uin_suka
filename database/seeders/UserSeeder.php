<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        //LEVEL
        //0. SUPERADMIN
        //1. ADMIN
        //2. CUSTOMER
        DB::table('users')->insert([
            [
                'name' => 'Super Admin',
                'username' => 'super@admin.com',
                'nik' => '3374586544122',
                'email' => 'super@admin.com',
                'password' => bcrypt('password'),
                'level' => '0',
                'status' => null,
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'User Umum',
                'username' => 'user@umum.com',
                'nik' => null,
                'email' => 'user@umum.com',
                'password' => bcrypt('password'),
                'level' => 2,
                'status' => 'umum',
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'User Umum',
                'username' => 'user1@umum.com',
                'nik' => null,
                'email' => 'user1@umum.com',
                'password' => bcrypt('password'),
                'level' => 2,
                'status' => 'umum',
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'User Internal Mahasiswa',
                'username' => '12345678',
                'nik' => '12345678',
                'email' => '12345678@student.uin-suka.ac.id',
                'password' => bcrypt('password'),
                'level' => 2,
                'status' => 'mahasiswa',
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'User Internal Dosen',
                'username' => '12345',
                'nik' => '12345',
                'email' => '12345@uin-suka.ac.id',
                'password' => bcrypt('password'),
                'level' => 2,
                'status' => 'dosen',
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Admin',
                'username' => 'user@admin.com',
                'nik' => null,
                'email' => 'user@admin.com',
                'password' => bcrypt('password'),
                'level' => 1,
                'status' => null,
                'is_verified' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
