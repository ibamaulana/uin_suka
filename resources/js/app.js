import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import { TablePlugin, SpinnerPlugin, PaginationPlugin, FormTimepickerPlugin, FormDatepickerPlugin, DropdownPlugin, CollapsePlugin, NavbarPlugin } from 'bootstrap-vue'
import '~/plugins'
import '~/components'
import VueSwal from 'vue-swal'
import 'vue-toast-notification/dist/theme-sugar.css'
import VCalendar from 'v-calendar';

Vue.use(VCalendar);
Vue.config.productionTip = false
Vue.use(TablePlugin)
Vue.use(SpinnerPlugin)
Vue.use(PaginationPlugin)
Vue.use(FormTimepickerPlugin)
Vue.use(FormDatepickerPlugin)
Vue.use(DropdownPlugin)
Vue.use(CollapsePlugin)
Vue.use(NavbarPlugin)

Vue.use(VueSwal)
/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
