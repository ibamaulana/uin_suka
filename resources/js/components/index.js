import Vue from 'vue'
import Card from './Card.vue'
import Child from './Child.vue'
import Button from './Button.vue'
import Checkbox from './Checkbox.vue'
import Checkboxes from './Checkboxes.vue'
import Radio from './Radio.vue'
import Icon from './BaseIcon.vue'
import TableData from './TableData.vue'
import TextInput from './TextInput.vue'
import CardPage from './CardPage.vue'
import DataTable from './DataTable.vue'
import DataTable2 from './DataTable2.vue'
import FormBuilder from './FormBuilder.vue'
import VSelect from './Select2.vue'
import CardCheckbox from './CardCheckbox.vue'
import TelpInput from './TelpInput.vue'
import ImageInput from './ImageInput.vue'
import PhoneInput from '../components/PhoneInput'
import TimePicker from './TimePicker.vue'
import DatePicker from './DatePicker.vue'
import { HasError, AlertError, AlertSuccess } from 'vform/components/bootstrap5'

// vue-tel-input
import VueTelInput from 'vue-tel-input';

Vue.use(VueTelInput);

// Components that are registered globaly.
[
  PhoneInput,
  Card,
  Child,
  Button,
  Checkbox,
  Checkboxes,
  Radio,
  Icon,
  TableData,
  TextInput,
  CardPage,
  DataTable,
  DataTable2,
  FormBuilder,
  VSelect,
  CardCheckbox,
  TelpInput,
  ImageInput,
  TimePicker,
  DatePicker,
  HasError,
  AlertError,
  AlertSuccess
].forEach(Component => {
  Vue.component(Component.name, Component)
})