function page(path) {
  return () =>
    import(/* webpackChunkName: '' */ `~/pages/${path}`).then(
      (m) => m.default || m
    );
}

const allRoutes = [
  { path: "/", name: "login", component: page("auth/login.vue") },
  {
    path: "/password/reset",
    name: "password.request",
    component: page("auth/password/email.vue"),
  },
  {
    path: "/password/reset/:token",
    name: "password.reset",
    component: page("auth/password/reset.vue"),
  },
  {
    path: "/verify/:id",
    name: "verification.verify",
    component: page("auth/verification/verify.vue"),
  },
  {
    path: "/email/resend",
    name: "verification.resend",
    component: page("auth/verification/resend.vue"),
  },

  { path: "/home", name: "home", component: page("home.vue") },

  {
    path: "/perusahaan",
    component: page("perusahaan/index.vue"),
    redirect: { name: "perusahaan" },
    children: [
      {
        path: "perusahaan",
        name: "perusahaan",
        component: page("perusahaan/table"),
      },
      {
        path: "perusahaan/edit/:id",
        name: "perusahaan.update",
        component: page("perusahaan/form.vue"),
      },
      {
        path: "perusahaan/create",
        name: "perusahaan.create",
        component: page("perusahaan/form.vue"),
      },
      {
        path: "perusahaan/import",
        name: "perusahaan.import",
        component: page("perusahaan/import.vue"),
      },
      {
        path: "perusahaan/status/:id",
        name: "perusahaan.status",
        component: page("perusahaan/status.vue"),
      },
      {
        path: "perusahaan/tindak_lanjut/:perusahaan_id/create",
        name: "perusahaan.tambah_tindak_lanjut",
        component: page("perusahaan/tambah_tindak_lanjut.vue"),
      },
      {
        path: "perusahaan/tindak_lanjut/:perusahaan_id/edit/:id",
        name: "perusahaan.tambah_tindak_lanjut",
        component: page("perusahaan/tambah_tindak_lanjut.vue"),
      },
      {
        path: "perusahaan/tindak_lanjut/:id",
        name: "perusahaan.tidak_lanjut",
        component: page("perusahaan/tindak_lanjut.vue"),
      },
    ],
  },

  {
    path: "/setting",
    component: page("futsal/index.vue"),
    redirect: { name: "setting.futsal" },
    children: [
      { path: "futsal", name: "setting.futsal", component: page("futsal/form.vue") },
      { path: "gedung", name: "setting.gedung", component: page("gedung/form.vue") },
      { path: "tenis", name: "setting.tenis", component: page("tenis/form.vue") },
      { path: "club", name: "setting.club", component: page("club/form.vue") },

    ],
  },


  {
    path: "/produk",
    component: page("produk/index.vue"),
    redirect: { name: "produk" },
    children: [
      { path: "produk", name: "produk", component: page("produk/table") },
      {
        path: "produk/edit/:id",
        name: "produk.update",
        component: page("produk/form.vue"),
      },
      {
        path: "produk/create",
        name: "produk.create",
        component: page("produk/form.vue"),
      },
    ],
  },

  {
    path: "/promo",
    component: page("promo/index.vue"),
    redirect: { name: "promo" },
    children: [
      { path: "promo", name: "promo", component: page("promo/table") },
      {
        path: "promo/edit/:id",
        name: "promo.update",
        component: page("promo/form.vue"),
      },
      {
        path: "promo/create",
        name: "promo.create",
        component: page("promo/form.vue"),
      },
    ],
  },

  {
    path: "/makanan",
    component: page("makanan/index.vue"),
    redirect: { name: "makanan" },
    children: [
      { path: "makanan", name: "makanan", component: page("makanan/table") },
      {
        path: "makanan/edit/:id",
        name: "makanan.update",
        component: page("makanan/form.vue"),
      },
      {
        path: "makanan/create",
        name: "makanan.create",
        component: page("makanan/form.vue"),
      },
    ],
  },

  {
    path: "/jadwal",
    component: page("jadwal/index.vue"),
    redirect: { name: "jadwal.futsal" },
    children: [
      { path: "futsal", name: "jadwal.futsal", component: page("jadwal/table") },
      { path: "tenis", name: "jadwal.tenis", component: page("jadwal/table-tenis") },
      { path: "club", name: "jadwal.club", component: page("jadwal/table-club") },
      { path: "gedung", name: "jadwal.gedung", component: page("jadwal/table-gedung") },
      { path: "hall", name: "jadwal.hall", component: page("jadwal/table-hall") },
      {
        path: "edit/:id",
        name: "jadwal.update",
        component: page("jadwal/form.vue"),
      },
      {
        path: "create",
        name: "jadwal.create",
        component: page("jadwal/form.vue"),
      },
      {
        path: "createtenis",
        name: "jadwal.createtenis",
        component: page("jadwal/form-tenis.vue"),
      },
      {
        path: "createclub",
        name: "jadwal.createclub",
        component: page("jadwal/form-club.vue"),
      },
      {
        path: "creategedung",
        name: "jadwal.creategedung",
        component: page("jadwal/form-gedung.vue"),
      },
      {
        path: "createhall",
        name: "jadwal.createhall",
        component: page("jadwal/form-hall.vue"),
      },
    ],
  },

  {
    path: "/order",
    component: page("order/index.vue"),
    redirect: { name: "order" },
    children: [
      { path: "order", name: "order", component: page("order/table") },
      {
        path: "order/edit/:id",
        name: "order.update",
        component: page("order/form.vue"),
      },
      {
        path: "order/create",
        name: "order.create",
        component: page("order/form.vue"),
      },
      {
        path: "order/import",
        name: "order.import",
        component: page("order/import.vue"),
      },
      {
        path: "order/status/:id",
        name: "order.status",
        component: page("order/status.vue"),
      },
    ],
  },

  {
    path: "/riwayat",
    component: page("riwayat/index.vue"),
    redirect: { name: "riwayat" },
    children: [
      { path: "riwayat", name: "riwayat", component: page("riwayat/table") },
      {
        path: "riwayat/edit/:id",
        name: "riwayat.update",
        component: page("riwayat/form.vue"),
      },
      {
        path: "riwayat/create",
        name: "riwayat.create",
        component: page("riwayat/form.vue"),
      },
      {
        path: "riwayat/import",
        name: "riwayat.import",
        component: page("riwayat/import.vue"),
      },
      {
        path: "riwayat/status/:id",
        name: "riwayat.status",
        component: page("riwayat/status.vue"),
      },

    ],
  },

  {
    path: "/users",
    component: page("users/index.vue"),
    redirect: { name: "users" },
    children: [
      { path: "users", name: "users", component: page("users/table") },
      { path: "dosen", name: "users.dosen", component: page("users/dosen") },
      { path: "mahasiswa", name: "users.mahasiswa", component: page("users/mahasiswa") },
      {
        path: "users/edit/:id",
        name: "users.update",
        component: page("users/form.vue"),
      },
      {
        path: "users/create",
        name: "users.create",
        component: page("users/form.vue"),
      },
      {
        path: "users/import",
        name: "users.import",
        component: page("users/import.vue"),
      },
      {
        path: "users/status/:id",
        name: "users.status",
        component: page("users/status.vue"),
      },
      {
        path: "users/tindak_lanjut/:id",
        name: "users.tidak_lanjut",
        component: page("users/tindak_lanjut.vue"),
      },
    ],
  },

  {
    path: "/account",
    component: page("account/index.vue"),
    redirect: { name: "account" },
    children: [
      { path: "account", name: "account", component: page("account/form") },
    ],
  },

  {
    path: "/settings",
    component: page("settings/index.vue"),
    children: [
      { path: "", redirect: { name: "settings.profile" } },
      {
        path: "profile",
        name: "settings.profile",
        component: page("settings/profile.vue"),
      },
      {
        path: "password",
        name: "settings.password",
        component: page("settings/password.vue"),
      },
    ],
  },

  {
    path: "/subject",
    component: page("subject/index.vue"),
    redirect: { name: "subject" },
    children: [
      { path: "subject", name: "subject", component: page("subject/subject") },
      {
        path: "subject/:id",
        name: "subject.update",
        component: page("subject/subjectForm.vue"),
      },
      {
        path: "subject/create",
        name: "subject.create",
        component: page("subject/subjectForm.vue"),
      },

      { path: "stages", name: "stages", component: page("stages/stages") },
      {
        path: "stages/:id",
        name: "stages.update",
        component: page("stages/stagesForm.vue"),
      },
      {
        path: "stages/create",
        name: "stages.create",
        component: page("stages/stagesForm.vue"),
      },
    ],
  },

  {
    path: "/subject/subject_stages",
    component: page("subject_stages/index.vue"),
    redirect: { name: "subject_stages" },
    children: [
      {
        path: "",
        name: "subject_stages",
        component: page("subject_stages/subject_stages"),
      },
      {
        path: ":id",
        name: "subject_stages.update",
        component: page("subject_stages/subject_stagesForm.vue"),
      },
      {
        path: "create",
        name: "subject_stages.create",
        component: page("subject_stages/subject_stagesForm.vue"),
      },
    ],
  },

  { path: "*", component: page("errors/404.vue") },
];

export default allRoutes;
