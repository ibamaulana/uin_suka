import axios from 'axios'

export const getSelect2Options = async function (url, fields, key) {
  const { data: { data: rolesOption } } = await axios.get(url)
  // Transform roles options from backend to roles options from package
  const options = []
  for (let i = 0; i < rolesOption.length; i++) {
    options.push({
      id: rolesOption[i].value,
      text: rolesOption[i].text
    })
  }

  const roleField = fields.filter(field => field.key === key)
  roleField[0].options = options
}

export const isFirstObjectEqualSecondObject = (firstObj, secondObj) => {
  let isEqual = true

  const firstObjKeys = Object.keys(firstObj)

  for (let i = 0; i < firstObjKeys.length; i++) {
    console.log(firstObjKeys[i])
    console.log('first obj', firstObj[firstObjKeys[i]])
    console.log('second obj', secondObj[firstObjKeys[i]])

    if (firstObj[firstObjKeys[i]] !== secondObj[firstObjKeys[i]]) {
      console.log(firstObjKeys, 'is not equal')
      isEqual = false
      break
    }
  }

  return isEqual
}

export const isEqual = (...objects) => objects.every(obj => JSON.stringify(obj) === JSON.stringify(objects[0]))
