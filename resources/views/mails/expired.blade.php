<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="utf-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
    <!--[if mso]>
      <xml><o:OfficeDocumentSettings
          ><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings
        ></xml>
      <style>
        td,
        th,
        div,
        p,
        a,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          font-family: "Segoe UI", sans-serif;
          mso-line-height-rule: exactly;
        }
      </style>
    <![endif]-->
      <title>Informasi Subscription</title>
      <style>
@media (max-width: 600px) {
  .sm-w-full {
    width: 100% !important;
  }
  
}
</style>
  </head>
  <body style="margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;">
    <div role="article" aria-roledescription="email" aria-label="Welcome?" lang="en">
      <table style="font-family: ui-sans-serif, system-ui, -apple-system, 'Segoe UI', sans-serif; width: 100%;" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
          <td align="center" background="{{ asset('images/topography.svg') }}" style="background-color: #173656; padding-top: 24px; padding-bottom: 80px; color: #ffffff; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);">
            <table cellpadding="0" cellspacing="0" role="presentation">
              <tr>
                <td align="center" class="sm-w-full" style="width: 600px;">
                  <a href="https://provey.id">
                    <img src="{{ asset('images/logo-white.png') }}" width="200" alt="Provey" style="border: 0; line-height: 100%; max-width: 100%; vertical-align: middle;">
                  </a>
                </td>
              </tr>
              <tr>
                <td align="center">
                  <span style="font-size: 20px; text-transform: capitalize;">#SolusiBisnisUntukSemua</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center">
    <table style="background-color: #ffffff; margin-top: -48px!important; padding-left: 36px; padding-right: 36px; padding-bottom: 36px; width: 600px;" cellpadding="0" cellspacing="0" role="presentation">
      <tr>
        <td style="padding-top: 36px;">
          <div style="font-weight: 700; font-size: 24px; color: #173656;">
            Informasi Subsciption
          </div>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 24px;">
          <div style="font-weight: 700; padding-top: 4px; padding-bottom: 4px;">Dear {{ $shop->shop_name }},</div>
          <div style="padding-top: 4px; padding-bottom: 4px;">
            Masa berlaku subscription toko anda 
            @if($diff < 0)
            <span style="color:red;font-weight:700">telah habis</span>. 
            @elseif($diff > 0)
            <span style="color:red;font-weight:700">berakhir dalam {{$diff}} hari</span>. 
            @endif
            Silahkan perpanjang masa berlaku anda pada menu subscription backoffice kami.
          </div>
          <div style="padding-top: 36px;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td align="center">
                        <a  href="https://backoffice.provey.id"
                        style="background-color: #173656; border-radius: 4px; padding-top: 10px; padding-bottom: 10px; padding-left: 20px; padding-right: 20px; color: #ffffff; text-decoration: none;"
                        >Perpanjang Subscription</a>
                    </td>
                </tr>
            </table>
        </div>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 36px;">
          <div style="padding-top: 10px; padding-bottom: 10px;">
            Apabila menemukan kendala dan memerlukan bantuan lebih lanjut
            hubungi kami di
            <span style="color: #2563eb; text-decoration: underline;">081392009102</span>
            , atau email kami di
            <span style="color: #2563eb; text-decoration: underline;">info@provey.id</span>.
          </div>
          <div style="padding-bottom: 5px; padding-top: 20px;">Best Regards,</div>
          <div style="padding-top: 5px; padding-bottom: 5px;">Provey.</div>
        </td>
      </tr>
    </table>
          </td>
        </tr>
        <tr>
          <td align="center" background="{{ asset('images/topography.svg') }}" style="background-color: #173656; padding-top: 24px; padding-bottom: 24px; color: #ffffff;">
            <table cellpadding="0" cellspacing="0" role="presentation">
              <tr>
                <td align="center" class="sm-w-full" style="width: 600px;">
                  <div style="font-weight: 700; margin-bottom: 4px;">Povey POS &copy; 2021</div>
                  <div style="margin-bottom: 4px;">Crafted by PT. Vantura Digital Agensi</div>
                  <div style="margin-bottom: 4px;">
                    Jl. Ulin Selatan III No.173, Banyumanik, Semarang
                  </div>
                  <div style="margin-bottom: 4px;">
                    <a href="https://instagram.com/provey.id" style="color: #ffffff; text-decoration: underline;">Instagram</a>
                    |
                    <a href="https://provey.id" style="color: #ffffff; text-decoration: underline;">Website
                  </a></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
