<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
    <!--[if mso]>
      <xml><o:OfficeDocumentSettings
          ><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings
        ></xml>
      <style>
        td,
        th,
        div,
        p,
        a,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          font-family: "Segoe UI", sans-serif;
          mso-line-height-rule: exactly;
        }
      </style>
    <![endif]-->
    <title>Pemberitahuan : Invoice</title>
    <style>
        @media (max-width: 600px) {
            .sm-w-full {
                width: 100% !important;
            }
        }

    </style>
</head>

<body style="margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;">
    <div role="article" aria-roledescription="email" aria-label="Pemberitahuan : Invoice" lang="en">
        <table style="font-family: ui-sans-serif, system-ui, -apple-system, 'Segoe UI', sans-serif; width: 100%;"
            cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td align="center" background="{{ asset('images/topography.svg') }}"
                    style="background-color: #173656; padding-top: 24px; padding-bottom: 80px; color: #ffffff; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);">
                    <table cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td align="center" class="sm-w-full" style="width: 600px;">
                                <a href="https://provey.id">
                                    <img src="{{ asset('images/logo-white.png') }}" width="200" alt="Maizzle"
                                        style="border: 0; line-height: 100%; max-width: 100%; vertical-align: middle;">
                                </a>
                                <span style="font-size: 36px; padding-left: 12px; padding-right: 12px;">|</span>
                                <span style="font-size: 20px; text-transform: capitalize;">Solusi bisnis untuk
                                    semua</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table
                        style="background-color: #ffffff; margin-top: -48px!important; padding-left: 36px; padding-right: 36px; padding-bottom: 36px; width: 600px; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);"
                        cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td style="padding-top: 36px;">
                                <div style="font-weight: 700; font-size: 24px; color: #173656;">
                                    Pemberitahuan : Invoice
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 24px;">
                                <div style="font-weight: 700; padding-top: 4px; padding-bottom: 4px;">Halo,
                                    {{ $user->name }}!
                                </div>
                                <div style="padding-top: 4px; padding-bottom: 4px;">
                                    Terima kasih atas kepercayaan Anda untuk berlangganan layanan
                                    Provey. Mohon segera lakukan pembayaran sebelum:
                                    <span style="font-weight: 700; color: #2563eb;">{{ $paymentDueDate }}</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 24px;">
                                <div style="font-weight: 700; font-size: 18px;">Detail Transaksi</div>
                                <div style="padding-top: 10px;">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0" role="presentation">
                                        <tr>
                                            <td align="left" style="font-weight: 700; padding: 3px;">No. Invoice</td>
                                            <td style="font-weight: 700;">:</td>
                                            <td style="font-weight: 700; padding: 3px; color: #3b82f6;">
                                                #{{ $shopTransaction->invoice_id }}</td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-weight: 700; padding: 3px;">Status</td>
                                            <td style="font-weight: 700;">:</td>
                                            <td style="font-weight: 700; padding: 3px; color: #f59e0b;">Unpaid</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 24px;">
                                <table border="1" style="border-color: #000000; width: 100%;" cellpadding="0"
                                    cellspacing="0" role="presentation">
                                    <thead>
                                        <tr>
                                            <th
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                No.</th>
                                            <th
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Deskripsi Produk</th>
                                            <th
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($shopTransaction->transactionDetails as $index => $transaction)
                                            <tr>
                                                <td align="center"
                                                    style="padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                    {{$index+1}}.</td>
                                                <td align="left"
                                                    style="padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                    @if ($transaction->has_device)
                                                        Perangkat : {{ $transaction->device->name }}
                                                    @endif

                                                    @if ($transaction->has_subscription)
                                                        Subscription : {{ $transaction->subscription->name }} (1
                                                        Tahun)
                                                    @endif
                                                </td>
                                                <td align="right"
                                                    style="padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                    Rp {{ number_format($transaction->price) }},-</td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td align="right" colspan="2"
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Sub Total :
                                            </td>
                                            <td align="right"
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Rp {{ number_format($shopTransaction->total) }},-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2"
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Diskon :
                                            </td>
                                            <td align="right"
                                                style="background-color: #dbeafe; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                -</td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2"
                                                style="background-color: #dbeafe; font-weight: 700; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Total :
                                            </td>
                                            <td align="right"
                                                style="background-color: #dbeafe; font-weight: 700; padding-left: 10px; padding-right: 10px; padding-top: 16px; padding-bottom: 16px;">
                                                Rp {{ number_format($shopTransaction->total) }},-
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 24px;">
                                <div style="padding-top: 10px; padding-bottom: 10px;">
                                    Invoice akan aktif selama satu minggu, mohon segera lakukan
                                    pembayaran untuk menghindari interupsi layanan Anda. Kami menerima
                                    pembayaran via transfer ke rekening :
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding-top: 24px;">
                                <table cellpadding="0" cellspacing="0" role="presentation">
                                    <tr>
                                        <td align="center" style="padding-left: 10px; padding-right: 10px;">
                                            <img src="{{ asset('images/mandiri.svg') }}" width="192" alt="" srcset=""
                                                style="border: 0; line-height: 100%; max-width: 100%; vertical-align: middle;">
                                        </td>
                                        <td style="padding-left: 10px; padding-right: 10px;">
                                            <div style="font-weight: 700; color: #3b82f6;">Bank Mandiri</div>
                                            <div style="color: #3b82f6;">136-00-9070219-9</div>
                                            <div style="color: #3b82f6;">PT. Vantura Digital Agensi</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 32px;">
                                <div>
                                    Jika Anda membutuhkan bantuan lebih lanjut silahkan hubungi kami di
                                    <span style="color: #2563eb; text-decoration: underline;">(024)3231321</span>, atau
                                    email kami di
                                    <span
                                        style="color: #2563eb; text-decoration: underline;">provey.id@gmail.com</span>.
                                </div>
                                <div style="padding-bottom: 5px; padding-top: 20px;">Best Regards,</div>
                                <div style="padding-top: 5px; padding-bottom: 5px;">Provey.</div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"
                    style="background-image: url('{{ asset('images/topography.svg') }}'); background-color: #173656; padding-top: 24px; padding-bottom: 24px; color: #ffffff;">
                    <table cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td align="center" class="sm-w-full" style="width: 600px;">
                                <div style="font-weight: 700; margin-bottom: 4px;">Povey POS &copy; 2021</div>
                                <div style="margin-bottom: 4px;">Crafted by PT. Vantura Digital Agensi</div>
                                <div style="margin-bottom: 4px;">
                                    Jl. Ulin Selatan III No.173, Banyumanik, Semarang
                                </div>
                                <div style="margin-bottom: 4px;">
                                    <a href="https://instagram.com/provey.id"
                                        style="color: #ffffff; text-decoration: underline;">Instagram</a>
                                    |
                                    <a href="https://provey.id"
                                        style="color: #ffffff; text-decoration: underline;">Website
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
