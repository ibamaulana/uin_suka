<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    </head>
    <body style="margin: 40px auto;text-align: center;
    margin: 0 auto;
    width: 650px;
    font-family: 'Lato', sans-serif;
    background-color: #e2e2e2;
    display: block;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 0px 30px 60px 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;border-radius:20px">
            <tbody>
                <tr>
                    <td>
                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="margin-bottom: 30px;">
                                    <img src="{{url('images/'.$order->shop->shop_logo)}}" alt="" style="width:20%;">
                                    <p style="magin-bottom:0"><b>{{ $order->shop->shop_name }}</b></p>
                                    <p>{{ $order->shop->shop_address }}</p>

                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <h2 class="title">Order Invoice {{$order->order_number}}</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <p>Nama : {{$order->customer_name}}</p>
                                    <p>Telepon : {{$order->customer->phone}}</p>
                                    <p>Tanggal : {{ date('Y-m-d',strtotime($order->created_at))}}</p>
                                    <p>Garansi : {{ date("Y-m-d", strtotime("+1 month", strtotime($order->created_at)))}}</p>
                                </td>
                            </tr>
                        </table>
                        <table align="left" border="1" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="padding:10px;width:10%">No.</td>
                                <td style="padding:10px;width:50%;text-align:left">Produk</td>
                                <td style="padding:10px;width:40%">Harga</td>
                            </tr>
                            @php
                                $i = 0;
                            @endphp
                            @foreach($order->orderdetails as $key)
                            <tr>
                                <td style="padding:20px;width:10%">{{ ++$i }}</td>
                                <td style="padding:20px;width:50%;text-align:left">{{$key->product->categories->name}} <br> {{ $key->product->product_name }}</td>
                                <td style="padding:20px;width:40%;text-align:right">Rp. {{number_format($key->total_price)}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="2" style="padding:20px;text-align:right">Sub Total</td>
                                <td style="padding:20px;width:40%;text-align:right">Rp. {{number_format($order->total_price)}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding:20px;text-align:right">Diskon</td>
                                <td style="padding:20px;width:40%;text-align:right">Rp. {{number_format($order->discount)}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding:20px;text-align:right">Total</td>
                                <td style="padding:20px;width:40%;text-align:right">Rp. {{number_format($order->total)}}</td>
                            </tr>
                        </table>
                        
                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>

                                <td>
                                    <div style="border-top:1px solid #777;height:1px;margin-top: 50px;">
                                </td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td>
                                    <img src="{{url('images/logo.jpg')}}" alt="" style="width:20%;margin-top: 30px;">
                                    <p>2020 | Powered By Provey</p>
                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>
            </tbody>
        </table>
        {{-- <table class="main-bg-light text-center top-0"  align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 30px;">
                                    <div>
                                        <h4 class="title" style="margin:0;text-align: center;">Follow us</h4>
                                    </div>
                                    <table border="0" cellpadding="0" cellspacing="0" class="footer-social-icon" align="center" class="text-center" style="margin-top:20px;">
                                        <tr>
                                            <td>
                                                <a href="#"><img src="{{asset("/assets-front/images/email-temp/facebook.png")}}" alt=""></a>
                                            </td>
                                            <td>
                                                <a href="#"><img src="{{asset("/assets-front/images/email-temp/youtube.png")}}" alt=""></a>
                                            </td>
                                            <td>
                                                <a href="#"><img src="{{asset("/assets-front/images/email-temp/twitter.png")}}" alt=""></a>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                                    <table  border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;" >
                                        <tr>
                                            <td>
                                                <p style="font-size:13px; margin:0;">2017-18 powered by <a href="https://vantura.id" target="_blank">vantura.id</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> --}}
    </body>
</html>
