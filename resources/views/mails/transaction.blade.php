<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="utf-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
    <title>Verifikasi Akun</title>
    <style>
        @media (max-width: 600px) {
        .sm-w-full {
            width: 100% !important;
        }
        }
    </style>
  </head>
  <body style="margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased;">
    <div role="article" aria-roledescription="email" aria-label="Welcome?" lang="en">
      <table style="font-family: ui-sans-serif, system-ui, -apple-system, 'Segoe UI', sans-serif; width: 100%;" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
          <td align="center" background="{{ asset('images/topography.svg') }}" style="background-color: #2BCC9C; padding-top: 24px; padding-bottom: 80px; color: #ffffff; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);">
            <table cellpadding="0" cellspacing="0" role="presentation">
              <tr>
                <td align="center">
                  <span style="font-size: 20px; text-transform: capitalize;">UIN SUKA APP</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center">
    <table style="background-color: #ffffff; margin-top: -48px!important; padding-left: 36px; padding-right: 36px; padding-bottom: 36px; width: 600px;" cellpadding="0" cellspacing="0" role="presentation">
      <tr>
        <td style="padding-top: 36px;">
          <div style="font-weight: 700; font-size: 24px; color: #2BCC9C;">
            Terimakasih telah melakukan transaksi !
          </div>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 24px;">
          <div style="font-weight: 700; padding-top: 4px; padding-bottom: 4px;">Halo, {{ $name }}!</div>
          <div style="padding-top: 4px; padding-bottom: 4px;">
            Terima kasih telah mendaftar di aplikasi kami, silahkan verifikasi akun anda dengaan menekan tombol dibawah.
          </div>
          <div style="padding-top: 36px;">
            
        </div>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 36px;">
          <div style="padding-top: 10px; padding-bottom: 10px;">
            Apabila menemukan kendala dan memerlukan bantuan lebih lanjut
            hubungi kami di
            <span style="color: #2563eb; text-decoration: underline;">(0274) 512474</span>
            , atau email kami di
            <span style="color: #2563eb; text-decoration: underline;">uinsukaapp@gmail.com</span>.
          </div>
          <div style="padding-bottom: 5px; padding-top: 20px;">Best Regards,</div>
          <div style="padding-top: 5px; padding-bottom: 5px;">UIN Sunan Kalijaga.</div>
        </td>
      </tr>
    </table>
          </td>
        </tr>
        <tr>
          <td align="center" background="{{ asset('images/topography.svg') }}" style="background-color: #2BCC9C; padding-top: 24px; padding-bottom: 24px; color: #ffffff;">
            <table cellpadding="0" cellspacing="0" role="presentation">
              <tr>
                <td align="center" class="sm-w-full" style="width: 600px;">
                  <div style="font-weight: 700; margin-bottom: 4px;">UIN SUKA APP &copy; 2021</div>
                  <div style="margin-bottom: 4px;">Crafted by UIN Sunan Kalijaga</div>
                  <div style="margin-bottom: 4px;">
                    Jl. Laksda Adisucipto, Papringan, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
