<?php


use App\Http\Controllers\Admin\User\UserController;
use App\Http\Controllers\Admin\User\ParentController;
use App\Http\Controllers\Admin\User\StudentController;
use App\Http\Controllers\Admin\User\TutorController;
use App\Http\Controllers\Admin\Privilege\PermissionController;
use App\Http\Controllers\Admin\Privilege\RoleController;
use App\Http\Controllers\Admin\Master\BranchController;
use App\Http\Controllers\Admin\Master\ClassController;
use App\Http\Controllers\Admin\Master\LevelController;
use App\Http\Controllers\Admin\Master\SubjectController;
use App\Http\Controllers\Admin\Master\StageController;
use App\Http\Controllers\Admin\Master\StrandController;
use App\Http\Controllers\Admin\Master\SubstrandController;
use App\Http\Controllers\Admin\Master\SchoolController;
use App\Http\Controllers\Admin\Master\ProfessionController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
// Master
    Route::prefix('branches')->as('branches.')->group(function () {
        Route::get('select2', [ BranchController::class, "select2" ])->name('select2');
        Route::post('configs', [ BranchController::class, "addGeneralConfig" ])->name('create-general-config');
        Route::post('/{id}/configs', [ BranchController::class, "addSpecificConfig" ])->name('create-specific-config');
        Route::delete('configs', [ BranchController::class, "destroyGeneralConfig" ])->name('destroy-general-config');
        Route::delete('/{id}/configs', [ BranchController::class, "destroySpecificConfig" ])->name('destroy-specific-config');
    });
    Route::apiResources(['branches' => BranchController::class]);

    Route::prefix('subjects')->as('subjects.')->group(function () {
        Route::get('select2', [ SubjectController::class, "select2" ])->name('select2');
        Route::post('/{id}/stages', [ SubjectController::class, "createSubjectStage" ])->name('create-subjectstage');
        Route::delete('/{id}/stages/{stageId}', [ SubjectController::class, "deleteSubjectStage" ])->name('delete-subjectstage');
    });
    Route::apiResources(['subjects' => SubjectController::class]);

    Route::prefix('stages')->as('stages.')->group(function () {
        Route::get('select2', [ StageController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['stages' => StageController::class]);

    Route::prefix('levels')->as('levels.')->group(function () {
        Route::get('select2', [ LevelController::class, "select2" ])->name('select2');
        Route::post('/{id}/substrands', [ LevelController::class, "createLevelSubstrand" ])->name('create-levelsubstrand');
        Route::delete('/{id}/substrands/{substrandId}', [ LevelController::class, "deleteLevelSubstrand" ])->name('delete-levelsubstrand');
    });
    Route::apiResources(['select2' => LevelController::class]);

    Route::prefix('strands')->as('strands.')->group(function () {
        Route::get('select2', [ StrandController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['strands' => StrandController::class]);

    Route::prefix('substrands')->as('substrands.')->group(function () {
        Route::get('select2', [ SubstrandController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['substrands' => SubstrandController::class]);

    Route::prefix('professions')->as('professions.')->group(function () {
        Route::get('select2', [ ProfessionController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['professions' => ProfessionController::class]);

    Route::prefix('schools')->as('schools.')->group(function () {
        Route::get('select2', [ SchoolController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['schools' => SchoolController::class]);

    Route::prefix('classes')->as('classes.')->group(function () {
        Route::post('/{id}/restore', [ ClassController::class, "restore" ])->name('restore');
        Route::get('/{id}/people', [ ClassController::class, "getClassPeople" ])->name('get-classpeople');
        Route::patch('/{id}/people', [ ClassController::class, "patchClassPeople" ])->name('update-classpeople');
    });
    Route::apiResources(['classes' => ClassController::class]);

// Privilage
    Route::prefix('roles')->as('roles.')->group(function () {
        Route::get('select2', [ RoleController::class, "select2" ])->name('select2');
        Route::get('/{id}/permissions', [ RoleController::class, "getRolePermission" ])->name('get-permissions');
        Route::patch('/{id}/permissions', [ RoleController::class, "patchUpdatePermission" ])->name('update-permissions');
    });
    Route::apiResources(['roles' => RoleController::class]);

    Route::prefix('permissions')->as('permissions.')->group(function () {
        Route::get('select2', [ PermissionController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['permissions' => PermissionController::class]);

// User
    Route::prefix('users')->as('users.')->group(function () {
        Route::get('select2', [ UserController::class, "select2" ])->name('select2');
        Route::post('/{id}/restore', [ UserController::class, "restore" ])->name('restore');
        Route::post('include', [ UserController::class, "include" ])->name('include');
        Route::post('exclude', [ UserController::class, "exclude" ])->name('exclude');
    });
    Route::apiResources(['users' => UserController::class]);

    Route::prefix('parents')->as('parents.')->group(function () {
        Route::get('select2', [ ParentController::class, "select2" ])->name('select2');
        Route::post('/{id}/students', [ ParentController::class, "createStudentParent" ])->name('create-studentparent');
        Route::delete('/{id}/students/{studentId}', [ ParentController::class, "deleteStudentParent" ])->name('delete-studentparent');
    });
    Route::apiResources(['parents' => ParentController::class]);

    Route::prefix('students')->as('students.')->group(function () {
        Route::get('select2', [ StudentController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['students' => StudentController::class]);

    Route::prefix('tutors')->as('tutors.')->group(function () {
        Route::get('select2', [ TutorController::class, "select2" ])->name('select2');
    });
    Route::apiResources(['tutors' => TutorController::class]);

});

