<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use App\Http\Controllers\Admin\Privilege\RoleController;
use App\Http\Controllers\Admin\Privilege\PermissionController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\MakananController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('current', [AuthController::class, 'current']);

    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);

    // Route::prefix('setting')->as('setting.')->group(function () {
    //     Route::get('/', [ PerusahaanController::class, "index" ])->name('data');
    //     Route::get('/datastatus', [ PerusahaanController::class, "datastatus" ])->name('datastatus');
    //     Route::get('/status', [ PerusahaanController::class, "status" ])->name('status');
    //     Route::get('/{id}', [ PerusahaanController::class, "detail" ])->name('detail');
    //     Route::post('/', [ PerusahaanController::class, "store" ])->name('store');
    //     Route::patch('/{id}', [ PerusahaanController::class, "update" ])->name('update');
    //     Route::delete('/{id}', [ PerusahaanController::class, "delete" ])->name('delete');
    // });

    Route::group(['prefix' => 'file'], function () {
        Route::post("upload", [FileController::class, 'upload']);
        Route::post("uploadbulk", [FileController::class, 'uploadbulk']);
        Route::post("remove", [FileController::class, 'remove']);
    });

    Route::prefix('perusahaan')->as('perusahaan.')->group(function () {
        Route::get('/datatindaklanjut', [ PerusahaanController::class, "datatindaklanjut" ])->name('datatindaklanjut');
        Route::post('/datatindaklanjut', [ PerusahaanController::class, "datatindaklanjut_store" ])->name('datatindaklanjutstore');
        Route::patch('/datatindaklanjut/{id}', [ PerusahaanController::class, "datatindaklanjut_update" ])->name('datatindaklanjutupdate');
        Route::delete('/datatindaklanjut/{id}', [ PerusahaanController::class, "datatindaklanjut_delete" ])->name('datatindaklanjutdelete');

        Route::get('/', [ PerusahaanController::class, "index" ])->name('data');
        Route::get('/datastatus', [ PerusahaanController::class, "datastatus" ])->name('datastatus');
        Route::get('/status', [ PerusahaanController::class, "status" ])->name('status');
        Route::get('/{id}', [ PerusahaanController::class, "detail" ])->name('detail');
        Route::post('/', [ PerusahaanController::class, "store" ])->name('store');
        Route::patch('/{id}', [ PerusahaanController::class, "update" ])->name('update');
        Route::delete('/{id}', [ PerusahaanController::class, "delete" ])->name('delete');
    });

    Route::prefix('produk')->as('produk.')->group(function () {
        Route::get('/', [ ProdukController::class, "index" ])->name('data');
        Route::get('/{id}', [ ProdukController::class, "detail" ])->name('detail');
        Route::post('/', [ ProdukController::class, "store" ])->name('store');
        Route::patch('/{id}', [ ProdukController::class, "update" ])->name('update');
        Route::delete('/{id}', [ ProdukController::class, "delete" ])->name('delete');
    });

    Route::prefix('promo')->as('promo.')->group(function () {
        Route::get('/', [ PromoController::class, "index" ])->name('data');
        Route::get('/{id}', [ PromoController::class, "detail" ])->name('detail');
        Route::post('/', [ PromoController::class, "store" ])->name('store');
        Route::patch('/{id}', [ PromoController::class, "update" ])->name('update');
        Route::delete('/{id}', [ PromoController::class, "delete" ])->name('delete');
    });

    Route::prefix('makanan')->as('makanan.')->group(function () {
        Route::get('/', [ MakananController::class, "index" ])->name('data');
        Route::get('/{id}', [ MakananController::class, "detail" ])->name('detail');
        Route::post('/', [ MakananController::class, "store" ])->name('store');
        Route::patch('/{id}', [ MakananController::class, "update" ])->name('update');
        Route::delete('/{id}', [ MakananController::class, "delete" ])->name('delete');
    });

    Route::prefix('users')->as('users.')->group(function () {
        Route::get('/', [ UsersController::class, "index" ])->name('data');
        Route::get('/select', [ UsersController::class, "select" ])->name('select');
        Route::get('/status', [ UsersController::class, "status" ])->name('status');
        Route::get('/current', [ UsersController::class, "current" ])->name('current');
        Route::get('/{id}', [ UsersController::class, "detail" ])->name('detail');
        Route::post('/', [ UsersController::class, "store" ])->name('store');
        Route::patch('/{id}', [ UsersController::class, "update" ])->name('update');
        Route::delete('/{id}', [ UsersController::class, "delete" ])->name('delete');
    });

    Route::prefix('setting')->as('setting.')->group(function () {
        Route::get('/futsal', [ SettingController::class, "futsal" ])->name('data');
        Route::get('/club', [ SettingController::class, "club" ])->name('data');
        Route::get('/gedung', [ SettingController::class, "gedung" ])->name('data');
        Route::get('/tenis', [ SettingController::class, "tenis" ])->name('data');
        Route::patch('/futsal/{id}', [ SettingController::class, "updatefutsal" ])->name('data');
        Route::patch('/club/{id}', [ SettingController::class, "updateclub" ])->name('data');
        Route::patch('/gedung/{id}', [ SettingController::class, "updategedung" ])->name('data');
        Route::patch('/tenis/{id}', [ SettingController::class, "updatetenis" ])->name('data');
        Route::get('/detailpesanan', [ PesananController::class, "detailpesanan" ])->name('detailpesanan');
        Route::get('/', [ PesananController::class, "index" ])->name('data');
        Route::get('/select', [ PesananController::class, "select" ])->name('select');
        Route::get('/status', [ PesananController::class, "status" ])->name('status');
        Route::get('/current', [ PesananController::class, "current" ])->name('current');
        Route::get('/{id}', [ PesananController::class, "detail" ])->name('detail');
        Route::post('/', [ PesananController::class, "store" ])->name('store');
        Route::patch('/{id}', [ PesananController::class, "update" ])->name('update');
        Route::delete('/{id}', [ PesananController::class, "delete" ])->name('delete');
    });

    Route::prefix('riwayat')->as('.riwayat')->group(function () {
        Route::get('/', [RiwayatController::class, "index" ])->name('data');
    });

    Route::prefix('order')->as('order.')->group(function () {
        Route::get('/', [ OrderController::class, "index" ])->name('data');
        Route::get('/jadwal/futsal', [ OrderController::class, "jadwalfutsal" ])->name('jadwalfutsal');
        Route::get('/jadwal/futsalturnamen', [ OrderController::class, "jadwalfutsalturnamen" ])->name('jadwalfutsalturnamen');
        Route::get('/jadwal/tenis', [ OrderController::class, "jadwaltenis" ])->name('jadwaltenis');
        Route::get('/jadwal/tenisturnamen', [ OrderController::class, "jadwaltenisturnamen" ])->name('jadwaltenisturnamen');
        Route::get('/jadwal/gedung', [ OrderController::class, "jadwalgedung" ])->name('jadwalgedung');
        Route::get('/jadwal/hall', [ OrderController::class, "jadwalhall" ])->name('jadwalhall');
        Route::get('/jadwal/club', [ OrderController::class, "jadwalclub" ])->name('jadwalclub');
        Route::post('/create', [ OrderController::class, "store" ])->name('store');
        Route::post('/updategedung/{id}', [ OrderController::class, "updategedung" ])->name('updategedung');
        Route::post('/bayargedung/{id}', [ OrderController::class, "bayargedung" ])->name('bayargedung');
        Route::post('/updatebukti/{id}', [ OrderController::class, "updatebukti" ])->name('updatebukti');
        Route::get('/{id}', [ OrderController::class, "detail" ])->name('detail');
        Route::delete('/delete/{id}', [ OrderController::class, "delete" ])->name('delete');
        Route::post('/cancelgedung/{id}', [ OrderController::class, "cancelgedung" ])->name('cancelgedung');
        // Route::post('/cancelfutsal/{id}', [ OrderController::class, "cancelgedung" ])->name('cancelgedung');
    });

    Route::prefix('jadwal')->as('order.')->group(function () {
        Route::get('/', [ JadwalController::class, "index" ])->name('data');
        Route::get('/futsal', [ JadwalController::class, "jadwalfutsal" ])->name('jadwalfutsal');
        Route::get('/tenis', [ JadwalController::class, "jadwaltenis" ])->name('jadwaltenis');
        Route::get('/gedung', [ JadwalController::class, "jadwalgedung" ])->name('jadwalgedung');
        Route::get('/hall', [ JadwalController::class, "jadwalhall" ])->name('jadwalhall');
        Route::get('/club', [ JadwalController::class, "jadwalclub" ])->name('jadwalclub');
        Route::post('/storefutsal', [ JadwalController::class, "storefutsal" ])->name('storefutsal');
        Route::post('/storetenis', [ JadwalController::class, "storetenis" ])->name('storetenis');
        Route::post('/storeclub', [ JadwalController::class, "storeclub" ])->name('storeclub');
        Route::post('/storegedung', [ JadwalController::class, "storegedung" ])->name('storegedung');
        Route::post('/storehall', [ JadwalController::class, "storehall" ])->name('storehall');
        Route::post('/create', [ JadwalController::class, "store" ])->name('store');

        Route::post('/updatebukti/{id}', [ JadwalController::class, "updatebukti" ])->name('updatebukti');
        Route::get('/{id}', [ JadwalController::class, "detail" ])->name('detail');
        Route::patch('/{id}', [ JadwalController::class, "update" ])->name('update');
        Route::delete('/{id}', [ JadwalController::class, "delete" ])->name('delete');

    });


});

Route::prefix('order')->as('order.')->group(function () {
    Route::post('handling', [ OrderController::class, "handling" ])->name('handling');
});

Route::group(['middleware' => 'guest:api'], function () {

    Route::post('loginweb', [LoginController::class, 'login']);

    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('verify', [AuthController::class, 'verify']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');

    Route::prefix('privileges')->as('roles.')->group(function () {
        Route::prefix('roles')->as('roles.')->group(function () {
            Route::get('/', [ RoleController::class, "roles" ])->name('all');
        });
        Route::prefix('permissions')->as('permissions.')->group(function () {
            Route::get('/', [ PermissionController::class, "permissions" ])->name('all');
        });
    });


});
